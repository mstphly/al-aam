-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             7.0.0.4379
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for allsmiles
DROP DATABASE IF EXISTS `allsmiles`;
CREATE DATABASE IF NOT EXISTS `allsmiles` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `allsmiles`;


-- Dumping structure for table allsmiles.bookings
DROP TABLE IF EXISTS `bookings`;
CREATE TABLE IF NOT EXISTS `bookings` (
  `book_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record_id` int(10) unsigned NOT NULL,
  `book_date` date DEFAULT NULL,
  `book_time` time DEFAULT NULL,
  `book_type` int(11) DEFAULT NULL,
  `book_category` int(11) DEFAULT NULL,
  `book_type_comment` text,
  `doctor_id` int(10) unsigned DEFAULT NULL,
  `book_status` set('BOOKED','NOT BOOKED','CANCELLED') NOT NULL DEFAULT 'NOT BOOKED',
  `appointment_status` set('CANCELLED BY CLIENT','COMPLETED','CANCELLED BY US','PENDING') NOT NULL,
  PRIMARY KEY (`book_id`),
  KEY `FK_bookings_record` (`record_id`),
  CONSTRAINT `FK_bookings_record` FOREIGN KEY (`record_id`) REFERENCES `record` (`record_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table allsmiles.bookings: ~3 rows (approximately)
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` (`book_id`, `record_id`, `book_date`, `book_time`, `book_type`, `book_category`, `book_type_comment`, `doctor_id`, `book_status`, `appointment_status`) VALUES
	(8, 22, '2013-04-02', '15:00:00', 7, 4, 'What do i do no...tell me', NULL, 'NOT BOOKED', ''),
	(9, 22, '2013-04-02', '14:00:00', 24, 1, 'read', 24, 'BOOKED', 'PENDING'),
	(11, 21, '2013-04-03', '14:00:00', 23, 1, 'adsasfdsfsdf', NULL, 'CANCELLED', 'CANCELLED BY US');
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;


-- Dumping structure for table allsmiles.chat
DROP TABLE IF EXISTS `chat`;
CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) NOT NULL DEFAULT '',
  `to` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table allsmiles.chat: ~0 rows (approximately)
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;


-- Dumping structure for table allsmiles.checking_patient_map
DROP TABLE IF EXISTS `checking_patient_map`;
CREATE TABLE IF NOT EXISTS `checking_patient_map` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `checking_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `change_by` int(11) NOT NULL,
  `change_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`auto_id`),
  KEY `fk_user_id` (`patient_id`,`create_by`,`change_by`),
  KEY `fk_checking_id` (`checking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table allsmiles.checking_patient_map: ~0 rows (approximately)
/*!40000 ALTER TABLE `checking_patient_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `checking_patient_map` ENABLE KEYS */;


-- Dumping structure for table allsmiles.doctor_patient_map
DROP TABLE IF EXISTS `doctor_patient_map`;
CREATE TABLE IF NOT EXISTS `doctor_patient_map` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `channel_date` date NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `change_by` int(11) NOT NULL,
  `change_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `checking_patient_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`auto_id`),
  KEY `fk_doctor_id` (`doctor_id`),
  KEY `fk_user_id` (`patient_id`,`doctor_id`,`create_by`,`change_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table allsmiles.doctor_patient_map: ~0 rows (approximately)
/*!40000 ALTER TABLE `doctor_patient_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `doctor_patient_map` ENABLE KEYS */;


-- Dumping structure for table allsmiles.doctor_specialist_map
DROP TABLE IF EXISTS `doctor_specialist_map`;
CREATE TABLE IF NOT EXISTS `doctor_specialist_map` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_id` int(11) unsigned NOT NULL COMMENT 'This ID is also the record ID',
  `specialist_id` int(11) NOT NULL,
  PRIMARY KEY (`auto_id`),
  KEY `fk_doctor_id` (`record_id`),
  KEY `fk_special_id` (`specialist_id`),
  CONSTRAINT `FK_doctor_specialist_map_record` FOREIGN KEY (`record_id`) REFERENCES `record` (`record_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table allsmiles.doctor_specialist_map: ~1 rows (approximately)
/*!40000 ALTER TABLE `doctor_specialist_map` DISABLE KEYS */;
INSERT INTO `doctor_specialist_map` (`auto_id`, `record_id`, `specialist_id`) VALUES
	(1, 24, 1);
/*!40000 ALTER TABLE `doctor_specialist_map` ENABLE KEYS */;


-- Dumping structure for table allsmiles.messaging
DROP TABLE IF EXISTS `messaging`;
CREATE TABLE IF NOT EXISTS `messaging` (
  `msg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record_id` int(10) unsigned NOT NULL,
  `msg_title` varchar(50) DEFAULT NULL,
  `msg_content` text,
  `recipient` char(128) NOT NULL,
  `msg_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `msg_status` char(50) DEFAULT 'Unread',
  PRIMARY KEY (`msg_id`),
  KEY `FK_messaging_record` (`record_id`),
  CONSTRAINT `FK_messaging_record` FOREIGN KEY (`record_id`) REFERENCES `record` (`record_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table allsmiles.messaging: ~5 rows (approximately)
/*!40000 ALTER TABLE `messaging` DISABLE KEYS */;
INSERT INTO `messaging` (`msg_id`, `record_id`, `msg_title`, `msg_content`, `recipient`, `msg_datetime`, `msg_status`) VALUES
	(1, 22, 'The New Privacy and Policy Plan', 'Dear Ketsia,Allsmiles dental clinic is willing to offer your preffered services to you at the way you want it, but it will require more charges from you.', 'b@b.com', '2013-04-10 09:08:24', 'read'),
	(2, 22, 'New Message Testing', 'Dear Ketsia,We are Happy to hear from you', 'b@b.com', '2013-04-10 09:21:46', 'Unread'),
	(3, 22, 'New Message Testing', '&lt;span style=\\&quot;font-weight: bold;\\&quot;&gt;Dear Ketsia,&lt;br /&gt;&lt;/span&gt;&lt;br /&gt;We are Happy to hear from you', 'b@b.com', '2013-04-10 09:25:57', 'read'),
	(5, 22, 'Booking Appointment Confirmation', '\r\n				&lt;div class=&quot;display:block; overflow:hidden; &quot;&gt;\r\n								&lt;div style=&quot;background:#333333; padding:5px 15px; font-size:14px; color:#FFFFFF&quot;&gt;\r\n									Booking Appointment COnfirm Details\r\n								&lt;/div&gt;\r\n								&lt;div style=&quot;padding:5px 10px; font-size:13px;border:1px solid #666666; line-height:22px;&quot;&gt;\r\n									Dear Receptionist M Receptionist,\r\n									\r\n									Your booking for the date Tuesday, April 2, 2013, 2:00 PM is confirmed, you are expected to visit to the clinic\r\n									\r\n									&lt;div style=&quot;margin-top:30px; border-top:1px solid #CCCCCC; background:#FEFEFE; padding:5px; 10px&quot;&gt;\r\n										This email is an automated mail from Allsmiles dental clinic, please do not reply to it\r\n									&lt;/div&gt;\r\n								&lt;/div&gt;\r\n								\r\n							&lt;/div&gt;\r\n				', 'info@allsmilesclinic.com', '2013-04-10 12:40:32', 'Unread'),
	(6, 22, 'Booking Appointment Confirmation', '\r\n				&lt;div class=&quot;display:block; overflow:hidden; &quot;&gt;\r\n								&lt;div style=&quot;background:#333333; padding:5px 15px; font-size:14px; color:#FFFFFF&quot;&gt;\r\n									Booking Appointment Cancellation\r\n								&lt;/div&gt;\r\n								&lt;div style=&quot;padding:5px 10px; font-size:13px;border:1px solid #666666; line-height:22px;&quot;&gt;\r\n									Dear ketsia ketsia ketsia,\r\n									\r\n									Your booking for the date Wednesday, April 3, 2013, 2:00 PM has being cancelled, you are expected to contact the clinic for more details\r\n									\r\n									&lt;div style=&quot;margin-top:30px; border-top:1px solid #CCCCCC; background:#FEFEFE; padding:5px; 10px&quot;&gt;\r\n										This email is an automated mail from Allsmiles dental clinic, please do not reply to it\r\n									&lt;/div&gt;\r\n								&lt;/div&gt;\r\n								\r\n							&lt;/div&gt;\r\n				', 'info@allsmilesclinic.com', '2013-04-10 12:45:04', 'Unread');
/*!40000 ALTER TABLE `messaging` ENABLE KEYS */;


-- Dumping structure for table allsmiles.newsletter
DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `news_id` int(10) DEFAULT NULL,
  `to_email` int(10) DEFAULT NULL,
  `to_subject` int(10) DEFAULT NULL,
  `to_content` int(10) DEFAULT NULL,
  `news_date` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table allsmiles.newsletter: ~0 rows (approximately)
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;


-- Dumping structure for table allsmiles.record
DROP TABLE IF EXISTS `record`;
CREATE TABLE IF NOT EXISTS `record` (
  `record_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_type` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `authorized` tinyint(4) NOT NULL,
  `title` varchar(30) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `sex` enum('M','F') DEFAULT NULL,
  `federaltaxid` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `occupation` varchar(50) DEFAULT NULL,
  `organisation` varchar(255) DEFAULT NULL,
  `street` varchar(200) DEFAULT NULL,
  `streetb` varchar(200) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `nextofkinfullname` varchar(200) DEFAULT NULL,
  `nextofkinstreet2` varchar(200) DEFAULT NULL,
  `nextofkinstreet2b` varchar(200) DEFAULT NULL,
  `nextofkincity2` varchar(200) DEFAULT NULL,
  `nextofkinstate2` varchar(50) DEFAULT NULL,
  `nextofkinzip2` varchar(20) DEFAULT NULL,
  `nextofkinphone` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `profileimage` varchar(30) DEFAULT NULL,
  `newsletter_period` char(50) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  UNIQUE KEY `username` (`username`),
  KEY `FK_record_role` (`role_type`),
  CONSTRAINT `FK_record_role` FOREIGN KEY (`role_type`) REFERENCES `role` (`role_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Dumping data for table allsmiles.record: ~5 rows (approximately)
/*!40000 ALTER TABLE `record` DISABLE KEYS */;
INSERT INTO `record` (`record_id`, `role_type`, `username`, `password`, `authorized`, `title`, `fname`, `mname`, `lname`, `dob`, `sex`, `federaltaxid`, `phone`, `fax`, `email`, `url`, `occupation`, `organisation`, `street`, `streetb`, `city`, `state`, `zip`, `nextofkinfullname`, `nextofkinstreet2`, `nextofkinstreet2b`, `nextofkincity2`, `nextofkinstate2`, `nextofkinzip2`, `nextofkinphone`, `country`, `profileimage`, `newsletter_period`) VALUES
	(20, 1, 'tobi', 'tobi', 1, 'Mr', 'tobi', 'tobi', 'tobi', '0000-00-00', NULL, NULL, '534534', NULL, 'a@a.com', NULL, NULL, NULL, 'tobi', 'tobi', 'tobi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Nigeria', '', NULL),
	(21, 3, 'ketsia', 'ketsia', 1, 'Mr', 'ketsia', 'ketsia', 'ketsia', '0000-00-00', NULL, NULL, '24324', NULL, 'b@b.com', NULL, NULL, NULL, '2342sad', 'ketsiaketsiaketsia', 'ketsia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Nigeria', '', NULL),
	(22, 5, 'receptionist', 'receptionist', 1, 'Mr', 'Receptionist', 'M', 'Receptionist', '2006-08-17', 'M', '89787', '32423423', '879', 'toby4ever@jhb.com', NULL, 'uhkj', 'jhgb', 'receptionist', 'receptionist', 'receptionist', NULL, NULL, '', '', '', '', NULL, NULL, '', 'Mauritius', '1364802398.jpg', NULL),
	(23, 3, 'mstphly', 'dontchusee', 1, 'Mr', 'Mustapha', 'M M A', 'MUhammad', '1991-01-09', 'M', NULL, '9888626', NULL, 'info@allsmilesclinic.com', NULL, NULL, NULL, 'No.11 Impasse Argentine, Remy Ollier, Bonne Terre', '', 'Vacoas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mauritius', '', NULL),
	(24, 2, 'orthondon1', 'orthondon1', 1, 'Mr', 'Roger', 'Philip', 'Gold', '1973-05-11', 'M', NULL, '324234353', NULL, 'd@d.com', NULL, NULL, NULL, 'Philip Roger Gold Affairs', 'Philip Roger Gold Affairs', 'Philip Roger Gold Affairs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mauritius', '', NULL);
/*!40000 ALTER TABLE `record` ENABLE KEYS */;


-- Dumping structure for table allsmiles.record_status
DROP TABLE IF EXISTS `record_status`;
CREATE TABLE IF NOT EXISTS `record_status` (
  `record_id` int(10) unsigned NOT NULL,
  `record_status` set('online','offline','idle','invisible') NOT NULL,
  `record_seen_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table allsmiles.record_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `record_status` DISABLE KEYS */;
INSERT INTO `record_status` (`record_id`, `record_status`, `record_seen_date`) VALUES
	(20, '', '2013-04-10 21:03:46'),
	(20, '', '2013-04-10 21:03:47'),
	(20, '', '2013-04-10 21:04:53'),
	(20, '', '2013-04-10 21:04:54'),
	(20, '', '2013-04-10 21:06:36'),
	(20, '', '2013-04-10 21:06:37'),
	(20, '', '2013-04-10 21:08:32'),
	(20, '', '2013-04-10 21:08:33'),
	(20, '', '2013-04-10 21:08:58'),
	(20, '', '2013-04-10 21:08:59'),
	(20, '', '2013-04-10 21:11:06'),
	(20, '', '2013-04-10 21:11:07'),
	(20, '', '2013-04-10 21:13:13'),
	(20, '', '2013-04-10 21:13:14'),
	(20, '', '2013-04-10 21:13:47'),
	(20, '', '2013-04-10 21:13:48'),
	(20, '', '2013-04-10 21:18:10'),
	(20, '', '2013-04-10 21:18:11'),
	(20, '', '2013-04-10 21:18:50'),
	(20, '', '2013-04-10 21:18:51'),
	(20, '', '2013-04-10 21:20:34'),
	(20, '', '2013-04-10 21:20:35'),
	(20, '', '2013-04-10 21:20:36'),
	(20, '', '2013-04-10 21:20:37'),
	(20, '', '2013-04-10 21:20:38'),
	(20, '', '2013-04-10 21:20:39'),
	(20, '', '2013-04-10 21:20:40'),
	(20, '', '2013-04-10 21:20:41'),
	(20, '', '2013-04-10 21:20:42'),
	(20, '', '2013-04-10 21:20:43'),
	(20, '', '2013-04-10 21:20:44'),
	(20, '', '2013-04-10 21:20:46'),
	(20, '', '2013-04-10 21:20:48'),
	(20, '', '2013-04-10 21:20:50'),
	(20, '', '2013-04-10 21:20:53'),
	(20, '', '2013-04-10 21:20:55'),
	(20, '', '2013-04-10 21:20:57'),
	(20, '', '2013-04-10 21:20:59'),
	(20, '', '2013-04-10 21:21:01'),
	(20, '', '2013-04-10 21:21:03'),
	(20, '', '2013-04-10 21:21:07'),
	(20, '', '2013-04-10 21:21:11'),
	(20, '', '2013-04-10 21:21:15'),
	(20, '', '2013-04-10 21:21:19');
/*!40000 ALTER TABLE `record_status` ENABLE KEYS */;


-- Dumping structure for table allsmiles.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `uniq` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table allsmiles.role: ~5 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`role_id`, `name`) VALUES
	(2, 'doctor'),
	(4, 'normaluser'),
	(3, 'patientuser'),
	(5, 'receptionist'),
	(1, 'superadmin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- Dumping structure for table allsmiles.specialties
DROP TABLE IF EXISTS `specialties`;
CREATE TABLE IF NOT EXISTS `specialties` (
  `specialist_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`specialist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table allsmiles.specialties: ~4 rows (approximately)
/*!40000 ALTER TABLE `specialties` DISABLE KEYS */;
INSERT INTO `specialties` (`specialist_id`, `title`, `description`) VALUES
	(1, 'Orthondontists', NULL),
	(2, 'Anesthetic', NULL),
	(3, 'Dental Assistant', NULL),
	(4, 'Dentist', NULL);
/*!40000 ALTER TABLE `specialties` ENABLE KEYS */;


-- Dumping structure for table allsmiles.subscription_list
DROP TABLE IF EXISTS `subscription_list`;
CREATE TABLE IF NOT EXISTS `subscription_list` (
  `sl_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subscription_id` int(10) unsigned NOT NULL,
  `record_id` int(10) unsigned NOT NULL,
  `activated` datetime DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `status` char(50) DEFAULT 'NOT ACTIVE',
  PRIMARY KEY (`sl_id`),
  KEY `FK_subscription_list_subscription_package` (`subscription_id`),
  CONSTRAINT `FK_subscription_list_subscription_package` FOREIGN KEY (`subscription_id`) REFERENCES `subscription_package` (`sp_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table allsmiles.subscription_list: ~0 rows (approximately)
/*!40000 ALTER TABLE `subscription_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription_list` ENABLE KEYS */;


-- Dumping structure for table allsmiles.subscription_package
DROP TABLE IF EXISTS `subscription_package`;
CREATE TABLE IF NOT EXISTS `subscription_package` (
  `sp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sp_title` varchar(50) NOT NULL,
  `sp_type` varchar(50) NOT NULL,
  `sp_duration` int(10) unsigned NOT NULL,
  `sp_amount` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table allsmiles.subscription_package: ~2 rows (approximately)
/*!40000 ALTER TABLE `subscription_package` DISABLE KEYS */;
INSERT INTO `subscription_package` (`sp_id`, `sp_title`, `sp_type`, `sp_duration`, `sp_amount`) VALUES
	(1, 'Monthly', 'Basic', 31, 5000),
	(2, 'Family Package', 'Prestige', 60, 9000);
/*!40000 ALTER TABLE `subscription_package` ENABLE KEYS */;


-- Dumping structure for table allsmiles.transaction
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `trans_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `record_id` int(10) unsigned NOT NULL,
  `trans_name` text NOT NULL,
  `trans_no` varchar(255) NOT NULL DEFAULT '',
  `trans_amount` decimal(10,2) NOT NULL,
  `trans_date` datetime NOT NULL,
  `generated_date` datetime NOT NULL,
  `trans_year` year(4) NOT NULL,
  `trans_status` set('Paid','Not Paid') NOT NULL DEFAULT 'Not Paid',
  PRIMARY KEY (`trans_id`),
  KEY `user_id` (`record_id`),
  KEY `trans_no` (`trans_no`),
  CONSTRAINT `FK_transaction_record` FOREIGN KEY (`record_id`) REFERENCES `record` (`record_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table allsmiles.transaction: ~0 rows (approximately)
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;


-- Dumping structure for table allsmiles.treatment
DROP TABLE IF EXISTS `treatment`;
CREATE TABLE IF NOT EXISTS `treatment` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `treatment_title` char(50) NOT NULL,
  `specialist_id` int(10) unsigned NOT NULL,
  `treatment_price` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tid`),
  KEY `FK_treatment_specialties` (`specialist_id`),
  CONSTRAINT `FK_treatment_specialties` FOREIGN KEY (`specialist_id`) REFERENCES `specialties` (`specialist_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- Dumping data for table allsmiles.treatment: ~28 rows (approximately)
/*!40000 ALTER TABLE `treatment` DISABLE KEYS */;
INSERT INTO `treatment` (`tid`, `treatment_title`, `specialist_id`, `treatment_price`) VALUES
	(1, 'Base Chart', 4, 300),
	(2, 'Bridge Prep', 4, 300),
	(3, 'Conservation', 4, 300),
	(4, 'Crown Addition', 4, 300),
	(5, 'Crown Fit', 4, 300),
	(6, 'Crown Prep', 4, 300),
	(7, 'Denture Bite', 4, 300),
	(8, 'Denture Fit', 4, 300),
	(9, 'Denture Imps', 4, 300),
	(10, 'Denture Try In', 4, 300),
	(11, 'Emergency Broken Teeth', 4, 300),
	(12, 'Emergency Lost Fill', 4, 300),
	(13, 'Emergency Swelling', 4, 300),
	(14, 'Emergency Toothache', 4, 300),
	(15, 'Examination', 4, 300),
	(16, 'Extraction', 4, 300),
	(17, 'Filling', 4, 300),
	(18, 'Non-Surgical', 4, 300),
	(19, 'Other', 4, 300),
	(20, 'Periodontal', 4, 300),
	(21, 'SP', 4, 300),
	(22, 'Static Chart', 4, 300),
	(23, 'Surgical', 1, 300),
	(24, 'Surgical Extraction', 1, 300),
	(25, 'Tooth Whitening', 4, 300),
	(26, 'Veneer Fit', 1, 300),
	(27, 'Veneer Prep', 4, 300),
	(28, 'Regular Checkup', 3, 100);
/*!40000 ALTER TABLE `treatment` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

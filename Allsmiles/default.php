<?php


	/* Components Setup */
		 require_once 'Components/func.loader.php';
		 require_once 'Components/func.timezone.php';
		 require_once 'Components/config.php'; 
		 require_once 'CMS/cms_api.php';
		 require_once 'Views/__menuTop.php';
	/* Components Setup */
	
	
	switch( $appSettings['appLanguage'] )
	{
		case 'fr':
			setLang( 'fr' );
		break;
		case 'en':
			setLang();
		break;		
	}

//ob_start('app_guard');




echo <<<DOCTYPE
	<!DOCTYPE html>
	<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
	<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
	<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
DOCTYPE;

echo <<<HEAD
	<head>
		<meta charset="utf-8" />		
		<title>Al-aam Engineering Services LTD</title>
		<link rel="stylesheet" type="text/css" href="assets/build/css/style.css">
		<link rel="stylesheet" type="text/css" href="assets/build/css/boxed.css" id="layout">
		<link rel="stylesheet" type="text/css" href="assets/build/css/colors/green.css" id="colors">
		<link rel="shortcut icon" href="favicon.ico" />
		<script src="assets/build/js/jquery.min.js"></script>
		<script src="assets/build/js/custom.js"></script>
		<script src="assets/build/js/selectnav.js"></script>
		<script src="assets/build/js/flexslider.js"></script>
		<script src="assets/build/js/twitter.js"></script>
		<script src="assets/build/js/tooltip.js"></script>
		<script src="assets/build/js/effects.js"></script>
		<script src="assets/build/js/fancybox.js"></script>
		<script src="assets/build/js/carousel.js"></script>
		<script src="assets/build/js/isotope.js"></script>		
HEAD;
	echo '
		<script type="text/javascript">
			$(function(){
				$(".dropdown img.flag").addClass("flagvisibility");
				$(".dropdown dt a").click(function () {
					$(".dropdown dd ul").toggle();
				});
				$(".dropdown dd ul li a").click(function () {
					var text = $(this).html();
					$(".dropdown dt a span").html(text);
					$(".dropdown dd ul").hide();
					$("#result").html("Selected value is: " + getSelectedValue("sample"));
				});
				function getSelectedValue(id) {
					return $("#" + id).find("dt a span.value").html();
				}
				$(document).bind(\'click\', function (e) {
					var $clicked = $(e.target);
					if (!$clicked.parents().hasClass("dropdown"))
						$(".dropdown dd ul").hide();
				});
				$("#flagSwitcher").click(function () {
					$(".dropdown img.flag").toggleClass("flagvisibility");
				});
			});
		</script>
</head>		
	';


	
	if( isset($_GET['pg']) )
	{

		function shangai_cut( $content )
		{
			return str_replace('src="media/', 'src="CMS/media/', $content );
		}												

		$displayContent = '';
		$sect = isset( $_GET['sect'] ) && !is_nan($_GET['sect']) ? $_GET['sect'] : false;
		$spg = isset( $_GET['spg'] ) && !is_nan($_GET['spg']) ? $_GET['spg'] : false;
		$ex = isset( $_GET['ex'] ) ? $_GET['ex'] : false;
		$content = getDisplayContent( $_GET['pg'], $spg, $sect, $ex );
	
		switch( $_GET['pg'] )
		{
			case 100:
				// Contact Us
				
			break;
			default:
				// Other Pages

				foreach( $content as $pageBody )
				{
					$displayContent .= '
										<div class="container">
											<div class="sixteen columns">

											  <div id="page-title">
												<h2>'.$pageBody['title'].'</h2>
												<div id="bolded-line"></div>
											  </div>
											</div>
										  </div>';
										  
					$displayContent .= '
									  <div class="container">
										<!-- 12 Columns -->
										<div class="twelve columns">
												<div class="" style="border-right:none;">'.shangai_cut($pageBody['content']).'</div>
										</div>
										<div class="four columns no-margin">
											   <div class="widget no-margin" style="margin-top:0!important">
												  <div class="headline no-margin">
													<h4>Popular Posts</h4>
												  </div>
												  <div class="latest-post-blog"> <a href="#"><img src="assets/build/images/popular-post-01.png" alt="" /></a>
													<p><a href="#">Maecenas metus lorem nulla, pretium lipsum.</a> <span>12 August 2012</span></p>
												  </div>
												  <div class="latest-post-blog"> <a href="#"><img src="assets/build/images/popular-post-02.png" alt="" /></a>
													<p><a href="#">Tetus lorem maecenas facili lipsum pretium.</a> <span>26 July 2012</span></p>
												  </div>
												  <div class="latest-post-blog"> <a href="#"><img src="assets/build/images/popular-post-03.png" alt="" /></a>
													<p><a href="#">Lorem pretium metusnula lorem ipsum dolor.</a> <span>16 June 2012</span></p>
												  </div>
												</div>
												<!-- Social -->
												<div class="widget">
												  <div class="headline no-margin">
													<h4>Social</h4>
												  </div>
												  <div id="social" class="tooltips"> <a href="#" rel="tooltip" title="Facebook" class="facebook">Facebook</a> <a href="#" rel="tooltip" title="Evernote" class="evernote">Evernote</a> <a href="#" rel="tooltip" title="LinkedIn" class="linkedin">LinkedIn</a> <a href="#" rel="tooltip" title="Google Plus" class="googleplus">Google Plus</a> <a href="#" rel="tooltip" title="Vimeo" class="vimeo">Vimeo</a> </div>
												  <div class="clear"></div>
												</div>
											</div>
									  </div>					
					';
					
				}
			break;
		}
		
	} else
	{
		$displayContent = <<<HOMEPAGE
							<div class="container">

								<!-- Flexslider -->
								<div class="sixteen columns">
									<section class="slider">
										<div class="flexslider home">
											<ul class="slides">
											
												<li>
													<img src="images/1.jpg" alt="" />
													<div class="slide-caption n">
														<h3>Construction</h3>
														<p> Our constructional services are; buildings including bungalows, duplexes, multi- story building, and factories, civil engineering structures including earth and water retaining structures, drainages and pipelines.</p>
													</div>
												</li>
												
												<li>
													<img src="images/2.jpg" alt="" />
													<div class="slide-caption">
														<h3>Engineering Consultancy </h3>
														<p>Geotechnical services, Appraisal of existing Facilities, Structures, water supply and Drainages, electrical and Mechanical services.</p>
													</div>
												</li>
												
												<li>
													<img src="images/3.jpg" alt="" />
													<div class="slide-caption">
														<h3>Computer Services</h3>
														<p>The firm provides a wide range of computer services and is involved in system design, analysis, and software development, both wired and wireless network design and deployment, to support its operations. The firm has the following computer hardware and software exclusively for its use.</p>
													</div>
												</li>
												
											</ul>
										</div>
									</section>
								</div>
								<!-- Flexslider / End -->
								
							</div>
		
		
		
			<div class="container">

				<div class="sixteen columns">
					<!-- Headline -->
					<div class="headline no-margin">
				  <h3>Our Services</h3></div>
				</div>
				
				<!-- Project -->
				<div class="four columns">
					<div class="picture"><a href="#"><img src="images/4.jpg" alt=""/><div class="image-overlay-link"></div></a></div>
					<div class="item-description">
						<h5><a href="#">Quantity Surveying & Project Cost Consultancy</a></h5>
						<p>Planning, Control and appraisal, Preparation and evaluation of tender documents and Contract arbitration.  </p>
					</div>
				</div>
				
				<!-- Project -->
				<div class="four columns">
					<div class="picture"><a href="#"><img src="images/5.jpg" alt=""/><div class="image-overlay-link"></div></a></div>
					<div class="item-description">
						<h5><a href="#">Project Management</a></h5>
						<p>Supervision and cost control development projects, Provision of Administrative service, financial and viability re-appraisal of existing structures.</p>
					</div>
				</div>
				
				
			</div>			
HOMEPAGE;
	}

echo <<<BODYSTART
	<body>
BODYSTART;
	$GLOBALS['appSettings']['cache'] = false;
	
	echo <<<ENDBODYHTML

	{$pageProperties['headContent']}

	{$displayContent}

	{$pageProperties['footContent']}
	</body></html>
ENDBODYHTML;

//ob_end_flush();

?>
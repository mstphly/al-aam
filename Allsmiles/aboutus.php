<?php


	/* Components Setup */
		 require_once 'Components/func.loader.php';
		 require_once 'Components/func.timezone.php';
		 require_once 'Components/config.php'; 
		 require_once 'CMS/cms_api.php';
		 require_once 'Views/__menuTop.php';
	/* Components Setup */
	
	
	switch( $appSettings['appLanguage'] )
	{
		case 'fr':
			setLang( 'fr' );
		break;
		case 'en':
			setLang();
		break;		
	}

//ob_start('app_guard');




echo <<<DOCTYPE
	<!DOCTYPE html>
	<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
	<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
	<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
DOCTYPE;

echo <<<HEAD
	<head>
		<meta charset="utf-8" />		
		<title>{$pageProperties['pageTitle']}</title>
		<link rel="stylesheet" type="text/css" href="assets/build/css/style.css">
		<link rel="stylesheet" type="text/css" href="assets/build/css/boxed.css" id="layout">
		<link rel="stylesheet" type="text/css" href="assets/build/css/colors/green.css" id="colors">
		<link rel="shortcut icon" href="favicon.ico" />
		<script src="assets/build/js/jquery.min.js"></script>
		<script src="assets/build/js/custom.js"></script>
		<script src="assets/build/js/selectnav.js"></script>
		<script src="assets/build/js/flexslider.js"></script>
		<script src="assets/build/js/twitter.js"></script>
		<script src="assets/build/js/tooltip.js"></script>
		<script src="assets/build/js/effects.js"></script>
		<script src="assets/build/js/fancybox.js"></script>
		<script src="assets/build/js/carousel.js"></script>
		<script src="assets/build/js/isotope.js"></script>		
HEAD;
	echo '
		<script type="text/javascript">
			$(function(){
				$(".dropdown img.flag").addClass("flagvisibility");
				$(".dropdown dt a").click(function () {
					$(".dropdown dd ul").toggle();
				});
				$(".dropdown dd ul li a").click(function () {
					var text = $(this).html();
					$(".dropdown dt a span").html(text);
					$(".dropdown dd ul").hide();
					$("#result").html("Selected value is: " + getSelectedValue("sample"));
				});
				function getSelectedValue(id) {
					return $("#" + id).find("dt a span.value").html();
				}
				$(document).bind(\'click\', function (e) {
					var $clicked = $(e.target);
					if (!$clicked.parents().hasClass("dropdown"))
						$(".dropdown dd ul").hide();
				});
				$("#flagSwitcher").click(function () {
					$(".dropdown img.flag").toggleClass("flagvisibility");
				});
			});
		</script>
</head>		
	';


	
	if( isset($_GET['pg']) )
	{

		function shangai_cut( $content )
		{
			return str_replace('src="media/', 'src="CMS/media/', $content );
		}												

		$displayContent = '';
		$sect = isset( $_GET['sect'] ) && !is_nan($_GET['sect']) ? $_GET['sect'] : false;
		$spg = isset( $_GET['spg'] ) && !is_nan($_GET['spg']) ? $_GET['spg'] : false;
		$ex = isset( $_GET['ex'] ) ? $_GET['ex'] : false;
		$content = getDisplayContent( $_GET['pg'], $spg, $sect, $ex );
	
		switch( $_GET['pg'] )
		{
			case 100:
				// Contact Us
				
			break;
			default:
				// Other Pages

				foreach( $content as $pageBody )
				{
					$displayContent .= '
										<div class="container">
											<div class="sixteen columns">

											  <div id="page-title">
												<h2>'.$pageBody['title'].'</h2>
												<div id="bolded-line"></div>
											  </div>
											</div>
										  </div>';
										  
					$displayContent .= '
									  <div class="container">
										<!-- 12 Columns -->
										<div class="twelve columns">
												<div class="" style="border-right:none;">'.shangai_cut($pageBody['content']).'</div>
										</div>
										<div class="four columns no-margin">
											   <div class="widget no-margin" style="margin-top:0!important">
												  <div class="headline no-margin">
													<h4>Popular Posts</h4>
												  </div>
												  <div class="latest-post-blog"> <a href="#"><img src="assets/build/images/popular-post-01.png" alt="" /></a>
													<p><a href="#">Maecenas metus lorem nulla, pretium lipsum.</a> <span>12 August 2012</span></p>
												  </div>
												  <div class="latest-post-blog"> <a href="#"><img src="assets/build/images/popular-post-02.png" alt="" /></a>
													<p><a href="#">Tetus lorem maecenas facili lipsum pretium.</a> <span>26 July 2012</span></p>
												  </div>
												  <div class="latest-post-blog"> <a href="#"><img src="assets/build/images/popular-post-03.png" alt="" /></a>
													<p><a href="#">Lorem pretium metusnula lorem ipsum dolor.</a> <span>16 June 2012</span></p>
												  </div>
												</div>
												<!-- Social -->
												<div class="widget">
												  <div class="headline no-margin">
													<h4>Social</h4>
												  </div>
												  <div id="social" class="tooltips"> <a href="#" rel="tooltip" title="Facebook" class="facebook">Facebook</a> <a href="#" rel="tooltip" title="Evernote" class="evernote">Evernote</a> <a href="#" rel="tooltip" title="LinkedIn" class="linkedin">LinkedIn</a> <a href="#" rel="tooltip" title="Google Plus" class="googleplus">Google Plus</a> <a href="#" rel="tooltip" title="Vimeo" class="vimeo">Vimeo</a> </div>
												  <div class="clear"></div>
												</div>
											</div>
									  </div>					
					';
					
				}
			break;
		}
		
	} else
	{
		$displayContent = <<<HOMEPAGE
							<div class="container">

								<!-- Flexslider -->
								<div class="sixteen columns">
									<section class="slider">
										<div class="flexslider home">
											<ul class="slides">
											
												<li>
													<img src="assets/build/images/slider-img-01.jpg" alt="" />
													<div class="slide-caption n">
														<h3>Teeth Whitening</h3>
														<p>Teeth whitening has become one of the most popular ways to enhance your smile. At The Dental Clinic, we offer a range of teeth whitening options and our qualified dental professionals will provide you with all the facts you need to make an informed decision about your teeth.</p>
													</div>
												</li>
												
												<li>
													<img src="assets/build/images/slider-img-02.jpg" alt="" />
													<div class="slide-caption">
														<h3>Orthodontist and Customer</h3>
														<p>We see our customers as our price possessions, we value you, we care for your teeths and that why we enable direct communication between you and our professionals</p>
													</div>
												</li>
												
												<li>
													<img src="assets/build/images/slider-img-03.jpg" alt="" />
													<div class="slide-caption">
														<h3>Family Care Package</h3>
														<p>There is a family care for all. You want a continuous smiling family package that caters for all your loved ones tooths. Allsmiles is here to deliver just that to every family at a very affordable price</p>
													</div>
												</li>
												
											</ul>
										</div>
									</section>
								</div>
								<!-- Flexslider / End -->
								
							</div>
		
		
		
			<div class="container">

				<div class="sixteen columns">
					<!-- Headline -->
					<div class="headline no-margin">
				  <h3>Our Services</h3></div>
				</div>
				
				<!-- Project -->
				<div class="four columns">
					<div class="picture"><a href="#"><img src="assets/build/images/portfolio/portoflio-09.jpg" alt=""/><div class="image-overlay-link"></div></a></div>
					<div class="item-description">
						<h5><a href="#">Teeth Whitening</a></h5>
						<p>At Allsmiles, we aim to offer you a full range of dental treatments.  </p>
					</div>
				</div>
				
				<!-- Project -->
				<div class="four columns">
					<div class="picture"><a href="#"><img src="assets/build/images/portfolio/portoflio-08.jpg" alt=""/><div class="image-overlay-link"></div></a></div>
					<div class="item-description">
						<h5><a href="#">Family Subscription Packages</a></h5>
						<p>There is a place for every one including your family.</p>
					</div>
				</div>
				
				<!-- Project -->
				<div class="four columns">
					<div class="picture"><a href="#"><img src="assets/build/images/portfolio/portoflio-10.jpg" alt=""/><div class="image-overlay-link"></div></a></div>
					<div class="item-description">
						<h5><a href="#">General Dentistry</a></h5>
						<p>We bring the our dentistry to you and your loved ones.</p>
					</div>
				</div>
				
				<!-- Project -->
				<div class="four columns">
					<div class="picture"><a href="#"><img src="assets/build/images/portfolio/portoflio-07.jpg" alt=""/><div class="image-overlay-link"></div></a></div>
					<div class="item-description">
						<h5><a href="#">Cosmetic Dentistry</a></h5>
						<p>At Allsmiles each treatment is customised to your individual wants.</p>
					</div>
				</div>
				
			</div>			
HOMEPAGE;
	}

echo <<<BODYSTART
	<body>
BODYSTART;
	$GLOBALS['appSettings']['cache'] = false;
	
	echo <<<ENDBODYHTML

	{$pageProperties['headContent']}

	{$displayContent}

	{$pageProperties['footContent']}
	</body></html>
ENDBODYHTML;


//ob_end_flush();

?>
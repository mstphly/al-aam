<?php

/**
 * @author Dodit Suprianto
 * @re-engineered Ojutiku Oluwatobi(Peace and Love)
 * Paginator is Toned Downed For API use not for normal page transport
 */


class CSSPagination {

	private static 
		$totalrows = 0,
		$rowsperpage,
		$website,
		$page,
		$sql,
		$db,
		$lastpage;
		

    function __construct($sql, $rowsperpage, $db) {
        self::$sql = $sql;
        self::$rowsperpage = $rowsperpage;
		self::$db = $db;
		self::getTotalRows();
    }
	
	
	public function return_cur_loaded( $t_row, $rpp, $pg )
	{
	  if( ($rpp * $pg) > $t_row ) return $t_row;
	  else
		  return $rpp >= $t_row ? $t_row : $rpp * $pg;
	}
	
	
    public function getLimit() {
        return ( self::$page - 1 ) * self::$rowsperpage;
    }
	
	
	public static function setPage($page) {
        self::$page = $page;
    }
		
	
	public function totalrow() {
		return (int)self::$totalrows;
	}
	
				
        private static function getTotalRows() {
			if( false === stripos(self::$sql, 'group by') )
			{
				self::$sql = 'SELECT count(1) as total '.substr(self::$sql, stripos(self::$sql, 'FROM') );
				$r = mysqli_query( self::$db, self::$sql );
				if( $r )
				{
					$f = mysqli_fetch_object($r);
					self::$totalrows = $f->total;
					unset($f);
					mysqli_free_result($r);
				}

			} else
			{
				self::$sql = 'SELECT count(1) as total '.substr(self::$sql, stripos(self::$sql, 'FROM') );
				$r = mysqli_query( self::$db, self::$sql );
				if( $r )
				{
					self::$totalrows = mysqli_num_rows($r);
					mysqli_free_result($r);
				}				
			}
        }
		
		
		private static function getLastPage() {
            self::$lastpage = ceil(self::$totalrows / self::$rowsperpage);
			return self::$lastpage;
        }
		
		
		public function returnLastPage() {
			return ceil(self::$totalrows / self::$rowsperpage);
		}
		
		
}


?>

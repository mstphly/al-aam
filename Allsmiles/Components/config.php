<?php
	
	// database connection config
	$dbHost = '127.0.0.1';
	$dbUser= 'allsmiles_oxygen';
	$dbPass = '!periodictable1';
	$dbName='allsmiles';
	
	$appSettings = array();
	
	$appSettings['cache'] = true;
	$appSettings['portalMail'] = 'info@allsmilesclinic.com';
	$appSettings['portalName'] = 'AllSmiles Dental Clinic';
	$appSettings['webRoot'] = $_SERVER['DOCUMENT_ROOT'].'/allsmiles/';
	$appSettings['appLanguage'] = 'en';
	
	
	$appSettings['cacheDir_WP'] = $appSettings['webRoot'].'/Cache/254fdsg45ygdfye546sdfgs432sd-WebPages/';
	
	$pageProperties = array();
	
	$dbTable['record'] = 'record';

	define('THIS', dirname(__FILE__).'/');
		


	// start the session and support backward in portal
	if( !isset($_SESSION) )
		session_start();	
		
	if( isset($_GET['lang'][1]) )
	{
		$_SESSION['__lang'] = $_GET['lang'];	
	}		

	if( isset($_SESSION['__lang'][1]) )
	{
		$appSettings['appLanguage'] = $_SESSION['__lang'];
	}		

	require_once THIS.'database.php';
	require_once THIS.'func.common.php';
	require_once THIS.'func.loader.php';
	require_once THIS.'func.lang.php';			
			
	/* This auto addslahes to all your input:// data */	
		if (get_magic_quotes_gpc()) {
			$in = array(&$_GET, &$_POST, &$_COOKIE);
			while (list($k,$v) = each($in)) {
				foreach ($v as $key => $val) {
					if (!is_array($val)) {
					$in[$k][$key] = stripslashes($val);
					continue;
				}
				$in[] =& $in[$k][$key];
				}
			}
			unset($in);
		}


		if (isset($_POST)) {
			
			foreach ($_POST as $key => $value) {
				
				if( is_array($value) )
				{
					foreach( $value as $k=>$v )
					{
						//$_POST[$key][$k] = filter_var( $v, FILTER_SANITIZE_STRIPPED);
						$_POST[$key][$k] = trim(addslashes($_POST[$key][$k]));
						$_POST[$key][$k] = dbEscape( $_POST[$key][$k] );
					}
				} else
				{
					//$_POST[$key] = filter_var( $value, FILTER_SANITIZE_STRIPPED);
					$_POST[$key] = trim(addslashes($_POST[$key]));
					$_POST[$key] = dbEscape( $_POST[$key] );
				}
				
			}
						
		}
		
		if (isset($_GET)) {
			foreach ($_GET as $key => $value) {
				$_GET[$key] = filter_var( $value, FILTER_SANITIZE_URL);
				$_GET[$key] = trim(addslashes($_GET[$key]));
			}
		}
		

		function checkSessionStatus()
		{
			if( !isset($_SESSION['activated']) )
			{
				unset( $_SESSION );
				header('Location: index.php');
				exit;
			}
		}
		
		
		function roleType( $id )
		{
			switch( $id )
			{
				case 1:
					return 'Super-Administrator';
				break;
				case 2:
					return 'Doctor';
				break;
				case 3:
					return 'Patient';
				break;
				case 4:
					return 'Normal User';
				break;
				case 5:
					return 'Receptionist';
				break;
			}
		}
	
	
	
	// NB: New Breeding Feature (Beta Stage)
		$cachetime = 300; // In seconds
		$cachefilename = urlencode($_SERVER['REQUEST_URI']).'-'.md5($_SERVER['REQUEST_URI']);
		$cachefile = $GLOBALS['appSettings']['cacheDir_WP'].$cachefilename.'.cache';
		
		function isCached()
		{
			
			if( is_file($GLOBALS['cachefile']) && $_SERVER['REQUEST_TIME'] - $GLOBALS['cachetime'] < filemtime($GLOBALS['cachefile']) )
			{
				return true;
			}
				return false;					
		}
		
		
		
		function serveCached()
		{
			return file_get_contents( $GLOBALS['cachefile'] );
		}
		
		
		function cacheOutput( $buffer )
		{
			
			if( $fp = fopen($GLOBALS['cachefile'], 'w') )
			{
				if( flock($fp, LOCK_EX) )
				{ // do an exclusive lock
					ftruncate($fp, 0); // truncate file
					//compress unnecesary spaces
					$busca = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
					$reemplaza = array('>','<','\\1');
					$webpage = preg_replace($busca, $reemplaza, $buffer);
					
					fwrite($fp, $webpage);
					fclose($fp);
				}
			}
						
		}



		if( isset($_GET[secureUrl::$var_name]) )
		{
			secureUrl::decode();
		}
		
		function app_guard( $buffer )
		{
			
			if( $GLOBALS['appSettings']['cache'] == true )
			{ // Caching Is Enabled
				if( isCached() )
				{
					return serveCached();
				} else
				{
					$buffer = secureUrl::encode( $buffer );
					cacheOutput( $buffer );
					return $buffer;				
				}
			} else
			{
				$buffer = secureUrl::encode( $buffer );				
				return $buffer;
			}

		}
		
		
		function foreceSessionReset( $sess='' )
		{
			
			$sess = empty($sess) ? $_SESSION : $sess;
			foreach( $sess as $k=>$arr )
			{
				if( in_array($arr) )
				{
					forceSessionReset( $arr );
				} else
				{
					unset($sess[$k]); 
				}
			}
									
		}
	

	
	$pageTitle="AllSmiles Dental Clinic";
	$pageProperties['pageTitle'] = $pageTitle;
	$pageProperties['metadata'] = '<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta content="" name="description" /><meta content="" name="author" />';
	$pageProperties['css'] = '';
	$pageProperties['jsHead'] = '';
	$pageProperties['jsFoot'] = '';
	$pageProperties['headContent'] = '';
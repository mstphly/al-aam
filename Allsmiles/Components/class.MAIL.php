<?php

class MAIL {

	
	private static  $is_m_err = '',
					$return_error = array(),
					$mfrom = false,
					$mto = array(),
					$mbc = array(),
					$mcc = array(),
					$mreply = array(),
					$msubj = '',
					$mbdy = '',
					$mprior = false,
					$mcharset = false,
					$mencoding = false,
					$mtype = false,
					$m_header = false,
					$mm = '';

	
	private function __constructor(){ }
	
/*---------------Mail Validator Crew---------------*/
	private static function is_mail($em){
		return preg_match("/^[[:alnum:]._-]+@[[:alnum:]._-]+.[[:alpha:]]{2,6}$/i", $em) ? true : false;
	}
	
	private static function is_mail_server(){
	}
/*---------------Mail Validator Crew---------------*/
	
	
	
	
/*---------------Mail Composer Crew---------------*/

	private static function mail_to($m){
		if( is_array($m) ){ foreach($m as $mail){ self::mail_to($mail); } }
		else { self::is_mail($m) === false ? self::$is_m_err = 'error-to' : self::$mto[] = $m; }
	}
	
	private static function mail_from( $m ){
		if( is_array($m) )
			self::$mfrom = array($m[0], $m[1]); 
		else 
			self::is_mail($m) === false ? self::$is_m_err = 'error-from' : self::$mfrom = $m; 
	}
	
	private static function mail_bc( $m ){
		if( is_array($m) ){ foreach($m as $mail){ self::mail_bc($mail); } }
		else { self::is_mail($m) === false ? self::$is_m_err = 'error-bc' : self::$mbc[] = $m; }
	}
	
	private static function mail_cc( $m ){
		if( is_array($m) ){ foreach($m as $mail){ self::mail_cc($mail); } }
		else { self::is_mail($m) == false ? self::$is_m_err = 'error-cc' : self::$mcc[] = $m; }
	}
	
	private static function mail_reply( $m ){
		self::is_mail($m) === false ? self::$is_m_err = 'error-reply' : self::$mreply = $m;				
	}
	
	
	private static function resetall() {
		self::$is_m_err = '';
		self::$return_error = array();
		
		self::$mfrom = false;
		self::$mto = array();
		self::$mbc = array();
		self::$mcc = array();
		self::$mreply = false;
		
		self::$msubj = '';
		self::$mbdy = '';
		
		self::$mprior = false;
		self::$mcharset = false;
		self::$mencoding = false;
		self::$mtype = false;
		self::$m_header = false;
	}
	
	
	private static function mail_subject( $s ){
		self::$msubj = strtr($s, "/\r/\n", "  ");
	}
	
	private static function mail_body($bdy='', $char='UTF-8'){
		self::$mbdy = $bdy;
		self::$mcharset = empty($char) ? 'iso-8859-1' : strtolower($char);	
	}
	
	private static function mail_priority($num){
		if( is_int($num) && $num>=1 && $num<=5 ) 
			self::$mprior = $num;
	}
	
	private static function mail_type($typ, $enc=''){
		if( $typ == 'html') {
			$random_hash = md5(@date('r', time()));
			self::$mtype = 'text/html';
			//self::$mtype .= 'multipart/alternative';
			self::$mtype .= '; boundary="PHP-alt-'.$random_hash.'"';
			self::$mencoding = empty($enc) ? '8bit' : strtolower($enc);
		} else {
			self::$mtype = 'text/plain';
			self::$mencoding = empty($enc) ? 'quoted-printable' : strtolower($enc);
		}
	}





	private static function mail_headers(){
		
		$mheader = array();
		$mheader['Mime-Version'] = '1.0';
		$mheader['Content-Type'] = self::$mtype.'; charset="'.self::$mcharset.'"';
		//$mheader['Content-Transfer-Encoding'] = '"'.self::$mencoding.'"';
		
		if( !empty( self::$mfrom ) ){
			$mheader['From'] = ( is_array(self::$mfrom) ) ? self::$mfrom[1].' <'.self::$mfrom[0].'>' : self::$mfrom;
		}
		if( !empty( self::$mcc ) )
			$mheader['Cc'] = implode( ',', self::$mcc );
		
		if( !empty( self::$mbc ) )
			$mheader['Bcc'] = implode( ',', self::$mbc );
		
		
		$mheader['X-Mailer'] = self::$mm;
		
		if( !empty( self::$mreply ) )
			$mheader['Reply-To'] = self::$mreply;
		
		
		if( !empty(self::$mprior) ){
			
			switch( self::$mprior ){
				case 1:
					$mheader["Priority"]="urgent";
					$mheader["X-Priority"]="1";
					$mheader["X-MSMail-Priority"]="Highest";
				break;
				case 2:
					$mheader["Priority"]="urgent";
					$mheader["X-Priority"]="2";
					$mheader["X-MSMail-Priority"]="High";
				break;
				case 3:
					$mheader["Priority"]="normal";
					$mheader["X-Priority"]="3";
					$mheader["X-MSMail-Priority"]="Normal";
				break;
				case 4:
					$mheader["Priority"]="non-urgent";
					$mheader["X-Priority"]="4";
					$mheader["X-MSMail-Priority"]="Low";
				break;
				case 5:
					$mheader["Priority"]="non-urgent";
					$mheader["X-Priority"]="5";
					$mheader["X-MSMail-Priority"]="Lowest";
				break;
			}
			
		}
		
//		while( list($ht, $v) = each($mheader) )
//			self::$m_header .= "$ht: $v\n";
		foreach( $mheader as $k=>$v )
			self::$m_header .= "$k: $v\n";
	
	}
/*---------------Mail Composer Crew---------------*/



/*---------------Error Reporter---------------*/
	private static function is_mail_error(){		
		return self::$is_m_err == '' ? false : true;
	}	

	private static function mail_error_save( $type, $trace = NULL ){
		//self::$is_m_err = $trace;
		$error = array();
		switch( $type ) {
			case 'mail-validate-error': 
				$error['error_type'] = 'mail-validation-error';
				$error['correction'] = 'Check Trace Mail';
				$error['trace'] = self::$is_m_err;
			break;
			case 'mail-send-error':
				$error['error_type'] = 'mail-send-error';
				$error['correction'] = 'Check Header TO emails';
				$error['trace'] = is_array($trace) ? implode(',', $trace) : $trace;				
			break;
		}
		self::$return_error = array('err_type'=>$error['error_type'], 'err_fix'=>$error['correction'], 'err_trace'=>$error['trace']);
	}
/*---------------Error Reporter---------------*/







/*---------------------------------PUBLIC FUNCTIONS-------------------------------*/
	public static function show_mail_error() {
		return empty( self::$return_error ) ? '' : self::$return_error;
	}


	public static function send_mail( $mail_properties=array(), $validate_server=false ) {
		
		
		self::resetall();
		if( is_array($mail_properties) ) {
			
			foreach($mail_properties as $type=>$value){
				
				if( self::is_mail_error() == false ){
					if( empty($value) )
						continue;
					
					switch( $type ){
						case 'm_to': self::mail_to($value); break;
						case 'm_from': self::mail_from($value); break;
						case 'm_bc': self::mail_bc($value); break;
						case 'm_cc': self::mail_cc($value); break;
						case 'm_reply': self::mail_reply($value); break;
					}
					
				} else {
					self::mail_error_save( "mail-validate-error", $value );
					break;
				}
			}
			
			if( self::is_mail_error() == false ) {
				
				self::mail_subject( $mail_properties['m_subject'] );
				self::mail_body( $mail_properties['m_body'], $mail_properties['mcharset'] );
				self::mail_type( $mail_properties['m_type'], $mail_properties['mencoding'] );
				self::mail_priority( $mail_properties['m_priority'] );
				self::mail_headers();
				
				$err_send_mail = array();
				$pass_send_mail = array();
				
				foreach( self::$mto as $to_mail ) 
                	@mail($to_mail, self::$msubj, self::$mbdy, self::$m_header) ? $pass_send_mail[] = $to_mail : $err_send_mail[] = $to_mail;
				
				if( !empty($err_send_mail) ) 
					self::mail_error_save('mail-send-error', $err_send_mail);
				
				
				//unset($pass_send_mail, $err_send_mail);
				return !self::is_mail_error() ? $pass_send_mail : self::show_mail_error();
				
            } else { return self::show_mail_error(); }
            return false;
		}
			return 'UNKNOWN FUNCTION FORMAT';
				
	}
	
}
	
/*---------------------------------PUBLIC FUNCTIONS-------------------------------*/
	
	
//}

/*
* usage just copy this array an fill in the values to the keys
mailer::send_mail(array(
		'm_to' => '',
		'm_from' => array('mail','name'),
		'm_bc' => '',
		'm_cc' => '',
		'm_reply' => '',
		'm_subject' => '',
		'm_body' => '',
		'm_type' => '',
		'm_priority' => '',
		'mcharset' => '',
		'mencoding' => ''
	) );
*/
?>
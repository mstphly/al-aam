<?php

	class mysqldb {
		
		private $db;
		private static $instance;
		private $result;
		private $last_result;
		private $last_error;
		private $row_count;
		
		
		private function __construct() { }
		private function __clone(){ }
	
		public static function instance() {
			if ( !self::$instance )
				self::$instance = new mysqldb();
			return self::$instance;
		}
	
	
		public function connecti($host, $user, $password, $db) {
			
			if( !is_resource($this->db) ) {
				
				$this->db = new mysqli($host, $user, $password, $db);
				if ( mysqli_connect_errno() ) {
					$this->last_error = mysqli_connect_error();
					return false;
				}
				
			}
			
			return $this->db;
			
		}
		
		
	   public function close() {
			if ( is_resource($this->db) )
					$this->db->close();
			return true;
	   }
	   
	   
	   public function is_error() {
			if ( isset($this->last_error) && !empty($this->last_error) )
				return $this->last_error;
			return false;
	   }
	  
	  
	   public function get_results($sql) {
			if ( !$this->query($sql) )
				return false;
			
			$num_rows = 0;
			while ( $row = $this->result->fetch_object() ) {
				$this->last_result[$num_rows] = $row;
				$num_rows++;
			}
			
			$this->result->close();
			
			return $this->last_result;
	   }
		
		
	   public function num_rows() {
			return (int)$this->row_count;
	   }
	   
	   
	   public function __destruct() { return $this->close(); }
	   
	
	}
	
	
	function dbi(){
		$db = mysqldb::instance();
		return $db->connecti( $GLOBALS['dbHost'], $GLOBALS['dbUser'], $GLOBALS['dbPass'], $GLOBALS['dbName'] );
	}
	
	$dbConn = dbi();
	
		
	function &dbQuery($sql)
	{
		$query = mysqli_query( $GLOBALS['dbConn'], $sql );
		return $query;
	}
	
	
	function dbReconnect()
	{
		mysqli_close($GLOBALS['dbConn']);
		$GLOBALS['dbConn'] = dbi();
	}
		
	
	function &dbMultiQuery($sql)
	{
		$query = mysqli_multi_query( $GLOBALS['dbConn'], $sql );
		return $query;
	}
	
	function dbAffectedRows()
	{
		return mysqli_affected_rows( $GLOBALS['dbConn'] );
	}
	
	function &dbFetchArray($result, $resultType = MYSQLI_BOTH) {
		$rows = mysqli_fetch_array($result, $resultType);
		return $rows;
	}
	
	function &dbFetchAssoc($result)
	{
		$rows = mysqli_fetch_assoc($result);
		return $rows;
	}
	
	function &dbFetchObject($result)
	{
		$rows = mysqli_fetch_object($result);
		return $rows;
	}
	
	function &dbFetchRow($result) 
	{
		$fetch = mysqli_fetch_row($result);
		return $fetch;
	}
	
	function dbFreeResult($result)
	{
		return mysqli_free_result($result);
	}
	
	function dbNumRows($result)
	{
		return mysqli_num_rows($result);
	}
	
	function dbSelect($dbName)
	{
		return mysqli_select_db( $GLOBALS['dbConn'], $dbName );
	}
	
	function dbInsertId()
	{
		return mysqli_insert_id( $GLOBALS['dbConn'] );
	}

	function dbEscape($post)
	{
		return mysqli_real_escape_string($GLOBALS['dbConn'], $post);
	}	


?>
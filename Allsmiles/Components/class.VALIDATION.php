<?php



class VALIDATION
{
    private function __construct()
    {
    }
    private function __clone()
    {
    }
    
    protected static $key = '';
    
    /**
     * 
     * 
     * 
     */
    private static function vemail($input)
    {
        return preg_match("/^[[:alnum:]._-]+@[[:alnum:]._-]+.[[:alpha:]]{2,6}$/i", $input) ? true : false;
    }
    
    
    
    
    /**
     * 
     * 
     * 
     */
    private static function vnum($input)
    {
        return is_numeric($input) ? true : false;
    }
    
    
    
    
    /**
     * 
     * 
     * 
     */
    private static function vstr($input)
    {
        return ctype_alpha($input) ? true : false;
    }
    
    
    
    /**
     * 
     * 
     * 
     */
    private static function vstrnum($input)
    {
        return ctype_alnum($input) ? true : false;
    }
	
	
	
	private static function vlen($input, $expectlen)
	{
		return strlen($input) < $expectlen ? false : true;
	}
	
	
	
	private static function vmust( $input, $must = array() )
	{
        if (!is_array($must))
            $must = explode(',', $must);
			
		$arrayInput = str_split($input, 1); // converts input from string to array
		foreach( $must as $v )
		{
			if ( !in_array($v,$arrayInput) )
			{
				return false;
			}
		}
		return true;
		
	}
    
    
    
    /**
     * 
     * 
     * 
     */
    private static function vsp_strnum($input, $allowed = array())
    {
        if (!is_array($allowed))
            $allowed = explode(',', $allowed);
        
        
        $a_list = array_merge(array(
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9
        ), $allowed);
        $a_list = array_unique($a_list);
		var_dump($a_list);
        
        $input = strtolower($input);
        for ($i = 0, $k = strlen($input); $i < $k; $i++) {
            if (!in_array($input[$i], $a_list)) {
                return false;
            }
        }
        return true;
        
    }
    
    
    
    
    /**
     * 
     * 
     * 
     */
    private static function verrname($errt)
    {
        $return = '';
        switch ($errt) {
            case 'em':
                $return = 'Invalid Email';
                break;
            case 'num':
                $return = 'Numbers Only';
                break;
            case 'alpnum':
                $return = 'Numbers and Alphabets Only';
                break;
            case 'alp':
                $return = 'Alphabets Only';
                break;
            case 'sp_alpnum':
                $return = 'Unknown Character Found';
                break;
            case 'emp':
                $return = 'Empty';
                break;
        }
        return $return;
    }
    
    
    
    public static function errkey()
    {
        return self::$key;
    }
    
    
    
    /**
     * 
     * 
     * 
     */
    public static function s_valid($input, $ty, $sp = false)
    {
        $return = '';
        switch ($ty) {
            case 'em':
                $return = self::vemail($input) ? 0 : 1;
                break;
            case 'num':
                $return = self::vnum($input) ? 0 : 1;
                break;
            case 'alpnum':
                $return = self::vstrnum($input) ? 0 : 1;
                break;
            case 'alp':
                $return = self::vstr($input) ? 0 : 1;
                break;
            case 'sp_alpnum':
                $return = self::vsp_strnum($input, $sp) ? 0 : 1;
                break;
            case 'emp':
                $return = empty($input) ? 1 : 0;
                break;
			case 'len':
				$return = self::vlen($input, $sp) ? 1 : 0;
				break;
			case 'must':
				return self::vmust($input, $sp);
				break;
        }
        self::$key = 0; // since just one element
        return empty($return) ? true : false;
    }
    
    
    
    
    /**
     * 
     * 
     * 
     */
    public static function m_valid($valid = array())
    {
        $return = '';
        $key    = 0;
        foreach ($valid as $v) {
            if ($return == 1) {
                break;
            }
            
            switch ($v['typ']) {
                case 'em':
                    $return = self::vemail($v['v']) ? 0 : 1;
                    break;
                case 'num':
                    $return = self::vnum($v['v']) ? 0 : 1;
                    break;
                case 'alpnum':
                    $return = self::vstrnum($v['v']) ? 0 : 1;
                    break;
                case 'alp':
                    $return = self::vstr($v['v']) ? 0 : 1;
                    break;
                case 'sp_alpnum':
                    $return = self::vsp_strnum($v['v'], $v['more']) ? 0 : 1;
                    break;
                case 'emp':
                    $return = !empty($v['v']) ? 0 : 1;
                    break;
				case 'len':
					$return = self::vlen($v['v'], $v['sp']) ? 0 : 1;
					break;
				case 'must':
					$return = self::vmust($v['v'], $v['sp'] ) ? 0 : 1;
					break;
            }
            $key++;
            
        }
        self::$key = $key - 1; // to follow array standards
        return empty($return) ? true : false;
        
    }
    
    
    
    
    /**
     * 
     * 
     * 
     */
    public static function xm_valid($valid = array())
    {
        $returns = array();
        $return  = '';
        $keys    = array();
        $key     = 0;
        foreach ($valid as $v) {
            switch ($v['typ']) {
                case 'em':
                    $return = self::vemail($v['v']) ? 0 : 1;
                    break;
                case 'num':
                    $return = self::vnum($v['v']) ? 0 : 1;
                    break;
                case 'alpnum':
                    $return = self::vstrnum($v['v']) ? 0 : 1;
                    break;
                case 'alp':
                    $return = self::vstr($v['v']) ? 0 : 1;
                    break;
                case 'sp_alpnum':
                    $return = self::vsp_strnum($v['v'], $v['sp']) ? 0 : 1;
                    break;
                case 'emp':
                    $return = !empty($v['v']) ? 0 : 1;
                    break;
				case 'len':
					$return = self::vlen($v['v'], $v['sp']) ? 0 : 1;
					break;
				case 'must':
					$return = self::vmust($v['v'], $v['sp']) ? 0 : 1;
					break;
            }
            $key++;
            if ($return == 1) {
                $returns[] = array(
                    'name' => $v['name'],
                    'errtype' => self::verrname($v['typ'])
                );
                $return    = '';
                $keys[]    = $key - 1;
            }
            
        }
        self::$key = array(
            'errorkeys' => $keys,
            'errordetail' => $returns
        );
        return empty($returns) ? true : false;
        
    }
    
    
}

/* Sample Usage */
 /*
	Single Validation
	$is_valid = validator::s_valid(array('typ'=>'em', 'v'=>$_POST['email']));
	
	Multiple Validation
	$is_valid = validator::m_valid(
	array(
		array('typ'=>'emp', 'v'=>$_POST['firstName']),
		array('typ'=>'emp', 'v'=>$_POST['lastName']),
		array('typ'=>'num', 'v'=>$_POST['phone']),
		array('typ'=>'em', 'v'=>$_POST['email']),
		array('typ'=>'len', 'v'=>$_POST['password'], 'sp'=>6),
		array('typ'=>'emp', 'v'=>$_POST['dobday']),
		array('typ'=>'emp', 'v'=>$_POST['dobmonth']),
		array('typ'=>'emp', 'v'=>$_POST['dobyear']),
		array('typ'=>'emp', 'v'=>$_POST['gender']),
		array('typ'=>'emp', 'v'=>$_POST['address']),
		array('typ'=>'emp', 'v'=>$_POST['city']),
		array('typ'=>'emp', 'v'=>$_POST['country']),
		array('typ'=>'emp', 'v'=>$_POST['state']),
		array('typ'=>'same', 'v'=>$_POST['password'], 'sp'=>$_POST['password2']),
	)
);
	
 */


?>

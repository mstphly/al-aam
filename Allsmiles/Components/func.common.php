<?php

	function queryString()
	{
		$qString = array();
		
		foreach($_GET as $key => $value) {
			if (trim($value) != '') {
				$qString[] = $key. '=' . trim($value);
			} else {
				$qString[] = $key;
			}
		}
		
		$qString = implode('&', $qString);
		
		return $qString;
	}



	
	function convert_datetime($str)
	{ 

		list($date, $time) = explode(' ', $str); 
		list($year, $month, $day) = explode('-', $date); 
		list($hour, $minute, $second) = explode(':', $time); 
		 
		$timestamp = mktime($hour, $minute, $second, $month, $day, $year); 
		 
		return $timestamp; 
		
	} 
	
	
	
	
	function ago($currentTime, $timestamp)
	{
		$timestamp = convert_datetime($timestamp);
		$currentTime = convert_datetime($currentTime);
        $difference = $currentTime - $timestamp;
		
        $periods = array("second", "minute", "hour", "day", "week", "month", "years", "decade");
        $lengths = array("60","60","24","7","4.35","12","10");
        for($j = 0; $difference >= $lengths[$j]; $j++)
        {
                $difference /= $lengths[$j];
        }
       
        $difference = round($difference);
        if($difference != 1) $periods[$j].= "s";
       
        $text = "$difference $periods[$j] ago";
        return $text;
	}
	

?>
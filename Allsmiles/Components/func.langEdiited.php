<?php

	// ENGLISH
	$print['en'] = array(
		'loginHeader' => 'Login',
		'loginUsername' => 'Username',
		'loginPassword' => 'Password',
		'loginRememberme' => 'Remember Me',
		'loginForgotPass' => 'Forgot Your Password?',
		'loginNoWorries' => 'No worries, click <a href="javascript:;" class="" id="forget-password">here</a> to reset your password.',
		'loginForgotPass2' => 'Forgot Password?',
		'loginResetPass' => 'Enter your e-mail address below to reset your password.',
		'loginPhrase' => 'Already a Registered User?',
		'loginError' => '<span>Error!</span> Incorrect Password or Username.',
		'loginPhraseInfo' => 'Cras consequat porttitor tincidunt. Praesent eu tincidunt lacus. Donec ut neque vitae erat congue venenatis.',
		'loginNewButton' => 'Click here to Login',
		'loginButton' => 'Login',
		'signupPhrase' => 'Not a Registered User?',
		'signupPhraseInfo' => 'Cras consequat porttitor tincidunt. Praesent eu tincidunt lacus. Donec ut neque vitae erat congue venenatis.',
		'signupHeader' => 'Sign Up',
		'signupNewButton' => 'Register with us',
		'signupButton' => 'Register',
		'signupUsername' => 'Username',
		'signupPassword' => 'Password',
		'signupInitials' => 'Initials',
		'signupTitle' => 'Title',
		'signupFirstname' => 'Firstname',
		'signupLastname' => 'Lastname',
		'signupTelephoneno' => 'Telephone No',
		'signupBirthday' => 'Birthday',
		'signupConfirmPassword' => 'Confirm Password',
		'signupAddress1' => 'Address 1',		
		'signupAddress2' => 'Address 2',
		'signupCity' => 'City',
		'signupCountry' => 'Country',
		'signupProfileImage' => 'Profile Image',		
		'pageStatus' => 'SORRY PAGE NOT FOUND'
	);
	
	
	
	// FRENCH
	$print['fr'] = array(
		'loginHeader' => 'Login',
		'loginUsername' => 'nom d'utilisateur',
		'loginPassword' => 'mot de passe',
		'loginRememberme' => 'se souvenir de moi',
		'loginForgotPass' => 'Mot de passe oubli� ?',
		'loginNoWorries' => 'Aucune inqui�tudes, clic <a href="javascript:;" class="" id="forget-password">here</a> to reset your password.',
		'loginForgotPass2' => 'Mot de passe oubli�?',
		'loginResetPass' => '�crivez votre adresse �lectronique ci-dessous pour remettre � z�ro votre mot de passe.',
		'loginPhrase' => 'D�j� un utilisateur enregistr�?',
		'loginError' => '<span>Error!</span> un mot de passe incorrect ou un username.',
		'loginPhraseInfo' => 'Cras consequat porttitor tincidunt. Praesent eu tincidunt lacus. Donec ut neque vitae erat congue venenatis.',
		'loginNewButton' => 'Le cliquez ici � ouvrir une session',
		'loginButton' => 'ouvrir une session',
		'signupPhrase' => 'pas un utilisateur enregistr�?',
		'signupPhraseInfo' => 'Cras consequat porttitor tincidunt. Praesent eu tincidunt lacus. Donec ut neque vitae erat congue venenatis.',
		'signupHeader' => 's'enregistrent',
		'signupNewButton' => 'Inscrivez-vous � nous',
		'signupButton' => 'le s'inscrire',
		'signupUsername' => 'Nom d'utilisateur',
		'signupPassword' => 'mot de passe',
		'signupInitials' => 'Initiales',
		'signupTitle' => 'Titre',
		'signupFirstname' => 'Pr�nom',
		'signupLastname' => 'Lastname',
		'signupTelephoneno' => 't�l�phone aucun',
		'signupBirthday' => 'date de naissance ',
		'signupConfirmPassword' => 'Confirmez le mot de passe',
		'signupAddress1' => 'Adresse  1',		
		'signupAddress2' => 'Adresse  2',
		'signupCity' => 'Ville',
		'signupCountry' => 'pays',
		'signupProfileImage' => 'Image de profil',		
		'pageStatus' => 'PAGE D�SOL�E NON TROUV�E'
	);
	
?>
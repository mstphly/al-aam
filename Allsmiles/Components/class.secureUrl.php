<?php

	class secureUrl
	{
		
		public static $var_name = 'url';		
			
		public static function encode( $buffer )
		{ // Please Not That This Function is Not Fully Stable

			$newBuffer = '';
			$oldLinks = array();
			$newLinks = array();
			$var = self::$var_name;
			$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>"; 
			if( preg_match_all("/$regexp/siU", $buffer, $matches, PREG_SET_ORDER) ) 
			{ 
			
				foreach($matches as $match)
				{ // $match[2] = link address // $match[3] = link text
					
					$pos_debut = '';
					
					if( ($pos_debut = strpos($match[2],'?')) === false )
					{
						continue;
					}
					
					//$url = $match[2];
					$sep = '';
					$fin = '';
					//$pos_debut=strpos($url,'?');
					if(!$pos_debut)
						$sep='&';					
					
					$pos_fin=strpos($match[2],' ');
					
					if($pos_fin)
					{
						$pos_long=$pos_fin-$pos_debut-1; 
						$fin=substr($match[2],$pos_fin); 
					}else
						$pos_long=strlen($match[2])-$pos_debut-1;
					
					$debut=substr($match[2],0,$pos_debut+1);
					$param=substr($match[2],$pos_debut+1,$pos_long);
					$code = base64_encode($param);
					$newUrl = $debut.$sep.$var.'='.$code.$fin;
					
					$oldLinks[] = '<a href="'.$match[2].'">'.$match[3].'</a>';
					$newLinks[] = '<a href="'.$newUrl.'">'.$match[3].'</a>';
					
				}

			}
			
			
			return $buffer = str_replace($oldLinks, $newLinks, $buffer );
				
		}
		
		
		public static function decode()
		{
			if( $_GET[self::$var_name] )
			{
				parse_str(base64_decode($_GET[self::$var_name]), $tbl);
				foreach($tbl as $k=>$v)
				{
					$_GET[$k]=$v;
				}
			}
			return;
		}
	
	
	}

?>
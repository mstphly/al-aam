<?php

	function randomString($randStringLength)
	{
		$timestring = microtime();
		$secondsSinceEpoch=(integer) substr($timestring, strrpos($timestring, " "), 100);
		$microseconds=(double) $timestring;
		$seed = mt_rand(0,1000000000) + 10000000 * $microseconds + $secondsSinceEpoch;
		mt_srand($seed);
		$randstring = "";
		for($i=0; $i < $randStringLength; $i++)
		{
			$randstring .= chr(ord('a') + mt_rand(0, 25));
		}
		return($randstring);
	}
	

	function randomNumber($randNumLength)
	{
		$timestring = microtime();
		$secondsSinceEpoch=(integer) substr($timestring, strrpos($timestring, " "), 100);
		$microseconds=(double) $timestring;
		$seed = mt_rand(0,1000000000) + 10000000 * $microseconds + $secondsSinceEpoch;
		mt_srand($seed);
		$randstring = "";
		for( $i=0; $i < $randNumLength; $i++ )
		{
			$randstring .= mt_rand(0, 9);
		}
		return($randstring);
	}	

?>
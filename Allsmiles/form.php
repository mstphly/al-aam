<?php


	/* Components Setup */
		 require_once 'Components/func.loader.php';
		 require_once 'Components/func.timezone.php';
		 require_once 'Components/config.php'; 
	/* Components Setup */
	
	if( isset($_GET['url']) && is_file("Formprocessing/form_".$_GET['url'].".php") )
	{
		include("Formprocessing/form_".$_GET['url'].".php");
	} else
	{
		// Goto Not Found Page
		header("Location: index.php?seek=notfound");
		exit;
	}

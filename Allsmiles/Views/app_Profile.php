<?php
	
	checkSessionStatus();
	require_once dirname(__FILE__).'/__menuTop.php';
	
	
	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
FOOTJS;

	$detailTemplate['Fullname'] = '';
	$detailTemplate['Address'] = '';
	$detailTemplate['Address2'] = '';
	$detailTemplate['Telephone'] = '';
	$detailTemplate['Email'] = '';
	$detailTemplate['Fax'] = '';
	$detailTemplate['Birthday'] = '';
	$detailTemplate['profImg'] = '';	
	
	$detailTemplate['nokAddress'] = '';
	$detailTemplate['nokAddress2'] = '';
	$detailTemplate['nokTelephone'] = '';		
	$detailTemplate['nokFullname'] = '';

	$detailTemplate['taxid'] = '';
	$detailTemplate['occupation'] = '';
	$detailTemplate['organisation'] = '';


	$view = isset($_GET['pid'][0]) ? $_GET['pid'] : $_SESSION['activated'];
	$result =& dbQuery('SELECT * FROM `record` WHERE `record_id` = '.$view.' LIMIT 1');
	
	if( dbNumRows($result)>0 )
	{

		$DataDetail =& dbFetchAssoc($result);
		dbFreeResult($result);

		$detailTemplate['Fullname'] = $DataDetail['title'].'. '.ucwords($DataDetail['lname']).' <span>'.ucwords($DataDetail['mname']).' '.ucwords($DataDetail['fname']).'</span>';
		$detailTemplate['Address'] = $DataDetail['street'];
		$detailTemplate['Address2'] = $DataDetail['streetb'];
		$detailTemplate['Telephone'] = $DataDetail['phone'];
		$detailTemplate['Email'] = $DataDetail['email'];
		$detailTemplate['Fax'] = $DataDetail['fax'];
		$detailTemplate['Birthday'] = $DataDetail['dob'];
		$detailTemplate['Sex'] = $DataDetail['sex'];
		$detailTemplate['profImg'] = empty($DataDetail['profileimage']) ? 'Profile/Default.jpg' : "SLIR/w197-h197/allsmiles/Profile/$DataDetail[profileimage]";
		
		$detailTemplate['nokFullname'] = $DataDetail['nextofkinfullname'];
		$detailTemplate['nokAddress'] = $DataDetail['nextofkinstreet2'];
		$detailTemplate['nokAddress2'] = $DataDetail['nextofkinstreet2b'];
		$detailTemplate['nokTelephone'] = $DataDetail['nextofkinphone'];		

		$detailTemplate['taxid'] = $DataDetail['federaltaxid'];
		$detailTemplate['occupation'] = $DataDetail['occupation'];
		$detailTemplate['organisation'] = $DataDetail['organisation'];	
		
	}
	

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$detailTemplate['Fullname']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">

  
    <!-- 12 Columns -->
    <div class="twelve columns">
      <div class="headline no-margin">
        <h3>{$print[ $appSettings['appLanguage'] ]['appProfileHeader']}</h3>
      </div>
      
		   <!-- Accordion #1 -->
			<span class="acc-trigger"><a href="#">{$print[ $appSettings['appLanguage'] ]['module_profiletype1']}</a></span>
			<div class="acc-container">
			  <div class="content">

				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailAddress']}</span>
					<span class="dB">{$detailTemplate['Address']}</span>
				</p>

				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailAddress2']}</span>
					<span class="dB">{$detailTemplate['Address2']}</span>
				</p>				

				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailTelephone']}</span>
					<span class="dB">{$detailTemplate['Telephone']}</span>
				</p>

				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailEmail']}</span>
					<span class="dB">{$detailTemplate['Email']}</span>
				</p>
				
				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailFax']}</span>
					<span class="dB">{$detailTemplate['Fax']}</span>
				</p>				

				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailBirthday']}</span>
					<span class="dB">{$detailTemplate['Birthday']}</span>
				</p>
				
				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailSex']}</span>
					<span class="dB">{$detailTemplate['Sex']}</span>
				</p>				
				
			  </div>
			</div>
			<!-- Accordion #2 -->
			<span class="acc-trigger"><a href="#">{$print[ $appSettings['appLanguage'] ]['module_profiletype2']}</a></span>
			<div class="acc-container">
			  <div class="content">

			  	<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailnokFullname']}</span>
					<span class="dB">{$detailTemplate['nokFullname']}</span>
				</p>

				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailnokAddress']}</span>
					<span class="dB">{$detailTemplate['nokAddress']}</span>
				</p>
				
				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailnokAddress2']}</span>
					<span class="dB">{$detailTemplate['nokAddress2']}</span>
				</p>				

				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailnokPhone']}</span>
					<span class="dB">{$detailTemplate['nokTelephone']}</span>
				</p>

			  </div>
			</div>
			
			<!-- Accordion #3 -->
			<span class="acc-trigger"><a href="#">{$print[ $appSettings['appLanguage'] ]['module_profiletype3']}</a></span>
			<div class="acc-container">
			  <div class="content">

				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailTax']}</span>
					<span class="dB">{$detailTemplate['taxid']}</span>
				</p>

				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailOccupation']}</span>
					<span class="dB">{$detailTemplate['occupation']}</span>
				</p>
				
				<p>
					<span class="dA">{$print[ $appSettings['appLanguage'] ]['module_profiledetailOrganisation']}</span>
					<span class="dB">{$detailTemplate['organisation']}</span>
				</p>				

			  </div>
			</div>				
			  
	  
    </div>
    <div class="four columns">
      <div class="headline no-margin">
        <h3>{$print[ $appSettings['appLanguage'] ]['module_profilephotoHeader']}</h3>
		<div>
			<p>
				<img src="{$detailTemplate['profImg']}" />
			</p>
		</div>
      </div>

	  
	  
    </div>  
  
  
  </div>
  <!-- 960 Container End -->
  <!-- 960 Container -->
  <!-- 960 Container End -->

BODYCONTENT;
	

?>
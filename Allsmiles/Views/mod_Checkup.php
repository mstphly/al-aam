<?php
	
	checkSessionStatus();
	require_once dirname(__FILE__).'/__menuTop.php';

	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS<script src="assets/build/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
FOOTJS;

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['module_checkup']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">

  
    <!-- 12 Columns -->
    <div class="twelve columns">
      <div class="headline no-margin">
        <h3>{$print[ $appSettings['appLanguage'] ]['module_checkupHead']}</h3>
      </div>
      
	  
	  
	  
    </div>
    <div class="four columns">
      <div class="headline no-margin">
        <h3>{$print[ $appSettings['appLanguage'] ]['module_checkupTip']}</h3>
      </div>	<!-- Testimonial's Carousel -->	<div class="testimonials-carousel" data-autorotate="3000">		<ul class="carousel">					<li class="testimonial">				<div class="testimonials">Posuere erat a ante venenatis dapibus posuere velit aliquet. Duis llis, est non coeammodo luctus, nisi erat porttitor ligula, egeteas laciniato odiomo sem.</div>				<div class="testimonials-bg"></div>				<div class="testimonials-author">Michael, <span>Flash Developer</span></div>			</li>						<li class="testimonial">				<div class="testimonials">Integer eu libero sit amet nisl vestibulum semper. Fusce nec porttitor massa. In nec neque elit. Curabitur malesuada ligula vitae purus ornare onec etea magna diam varius.</div>				<div class="testimonials-bg"></div>				<div class="testimonials-author">John, <span>Web Developer</span></div>			</li>						<li class="testimonial">				<div class="testimonials">Cras sed odio est, sit amet porttitor elit. Vestibulum elementum erat vitae ante venenatis cursus scelerisque quam vel nibh imperdiet vitae porta lorem posuere.</div>				<div class="testimonials-bg"></div>				<div class="testimonials-author">Peter, <span>Project Manager</span></div>			</li>						<li class="testimonial">				<div class="testimonials">Elementum erat vitae ante venenatis dapibus. Maecenas cursus scelerisque quam vel nibh imperdiet vitae porta lorem posuere.</div>				<div class="testimonials-bg"></div>				<div class="testimonials-author">Kathy, <span>Art Director</span></div>			</li>					</ul>	</div>
    </div>  
  
  
  </div>
  <!-- 960 Container End -->
  <!-- 960 Container -->
  <!-- 960 Container End -->

BODYCONTENT;
	

?>
<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
 * @version    1.7.8, 2012-10-12
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** Include PHPExcel */



$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("AllSmiles Dental Clinic")->setLastModifiedBy("AllSmiles Dental Clinic")->setDescription("All Smiles Reporting");

$objPHPExcel->getActiveSheet()->getStyle('A1:W1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:W1')->getFont()->setSize(14);
$reportType = isset($_GET['rt']) ? $_GET['rt'] : '';
switch ($reportType) {
    case 1:
        $i = 1;
        $objPHPExcel->getProperties()->setTitle("Dental Clinic List for all Male")->setSubject("Dental Clinic List for all Male");
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Title')->setCellValue('B1', 'Lastname')->setCellValue('C1', 'Middlename')->setCellValue('D1', 'Firstname')->setCellValue('E1', 'Date of Birth')->setCellValue('F1', 'Sex')->setCellValue('G1', 'Email')->setCellValue('H1', 'Fax')->setCellValue('I1', 'Personal Website')->setCellValue('J1', 'Telephone No')->setCellValue('K1', 'Address 1')->setCellValue('L1', 'Address 2')->setCellValue('M1', 'City')->setCellValue('N1', 'Country')->setCellValue('O1', 'Zip Code')->setCellValue('P1', 'Occupation')->setCellValue('Q1', 'Organisation')->setCellValue('R1', 'Federal Tax ID')->setCellValue('S1', 'Next of Kin Fullname')->setCellValue('T1', 'Next of Kin Telephone no')->setCellValue('U1', 'Next of Kin Address 1')->setCellValue('V1', 'Next of Kin Address 2')->setCellValue('W1', 'Next of Kin City');
        $result =& dbQuery('SELECT * FROM `record` WHERE `sex` = "M" && `role_type` = 3');
        if (dbNumRows($result) > 0) {
            while (($row =& dbFetchAssoc($result))) {
                $i++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$i", $row['title'])->setCellValue("B$i", $row['lname'])->setCellValue("C$i", $row['mname'])->setCellValue("D$i", $row['fname'])->setCellValue("E$i", $row['dob'])->setCellValue("F$i", $row['sex'])->setCellValue("G$i", $row['email'])->setCellValue("H$i", $row['fax'])->setCellValue("I$i", $row['ulr'])->setCellValue("J$i", $row['phone'])->setCellValue("K$i", $row['street'])->setCellValue("L$i", $row['streeb'])->setCellValue("M$i", $row['city'])->setCellValue("N$i", $row['country'])->setCellValue("O$i", $row['zip'])->setCellValue("P$i", $row['occupation'])->setCellValue("Q$i", $row['organisation'])->setCellValue("R$i", $row['federaltaxid'])->setCellValue("S$i", $row['nextofkinfullname'])->setCellValue("T$i", $row['nextofkinphone'])->setCellValue('U$i', $row['nextofkinstreet2'])->setCellValue('V$i', $row['nextofkinstreet2b'])->setCellValue('W$i', $row['nextofkincity2']);
            }
            dbFreeResult($result);
        }
        break;
    case 2:
        $i = 1;
        $objPHPExcel->getProperties()->setTitle("Dental Clinic List for all Male")->setSubject("Dental Clinic List for all Male");
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Title')->setCellValue('B1', 'Lastname')->setCellValue('C1', 'Middlename')->setCellValue('D1', 'Firstname')->setCellValue('E1', 'Date of Birth')->setCellValue('F1', 'Sex')->setCellValue('G1', 'Email')->setCellValue('H1', 'Fax')->setCellValue('I1', 'Personal Website')->setCellValue('J1', 'Telephone No')->setCellValue('K1', 'Address 1')->setCellValue('L1', 'Address 2')->setCellValue('M1', 'City')->setCellValue('N1', 'Country')->setCellValue('O1', 'Zip Code')->setCellValue('P1', 'Occupation')->setCellValue('Q1', 'Organisation')->setCellValue('R1', 'Federal Tax ID')->setCellValue('S1', 'Next of Kin Fullname')->setCellValue('T1', 'Next of Kin Telephone no')->setCellValue('U1', 'Next of Kin Address 1')->setCellValue('V1', 'Next of Kin Address 2')->setCellValue('W1', 'Next of Kin City');
        $result =& dbQuery('SELECT * FROM `record` WHERE `sex` = "F" && `role_type` = 3');
        if (dbNumRows($result) > 0) {
            while (($row =& dbFetchAssoc($result))) {
                $i++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$i", $row['title'])->setCellValue("B$i", $row['lname'])->setCellValue("C$i", $row['mname'])->setCellValue("D$i", $row['fname'])->setCellValue("E$i", $row['dob'])->setCellValue("F$i", $row['sex'])->setCellValue("G$i", $row['email'])->setCellValue("H$i", $row['fax'])->setCellValue("I$i", $row['ulr'])->setCellValue("J$i", $row['phone'])->setCellValue("K$i", $row['street'])->setCellValue("L$i", $row['streeb'])->setCellValue("M$i", $row['city'])->setCellValue("N$i", $row['country'])->setCellValue("O$i", $row['zip'])->setCellValue("P$i", $row['occupation'])->setCellValue("Q$i", $row['organisation'])->setCellValue("R$i", $row['federaltaxid'])->setCellValue("S$i", $row['nextofkinfullname'])->setCellValue("T$i", $row['nextofkinphone'])->setCellValue('U$i', $row['nextofkinstreet2'])->setCellValue('V$i', $row['nextofkinstreet2b'])->setCellValue('W$i', $row['nextofkincity2']);
            }
            dbFreeResult($result);
        }
        break;
}
$objPHPExcel->getSecurity()->setLockWindows(true);
$objPHPExcel->getSecurity()->setLockStructure(true);
$objPHPExcel->getSecurity()->setWorkbookPassword("allsmiles");
$objPHPExcel->getActiveSheet()->getProtection()->setPassword('allsmiles');
$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save("AllSMILESREPORT$_SERVER[REQUEST_TIME].xlsx"); // place this code inside a php file and call it f.e. "download.php"$fullPath = $appSettings['webRoot']."AllSMILESREPORT$_SERVER[REQUEST_TIME].xlsx";if ($fd = fopen ($fullPath, "r")) {    $fsize = filesize($fullPath);    $path_parts = pathinfo($fullPath);    $ext = strtolower($path_parts["extension"]);
header("Content-type: application/octet-stream");
header("Content-Disposition: filename=\"" . $path_parts["basename"] . "\"");
header("Content-length: $fsize");
header("Cache-control: private"); //use this to open files directly
while (!feof($fd)) {
    $buffer = fread($fd, 2048);
    echo $buffer;
}
fclose($fd);
exit;

?>
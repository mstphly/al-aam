<?php
	
	checkSessionStatus();
	require_once dirname(__FILE__).'/__menuTop.php';
	
	
	
	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/build/css/boxed.css" id="layout">
<link rel="stylesheet" type="text/css" href="assets/build/css/colors/green.css" id="colors">
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
FOOTJS;
		
	$listInTable = '
		<table class="standard-table">
			<tr>
				<th>No</th>
				<th>Fullname</th>
				<th>Role Type</th>
				<th>Options</th>
			</tr>
	';
	
	
	
	$result =& dbQuery('SELECT `role_type`, `title`, `fname`, `lname` FROM `record` WHERE role_type = 3');
	if( dbNumRows($result)>0 )
	{
	
		$no = 0;
		while( ($x =& dbFetchAssoc($result)) )
		{
			$no++;
			$listInTable .= 
			'
				<tr>
					<td>'.$no.'</td>
					<td>'.$x['title'].'. '.$x['lname'].' '.$x['lname'].'</td>
					<td>'.roleType($x['role_type']).'</td>
					<td>
						<a href="index.php?seek=profile&pid='.$x['record_id'].'"><i class="mini-ico-user"></i>View User</a> <span class="i">|</span> 
						<a href="index.php?seek=editprofile&pid='.$x['record_id'].'"><i class="mini-ico-pencil"></i>Edit User</a> <span class="i">|</span>
						<a href="#"><i class="mini-ico-comment"></i>Send Message</a>
					</td>
				</tr>
			';
		}
		dbFreeResult($result);	
		$listInTable .= '</table>';
		
	} else
	{
		$listInTable = '<div class="notification notice closeable" style="display: block;">
							<p><span>Notice!</span> Patient List is Empty</p>
							<a href="#" class="close"></a>
						</div>';
	}
	
	

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appPatientsHeader']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">

  
    <!-- 12 Columns -->
    <div class="twelve columns">

      	
			{$listInTable}
	  
    </div>
    <div class="four columns">
     

	  
	  
    </div>  
  
  
  </div>
  <!-- 960 Container End -->
  <!-- 960 Container -->
  <!-- 960 Container End -->

BODYCONTENT;
	

?>
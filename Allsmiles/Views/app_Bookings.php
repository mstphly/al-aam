<?php
	
	checkSessionStatus();
	require_once dirname(__FILE__).'/__menuTop.php';
	
	
	
	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<style type="text/css">
			.calendarControl{ background:#333; padding:5px 0; display:block; overflow:hidden }
			.calendarControl h2{ text-align:center; color:#FFFFFF; padding-top:5px;}
			table.calendar		{ border-left:1px solid #999; background:#FFFFFF; }
			.green{ background:#E9FEDE; }
			.green a {color:#FFFFFF}
			.red{ background:#FF0000}
			tr.calendar-row	{  }
			td.calendar-day	{ min-height:150px; font-size:11px; position:relative; } * html div.calendar-day { height:150px; }
			/*td.calendar-day:hover	{ background:#eceff5; }*/
			td.calendar-day-np	{ /*background:#EEE;*/ min-height:150px; } * html div.calendar-day-np { height:150px; }
			td.calendar-day-head { background:#EEEEEE; font-weight:bold; text-align:center; width:120px; padding:5px; border-bottom:1px solid #999; border-top:1px solid #999; border-right:1px solid #999; font-size:11px; }
			div.day-number{border-bottom:1px solid #CCCCCC; background:#FAFAFA; padding:5px 5px 4px; text-align:right; display:block; overflow:hidden; }
			div.day-number	a{ /*background:#999;*/ /*padding:20px;*/ padding:0 5px; color:#222222; font-weight:bold; float:right; margin:-5px -5px 0 0; /*width:20px;*/  }
			/* shared */
			td.calendar-day, td.calendar-day-np { width:120px; /*padding:5px;*/ border-bottom:1px solid #999; border-right:1px solid #999; }	
			.available, .notavailable, .aaavailable,.pastDate{height:60px; display:block; overflow:hidden; text-align:center; vertical-align:middle; background:#E9FEDE}
			.available a{ color:#002200;}
			.available a span, .notavailable a span, .aaavailable a span, .pastDate a span{padding-top:22px; font-size:11px;display:block; overflow:hidden;}
			.pastDate{ background:#EEEEEE;}
			.pastDate a{ color:#333333;}
			.notavailable{background:#FFEEDD;}
			.aaavailable{background:#FCFCFC}
			.notavailable a, .aaavailable a{ color:#400000}
		</style>		
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
FOOTJS;
		
			$monthList = $print[ $appSettings['appLanguage'] ]['appBookingMonths'];
			$dayList = $print[ $appSettings['appLanguage'] ]['appBookingDays'];
			$red = $print[ $appSettings['appLanguage'] ]['appBookingsRed'];
			$blue = $print[ $appSettings['appLanguage'] ]['appBookingsBlue'];
			
			
			
			function draw_calendar($month,$year)
			{
			
				$d = new DateTime( "$year-$month-01" );
				$d->modify( 'first day of next month' );
				$todaysDate = date('Y-m-d');
				$tempDate = '';
				
				$linkBefore = '';
				$linkAfter='<a style="float:right" href="index.php?seek=booking&date='.$d->format('m,Y').'" class="button color medium"> >> </a>';
				if( $year >= date('Y') && $month > date('m') )
				{
					$d->modify( 'first day of previous month' );
					$linkBefore = '<a href="index.php?seek=booking&date='.$d->format('m,Y').'" style="float:left" class="button color medium"> << </a>';
				}else
				{
					$linkBefore = '<span class="button color medium" style="float:left"> << </span>';
				}
				
				/* draw table */
				$monthname = $GLOBALS['monthList'];
				
				$calendar = '
				<div class="calendarControl">
					'.$linkBefore.'
					'.$linkAfter.'
											<h2>'.$monthname[$month].', '.$year.'</h2>
					<span></span>
				</div>
				<table cellpadding="0" cellspacing="0" class="calendar">';

				/* table headings */
				$headings = $GLOBALS['dayList'];
				$calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

				/* days and weeks vars now ... */
				$running_day = date('w',mktime(0,0,0,$month,1,$year));
				$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
				$days_in_this_week = 1;
				$day_counter = 0;
				$dates_array = array();
				
				$CalendarData = array();
				$bookedResult =& dbQuery('SELECT `record_id`, DAY(`book_date`) as day FROM `bookings` WHERE YEAR(bookings.book_date) = "'.$year.'" && MONTH(bookings.book_date) = "'.$month.'"');
				if( dbNumRows($bookedResult)>0 )
				{
					while( ($data =& dbFetchAssoc($bookedResult)) )
					{
					
						if( !isset($CalendarData[ $data['day'] ]) )
						{
							$CalendarData[ $data['day'] ] = 1;
						} else
						{
							$CalendarData[ $data['day'] ] += 1;
						}
							
					}
					dbFreeResult($bookedResult);
				}

				/* row for week one */
				$calendar.= '<tr class="calendar-row">';
				
				
				/* print "blank" days until the first of the current week */
				for($x = 0; $x < $running_day; $x++):

					$calendar.= '<td class="calendar-day-np"> </td>';
					$days_in_this_week++;
				endfor;
				
				$notBookableDaysName = array('sunday', 'saturday', 'Sunday', 'Saturday');
				/* keep going with days.... */
				for($list_day = 1; $list_day <= $days_in_month; $list_day++):
					
					$dayname = date("l", mktime(0, 0, 0, $month, $list_day, $year));		
					$calendar.= '<td class="calendar-day">';
						
						
						/* add in the day number */
						$tempDate = "$year-$month-$list_day";
						if( strtotime($todaysDate) > strtotime($tempDate) )
						{
						
							$calendar .= '<div class="day-number green"><a href="">'.$list_day.'</a></div>';
							$calendar .= '<div class="pastDate"><a href=""><span>NOT BOOKABLE</span></a></div>';						
						} else
						{
							if( isset($CalendarData[ $list_day ])  )
							{
							
								if( $CalendarData[ $list_day ] >= 3 )
								{
									$calendar .= '<div class="day-number red">'.$list_day.'</div>';
									$calendar .= '<div class="notavailable"><a href="index.php?seek=bookingnew&dt='.$list_day.','.$month.','.$year.'"><span>CLOSED</span></a></div>';
								} else
								{
									$calendar.= '<div class="day-number green"><a href="index.php?seek=bookingnew&dt='.$list_day.','.$month.','.$year.'">'.$list_day.'</a></div>';
									$calendar .= '<div class="available"><a href="index.php?seek=bookingnew&dt='.$list_day.','.$month.','.$year.'"><span>AVAILABLE</span></a></div>';
								}
								
							} else
							{
								
								if( in_array($dayname, $notBookableDaysName) )
								{
									// Unbookable days
									$calendar .= '<div class="day-number green"><a href="#">'.$list_day.'</a></div>';
									$calendar .= '<div class="aaavailable"><a href="#"><span>&nbsp;</span></a></div>';
								} else
								{
																	
									$calendar .= '<div class="day-number green"><a href="index.php?seek=bookingnew&dt='.$list_day.','.$month.','.$year.'">'.$list_day.'</a></div>';
									$calendar .= '<div class="available"><a href="index.php?seek=bookingnew&dt='.$list_day.','.$month.','.$year.'"><span>AVAILABLE</span></a></div>';

								}
							}
						
					}
						

						/** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
						
					$calendar.= '</td>';
					if($running_day == 6):
						$calendar.= '</tr>';
						if(($day_counter+1) != $days_in_month):
							$calendar.= '<tr class="calendar-row">';
						endif;
						$running_day = -1;
						$days_in_this_week = 0;
					endif;
					$days_in_this_week++; $running_day++; $day_counter++;
				endfor;

				/* finish the rest of the days in the week */
				if($days_in_this_week < 8):
					for($x = 1; $x <= (8 - $days_in_this_week); $x++):
						$calendar.= '<td class="calendar-day-np"> </td>';
					endfor;
				endif;

				/* final row */
				$calendar.= '</tr>';

				/* end the table */
				$calendar.= '</table>';
				
				$calendar .= '
					<div style="margin-top:20px">
						<p style="display:block; overflow:hidden;"><span class="green" style="display:block; float:left; width:30px; height:30px;  margin-right:20px">&nbsp;</span><span style="padding-top:5px; display:block; float:left;">'.$GLOBALS['red'].'</span></p>
						<p style="display:block; overflow:hidden;"><span class="red"  style="width:30px; height:30px; float:left;margin-right:20px">&nbsp;</span><span style="padding-top:5px; display:block; float:left;">'.$GLOBALS['blue'].'</span></p>						
					</div>
				';
				
				/* all done, return result */
				return $calendar;
			}
			
			
			
			$calendarDetail = '';
			if( isset($_GET['type']) )
			{
			
				switch( $_GET['type'] )
				{
					case 'personal':
						//$calendarDetail = draw_personal_calendar(date('m'),date('Y'));
					break;
					default:

						if( !isset($_GET['date'][0]) )
						{
							$calendarDetail = draw_calendar(date('m'),date('Y'));
						} else
						{
							$dt = explode(',', $_GET['date']);
							$calendarDetail = draw_calendar( $dt[0], $dt[1] );
						}
						
					break;
				}
			
			} else
			{
				if( !isset($_GET['date'][0]) )
				{
					$calendarDetail = draw_calendar(date('m'),date('Y'));
				} else
				{
					$dt = explode(',', $_GET['date']);
					$calendarDetail = draw_calendar( $dt[0], $dt[1] );
				}				
			}
			
	
	

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appBookingHeader']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">

  
    <!-- 12 Columns -->
    <div class="twelve columns">

      	
			{$calendarDetail}
	  
    </div>
    <div class="four columns">
     
		<div class="headline no-margin">
		<h3>{$print[ $appSettings['appLanguage'] ]['appBookingTip']}</h3>
		</div>
		<!-- Tip -->
		<div class="testimonials-carousel" data-autorotate="3000">
			<ul class="carousel">

				<li class="testimonial">
				<div class="testimonials">Start a booking by clicking on any date of choice on the calendar displayed. Your will be shown a form to fill so as to know your booking type</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>
				</li>

				<li class="testimonial">
				<div class="testimonials">Start a booking by clicking on any date of choice on the calendar displayed. Your will be shown a form to fill so as to know your booking type</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>				
				</li>

			</ul>
		</div>
	  
	  
    </div>  
  
  
  </div>
  <!-- 960 Container End -->
  <!-- 960 Container -->
  <!-- 960 Container End -->

BODYCONTENT;
	

?>
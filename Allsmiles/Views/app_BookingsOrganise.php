<?php
	
	require_once dirname(__FILE__).'/__menuTop.php';

	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
		<script type="text/javascript">
        
		document.getElementById('sumbitbutton').disabled = false;
		
		function requestPost()
		{
			return true;
		}
		
		function statusPost( response )
		{
			
			var container = xId("statusContainer");
			response = strip_tags(response);
			response = trim( response );
			
			switch( response.toString() ) 
			{
				case 'success':
				case '["success"]':
					container.innerHTML = '<div class="notification success closeable">Sucessfully Organised the Booking.. Patient Has been sent a notification</div>';				
				break;
				case 'success2':
				case '["success2"]':
					container.innerHTML = '<div class="notification success closeable">Booking Cancellation Completed.. Patient Has been sent a notification</div>';				
				break;				
				default:
				case 'failure':
				case '["failure"]':
					container.innerHTML = '<div class="notification error closeable"><p>Booking Failed</div>';
				break;				
			}

			scrollTo(0,0);
			
		}
		</script>	
FOOTJS;

	
	$dataDetail = array();
	$treatmentList = '';
	$specialist = '';
	
	$resultA =& dbQuery('SELECT `specialist_id`, `doctor_specialist_map`.`record_id`, `lname`, `fname`, `mname` FROM `doctor_specialist_map` LEFT JOIN `record` USING (record_id)');
	
	if( dbNumRows($resultA)>0 )
	{
		while( $row =& dbFetchAssoc($resultA) )
		{
			$specialist .= '<option value="'.$row['record_id'].'">Dr. '.$row['lname'].' '.$row['fname'].'</option>';
		}
		dbFreeResult($resultA);
	}

	$result =& dbQuery('SELECT  `book_id`,  `record_id`,  `book_date`,  `book_time`,  `book_type`,  `book_type_comment`,  `doctor_id`,  `book_status`, `fname`, `mname`, `lname`, `email` FROM `bookings` LEFT JOIN `record` USING (record_id) WHERE `book_id` = '.$_GET['bk']);
	
	
	if( dbNumRows($result)>0 )
	{
	
		$dataDetail = dbFetchAssoc($result);
		dbFreeResult($result);
		
		$result =& dbQuery('SELECT `tid`, `treatment_title` FROM `treatment`');
		if( dbNumRows($result)>0 )
		{
			while( $row =& dbFetchAssoc($result) )
			{
				
				if( $dataDetail['book_type'] == $row['tid'] )
				{
					$treatmentList .= '<option selected value="'.$row['tid'].'">'.$row['treatment_title'].'</option>';			
				} else
				{
					$treatmentList .= '<option value="'.$row['tid'].'">'.$row['treatment_title'].'</option>';
				}
			}
			dbFreeResult($result);
		}		
		
	}
	


	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appBookingHeaderB']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">
    <!-- 1/2 Columns -->
    <div class="twelve columns">
	  
      <div class="headline no-margin">
        <h3>{$print[ $appSettings['appLanguage'] ]['appBookingHeaderOrganise']}</h3>
      </div>
      <div class="large-notice">
		<div id="statusContainer">	
		</div>
        <form method="post" autocomplete="off" onsubmit="return UPLOADER.submit(this, {'onStart' : function(){return requestPost()}, 'onComplete' : statusPost})" action="form.php?url=organisebooking">
			
			<div><p style="font-size:30px; text-align:center;">{$dataDetail['lname']} {$dataDetail['mname']} {$dataDetail['fname']}</p></div>
          <div class="field">
			<input type="hidden" name="uid" value="{$_SESSION['activated']}" />
			<input type="hidden" name="email" value="{$dataDetail['email']}" />
			<input type="hidden" name="bookdatetime" value="{$dataDetail['book_date']} {$dataDetail['book_time']}" />
			<input type="hidden" name="bookfullname" value="{$dataDetail['lname']} {$dataDetail['mname']} {$dataDetail['fname']}" />
			<input type="hidden" name="book_id" value="{$_GET['bk']}" />
            <label>{$print[ $appSettings['appLanguage'] ]['appBookingFormBtype']}:</label>
			<select name="type" readonly="readonly">
				{$treatmentList}
			</select>
          </div>
		  
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['appBookingFormBInvolved']}:</label>
			<select name="d_id">
				{$specialist}
			</select>
          </div>
		  
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['appBookingFormBtime']}:</label>
            <input type="text" name="time" value="{$dataDetail['book_time']}" readonly="readonly" id="time" class="text" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['appBookingFormBcomment']}:</label>
            <textarea name="comment">{$dataDetail['book_type_comment']}</textarea>
          </div>		  
          <div class="field">
            <input type="submit" name="confirmButton" id="sumbitbutton" value="{$print[ $appSettings['appLanguage'] ]['appBookingConfirmSubmit']}" style="margin-right:10px;"/>
            <input type="submit" name="cancelButton" style="background:#FF0000; border:1px solid #F00000" id="sumbitbuttonB" value="{$print[ $appSettings['appLanguage'] ]['appBookingCancelSubmit']}"/>
            <div class="loading"></div>
          </div>
      </form>
        <p>&nbsp;</p></div>
      <p>&nbsp;</p>
    </div>
    <div class="four columns">

		<div class="headline no-margin">
			<h3>{$print[ $appSettings['appLanguage'] ]['appBookingTip']}</h3>
		</div>
		<!-- Tip -->
		<div class="testimonials-carousel" data-autorotate="3000">
			<ul class="carousel">

				<li class="testimonial">
				<div class="testimonials">Posuere erat a ante venenatis dapibus posuere velit aliquet. Duis llis, est non coeammodo luctus, nisi erat porttitor ligula, egeteas laciniato odiomo sem.</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Michael, <span>Flash Developer</span></div>
				</li>

				<li class="testimonial">
				<div class="testimonials">Integer eu libero sit amet nisl vestibulum semper. Fusce nec porttitor massa. In nec neque elit. Curabitur malesuada ligula vitae purus ornare onec etea magna diam varius.</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">John, <span>Web Developer</span></div>
				</li>

				<li class="testimonial">
				<div class="testimonials">Cras sed odio est, sit amet porttitor elit. Vestibulum elementum erat vitae ante venenatis cursus scelerisque quam vel nibh imperdiet vitae porta lorem posuere.</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Peter, <span>Project Manager</span></div>
				</li>

				<li class="testimonial">
				<div class="testimonials">Elementum erat vitae ante venenatis dapibus. Maecenas cursus scelerisque quam vel nibh imperdiet vitae porta lorem posuere.</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Kathy, <span>Art Director</span></div>
				</li>

			</ul>
		</div>	
	
    </div>
    <!-- 1/2 Columns End -->
  </div>

BODYCONTENT;
	

?>
<?php

	require_once dirname(__FILE__).'/__menuTop.php';

	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >

<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script type="text/javascript" src="assets/build/js/jquery.min.js"></script>
        
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
		<script type="text/javascript">
        
        $(function(){
        	$('#birthday').Zebra_DatePicker();
		});
        document.getElementById('sumbitbutton').disabled = false;
		
		function requestPost()
		{
			return true;
		}
		
		function statusPost( response )
		{
			
			var container = xId("statusContainer");
			response = strip_tags(response);
			response = trim( response );
			
			switch( response.toString() ) 
			{
				case 'validation-error':
				case '["validation-error"]':
					container.innerHTML = '<div class="notification warning closeable">Validation Error.. Please Fill All Fields</div>';
				break;
				case 'success':
				case '["success"]':
					container.innerHTML = '<div class="notification success closeable">Account Successfully Created</div>';
				break;
				default:
				case 'failure':
				case '["failure"]':
					container.innerHTML = '<div class="notification warning closeable">Account Registration Failed</div>';
				break;				
			}

			scrollTo(0,0);
			
		}
		</script>
FOOTJS;
	
	
	
	if( $_GET['acctT'] == 2 )
	{
		$acctType =<<<ROLE
			<input type="hidden" name="role" value="{$_GET['acctT']}" />
			  <div class="field">
				<label>{$print[ $appSettings['appLanguage'] ]['signupAcctType']}:</label>
				<select name="doctorType">
					<option value="1">Orthondontists</option>
					<option value="2">Anesthetic</option>
					<option value="3">Dental Assistant</option>
					<option value="4">Dentist</option>
				</select>
			  </div>			
ROLE;
	} else
	{
		$acctType =<<<ROLE
			<input type="hidden" name="role" value="{$_GET['acctT']}" />
ROLE;
	}

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	
<!-- Wrapper Start -->

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appSuperCreationTitle']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">
    <!-- 1/2 Columns -->
    <div class="twelve columns">
      <div class="headline no-margin">
        <h3>{$print[ $appSettings['appLanguage'] ]['signupHeaderS']} </h3>
      </div>
      <div class="large-notice">
		<div id="statusContainer"></div>
        <form method="post" autocomplete="off" enctype="multipart/form-data" onsubmit="return UPLOADER.submit(this, {'onStart' : function(){return requestPost()}, 'onComplete' : statusPost})" action="form.php?url=addaccount">
          
		  {$acctType}

		  
		  <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupUsername']}:</label>
            <input type="text" name="username" class="text" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupPassword']}:</label>
            <input type="password" name="password" class="text" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupConfirmPassword']}:</label>
            <input type="password" name="confirmpassword" class="text" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupTitle']}:</label>
			<select name="title"><option>Mr</option><option>Mrs</option><option>Ms.</option></select>
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupSex']}:</label>
			<select name="sex"><option value="M">Male</option><option value="F">Female</option></select>
          </div>		  
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupInitials']}:</label>
            <input type="text" name="initials" class="text" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupFirstname']}:</label>
            <input type="text" name="firstname" class="text" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupLastname']}:</label>
            <input type="text" name="lastname" class="text" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupBirthday']}:</label>
            <input type="text" name="birthday" id="birthday" class="text" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupTelephoneno']}:</label>
            <input type="text" name="telephoneno" class="text" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupAddress1']}:</label>
            <textarea name="address1"></textarea>
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupAddress2']}:</label>
            <textarea name="address2"></textarea>
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupCity']}:</label>
            <input type="text" name="city" class="text" />
          </div>		
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupCountry']}:</label>
			<select name="country">
				<option value="">Select Country</option>
				<option>Afghanistan</option>
				<option>Albania</option>
				<option>Algeria</option>
				<option>Andorra</option>
				<option>Angola</option>
				<option>Antigua and Barbuda</option>
				<option>Argentina</option>
				<option>Armenia</option>
				<option>Aruba</option>
				<option>Australia</option>
				<option>Austria</option>
				<option>Azerbaijan</option>
				<option>Bahamas</option>
				<option>Bahrain</option>
				<option>Bangladesh</option>
				<option>Barbados</option>
				<option>Belarus</option>
				<option>Belgium</option>
				<option>Belize</option>
				<option>Benin</option>
				<option>Bermuda</option>
				<option>Bhutan</option>
				<option>Bolivia</option>
				<option>Bosnia</option>
				<option>Botswana</option>
				<option>Brazil</option>
				<option>Brunei</option>
				<option>Bulgaria</option>
				<option>Burkina Faso</option>
				<option>Burma</option>
				<option>Burundi</option>
				<option>Cambodia</option>
				<option>Cameroon</option>
				<option>Canada</option>
				<option>Cape Verde</option>
				<option>Cayman Islands</option>
				<option>Central African Rep</option>
				<option>Chad</option>
				<option>Chile</option>
				<option>China</option>
				<option>Colombia</option>
				<option>Comoros</option>
				<option>Congo</option>
				<option>Costa Rica</option>
				<option>Croatia</option>
				<option>Cuba</option>
				<option>Cyprus</option>
				<option>Czech Republic</option>
				<option>Denmark</option>
				<option>Djibouti</option>
				<option>Dominica</option>
				<option>Dom. Republic</option>
				<option>Ecuador</option>
				<option>Egypt</option>
				<option>El Salvador</option>
				<option>Equatorial Guinea</option>
				<option>Eritrea</option>
				<option>Estonia</option>
				<option>Ethiopia</option>
				<option>Fiji</option>
				<option>Finland</option>
				<option>France</option>
				<option>Gabon</option>
				<option>Gambia</option>
				<option>Georgia</option>
				<option>Germany</option>
				<option>Ghana</option>
				<option>Gibraltar</option>
				<option>Greece</option>
				<option>Grenada</option>
				<option>Guadeloupe</option>
				<option>Guatemala</option>
				<option>Guinea</option>
				<option>Guinea Bissau</option>
				<option>Guyana</option>
				<option>Honduras</option>
				<option>Hong Kong</option>
				<option>Haiti</option>
				<option>Hungary</option>
				<option>Iceland</option>
				<option>India</option>
				<option>Indonesia</option>
				<option>Iran</option>
				<option>Iraq</option>
				<option>Ireland</option>
				<option>Israel</option>
				<option>Italy</option>
				<option>Ivory Coast</option>
				<option>Jamaica</option>
				<option>Japan</option>
				<option>Jersey</option>
				<option>Jordan</option>
				<option>Kazakhstan</option>
				<option>Kenya</option>
				<option>Kuwait</option>
				<option>Laos</option>
				<option>Latvia</option>
				<option>Lebanon</option>
				<option>Lesotho</option>
				<option>Liberia</option>
				<option>Libya</option>
				<option>Liechtenstein</option>
				<option>Lithuania</option>
				<option>Luxembourg</option>
				<option>Macau</option>
				<option>Macedonia</option>
				<option>Madagascar</option>
				<option>Malawi</option>
				<option>Malaysia</option>
				<option>Maldives</option>
				<option>Mali</option>
				<option>Malta</option>
				<option>Martinique</option>
				<option>Mauritania</option>
				<option selected>Mauritius</option>
				<option>Mexico</option>
				<option>Moldova</option>
				<option>Monaco</option>
				<option>Mongolia</option>
				<option>Morocco</option>
				<option>Mozambique</option>
				<option>Namibia</option>
				<option>Nepal</option>
				<option>Netherlands</option>
				<option>Netherlands Antilles</option>
				<option>New Zealand</option>
				<option>Nicaragua</option>
				<option>Niger</option>
				<option>Nigeria</option>
				<option>North Korea</option>
				<option>Norway</option>
				<option>Oman</option>
				<option>Pakistan</option>
				<option>Panama</option>
				<option>Paraguay</option>
				<option>Peru</option>
				<option>Philippines</option>
				<option>Poland</option>
				<option>Portugal</option>
				<option>Qatar</option>
				<option>Romania</option>
				<option>Russia</option>
				<option>Rwanda</option>
				<option>San Marino</option>
				<option>Saudi Arabia</option>
				<option>Senegal</option>
				<option>Seychelles</option>
				<option>Sierra Leone</option>
				<option>Singapore</option>
				<option>Slovakia</option>
				<option>Slovenia</option>
				<option>Somalia</option>
				<option>South Africa</option>
				<option>South Korea</option>
				<option>Sri Lanka</option>
				<option>Sudan</option>
				<option>Spain</option>
				<option>Suriname</option>
				<option>Sweden</option>
				<option>Switzerland</option>
				<option>Syria</option>
				<option>Taiwan</option>
				<option>Tajikistan</option>
				<option>Tanzania</option>
				<option>Thailand</option>
				<option>Togo</option>
				<option>Trinidad and Tobago</option>
				<option>Tunisia</option>
				<option>Turkey</option>
				<option>Turkmenistan</option>
				<option>Turks and Caicos Islands</option>
				<option>Uganda</option>
				<option>Ukraine</option>
				<option>United Arab Emirates</option>
				<option>United Kingdom</option>
				<option>United States</option>
				<option>Uruguay</option>
				<option>Uzbekistan</option>
				<option>Venezuela</option>
				<option>Vietnam</option>
				<option>Yemen</option>
				<option>Yugoslavia</option>
				<option>Zambia</option>
				<option>Zimbabwe</option>                  
			</select>
          </div>		
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['signupProfileImage']}:</label>
            <input type="file" name="image" class="text" />
          </div>				  
          <div class="field">
            <input type="submit" id="sumbitbutton" value="{$print[ $appSettings['appLanguage'] ]['signupButton']}"/>
            <div class="loading"></div>
          </div>
      </form>
        <p>&nbsp;</p></div>
      <p>&nbsp;</p>
    </div>
    <div class="four columns">
     
    </div>
    <!-- 1/2 Columns End -->
  </div>
  <!-- 960 Container End -->
  <!-- 960 Container -->
  <!-- 960 Container End -->

BODYCONTENT;
	

?>
<?php
	
	checkSessionStatus();
	require_once dirname(__FILE__).'/__menuTop.php';
	
	
	
	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
FOOTJS;
		
	
    $HTML = '
		<table class="standard-table">
			<tr>
				<th width="2%">#</th>
				<th width="38%">Booking By</th>
				<th width="30%">Booking Category</th>
				<th width="10%">Appointment Timing</th>
                <th width="10%">Options</th>	
			</tr>
    ';
    
	$specialistCat = array();
	
	
	$resultA =& dbQuery('SELECT `specialist_id`, `title` FROM `specialties`');
	if( dbNumRows($resultA)>0 )
	{
		while( $row =& dbFetchAssoc($resultA) )
		{
			$specialistCat[$row['specialist_id']] = $row['title'];
		}
		dbFreeResult($resultA);

		
		$result =& dbQuery('SELECT `book_id`, `record_id`, `book_date`, `book_time`, `book_type`, `book_category`,  `book_type_comment`, `doctor_id`, `book_status`, `title`, `fname`, `mname`, `lname`, NOW() as `timenow` FROM `bookings` LEFT JOIN `record` USING (record_id) WHERE `book_status` = "NOT BOOKED" ORDER BY `book_date` DESC, `book_time` DESC');
		
		if( dbNumRows($result)>0 )
		{
			
			$y = 0;
			while( $row =& dbFetchAssoc($result) )
			{
				$date = '';
				if( strtotime($row['book_date'].' '.$row['book_time']) > strtotime($row['timenow']) )
				{
					$date = date('D jS \of F Y h:i:s A', convert_datetime($row['book_date'].' '.$row['book_time']));
				} else
				{
					$date = ago($row['timenow'], $row['book_date'].' '.$row['book_time']);
				}
				$y++;
				$HTML .= '
						<tr>
							<td>'.$y.'</td>
							<td>'.ucwords($row['title'].' '.$row['lname'].' '.$row['mname'].' '.$row['fname']).'</td>
							<td style="background:#EEEEEE; font-weight:700">'.$specialistCat[$row['book_category']].'</td>
							<td>'.$date.'</td>
							<td><a href="index.php?seek=bookingorganise&bk='.$row['book_id'].'" class="button color">Organise</a></td>	
						</tr>            
				';
			}
			$HTML .= '</table>';
			dbFreeResult($result);
		
		}		

	}
	

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appBookingHeaderB']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">
    <!-- 12 Columns -->
    <div class="twelve columns">
		
        <div class="headline no-margin">
        	<h3>{$print[ $appSettings['appLanguage'] ]['appBookingViewList']}</h3>
        </div>
	
      <div class="large-notice">
            <div id="statusContainer"></div>
            <form method="post" autocomplete="off" onsubmit="return UPLOADER.submit(this, {'onStart' : function(){return requestPost()}, 'onComplete' : statusPost})" action="form.php?url=subscription">
            
                <div style="display:block;overflow:hidden">
                
                    {$HTML}       
            
                 </div>
            
         	 </form>
       </div>
       
       		
    </div>

    <div class="four columns">
    </div>
    
  
  
  </div>
  <!-- 960 Container End -->
  <!-- 960 Container -->
  <!-- 960 Container End -->

BODYCONTENT;
	

?>
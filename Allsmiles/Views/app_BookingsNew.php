<?php
	
	require_once dirname(__FILE__).'/__menuTop.php';

	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
	<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >
	<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
		<script type="text/javascript">
        
        $(function(){
         	$("#time").clockpick({ valuefield: 'time',useBgiframe: false,starthour : 9,endhour : 17, } ); 	
			$('#bookingForm').easyValidate();
        	}
        );
		document.getElementById('sumbitbutton').disabled = false;
		
		function requestPost()
		{
			return true;
		}
		
		function statusPost( response )
		{
			
			var container = xId("statusContainer");
			response = strip_tags(response);
			response = trim( response );
			
			switch( response.toString() ) 
			{
				case 'success':
				case '["success"]':
					container.innerHTML = '<div class="notification success closeable">Sucessfully Made a booking<p>You will be notified in your profile inbox</p></div>';				
				break;
				default:
				case 'failure':
				case '["failure"]':
					container.innerHTML = '<div class="notification error closeable"><p>Booking Failed</p><a class="close" href="#"></a></div>';
				break;				
			}

			scrollTo(0,0);
			
		}
		</script>	
FOOTJS;


	$monthname = array(
		'01' => 'January',
		'02' => 'February',
		'03' => 'March',
		'04' => 'April',
		'05' => 'May',
		'06' => 'June',
		'07' => 'July',
		'08' => 'August',
		'09' => 'September',
		'10' => 'October',
		'11' => 'November',
		'12' => 'December'
	);
	
	$bookingdate = '';
	$titleDate= '';
	if( isset($_GET['dt'][0]) )
	{
		$dta = explode(',', $_GET['dt']);
		$titleDate = $dta[0].' '.$monthname[ $dta[1] ].', '.$dta[2];
		$bookingdate = $dta[2].'-'.$dta[1].'-'.$dta[0];
	}
	
	
	$treatmentList = '';
	$result =& dbQuery('SELECT `tid`, `treatment_title` FROM `treatment`');
	if( dbNumRows($result)>0 )
	{
		while( $row =& dbFetchAssoc($result) )
		{
			$treatmentList .= '<option value="'.$row['tid'].'">'.$row['treatment_title'].'</option>';
		}
		dbFreeResult($result);
	}

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appBookingTitle']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">
    <!-- 1/2 Columns -->
    <div class="twelve columns">
	  
      <div class="headline no-margin">
        <h3>{$print[ $appSettings['appLanguage'] ]['appBookingHeader']}</h3>
      </div>
      <div class="large-notice">
		<div id="statusContainer">	
		</div>
        <form method="post" id="bookingForm" autocomplete="off" onsubmit="return UPLOADER.submit(this, {'onStart' : function(){return requestPost()}, 'onComplete' : statusPost})" action="form.php?url=newbooking">
		  <div><p style="font-size:30px; text-align:center;">{$titleDate}</p></div>
          <div class="field">
			<input type="hidden" name="date" value="{$bookingdate}" />
			<input type="hidden" name="uid" value="{$_SESSION['activated']}" />
            <label>{$print[ $appSettings['appLanguage'] ]['appBookingFormBtype']}:</label>
			<select name="type" class="required">
				{$treatmentList}
			</select>
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['appBookingFormBtime']}:</label>
            <input type="text" name="time" readonly="readonly" id="time" class="text required" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['appBookingFormBcomment']}:</label>
            <textarea name="comment"></textarea>
          </div>		  
          <div class="field">
            <input type="submit" id="sumbitbutton" value="{$print[ $appSettings['appLanguage'] ]['appBookingSubmit']}"/>
            <div class="loading"></div>
          </div>
      </form>
        <p>&nbsp;</p></div>
      <p>&nbsp;</p>
    </div>
    <div class="four columns">

		<div class="headline no-margin">
			<h3>{$print[ $appSettings['appLanguage'] ]['appBookingTip']}</h3>
		</div>
		<!-- Tip -->
		<div class="testimonials-carousel" data-autorotate="3000">
			<ul class="carousel">

				<li class="testimonial">
				<div class="testimonials">You are required to fill the form. Selecting the type of appointment you want to have with any of Allsmiles dental clinic staff</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>
				</li>

				<li class="testimonial">
				<div class="testimonials">You are required to fill the form. Selecting the type of appointment you want to have with any of Allsmiles dental clinic staff</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>				
				</li>

			</ul>
		</div>	
	
    </div>
    <!-- 1/2 Columns End -->
  </div>

BODYCONTENT;
	

?>
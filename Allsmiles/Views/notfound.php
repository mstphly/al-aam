<?php

	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/metro.css" rel="stylesheet" />
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="assets/css/style.css" rel="stylesheet" />
<link href="assets/css/style_responsive.css" rel="stylesheet" />
<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
	<script language='JavaScript'>
    //VicarePlus :: Validation function for checking the hashing algorithm used for encrypting passwordfunction chk_hash_fn(){	var str = document.forms[0].authUser.value;	$.ajax({	url: "app/interface/login/validateUser.php?u="+str,	context: document.body,	success: function(data)	{		if(data == 0) //VicarePlus :: If the hashing algorithm is 'MD5'		{			document.forms[0].authPass.value=MD5(document.forms[0].clearPass.value);			document.forms[0].authNewPass.value=SHA1(document.forms[0].clearPass.value);		}		else  //VicarePlus :: If the hashing algorithm is 'SHA1'		{			document.forms[0].authPass.value=SHA1(document.forms[0].clearPass.value);		}		document.forms[0].clearPass.value='';				imsubmitted();		document.login_form.submit();	}	});}
    </script>	
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS  <!-- END COPYRIGHT -->  <!-- BEGIN JAVASCRIPTS -->  <script src="assets/js/jquery-1.8.3.min.js"></script>  <script src="assets/bootstrap/js/bootstrap.min.js"></script>    <script src="assets/uniform/jquery.uniform.min.js"></script>   <script src="assets/js/jquery.blockui.js"></script>  <script src="assets/js/app.js"></script>  <script>	jQuery(document).ready(function() {     	  App.initLogin();	});  </script>  <!-- END JAVASCRIPTS -->
FOOTJS;


	$pageProperties['bodyContent'] = <<<BODYCONTENT

	<!-- Wrapper Start -->
<div id="wrapper">
  <!-- Header
================================================== -->
  <!-- 960 Container -->
  <div class="container ie-dropdown-fix">
    <!-- Header -->
    <div id="header">
      <!-- Logo -->
      <div class="eight columns">
        <div id="logo"> <a href="#"><img src="images/logo.png" alt="logo" /></a>
          <div class="clear"></div>
        </div>
      </div>
      <!-- Social / Contact -->
      <div class="eight columns">
        <!-- Social Icons -->
        <ul class="social-icons">
          <li class="facebook"><a href="#">Facebook</a></li>
          <li class="twitter"><a href="#">Twitter</a></li>
          <li class="linkedin"><a href="#">LinkedIn</a></li>
        </ul>
        <div class="clear"></div>
        <!-- Contact Details -->
        <div id="contact-details">
          <ul>
            <li><i class="mini-ico-envelope"></i><a href="#">support@allsmilesdental.com</a></li>
            <li><i class="mini-ico-user"></i>4123 456
          </li></ul>
        </div>
      </div>
    </div>
    <!-- Header / End -->
    <!-- Navigation -->
    <div class="sixteen columns">
      <div id="navigation">
        <ul id="nav">
          <li><a href="index.html">Home</a></li>
          <li><a href="#">Submenu Item</a>
            <ul>
              <li><a href="#">Submenu One</a></li>
              <li><a href="#">Submenu Two</a></li>
              <li><a href="#">Submenu Three</a></li>
              <li><a href="#">Submenu Four</a></li>
            </ul>
          </li>
          <li><a href="#">Shortcodes</a></li>
          <li><a href="#">Elements</a></li>
          <li><a href="#">Menu Item</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
        <!-- Search Form -->
        <div class="search-form">
          <form method="get" action="#">
            <input type="text" class="search-text-box" />
          </form>
        </div>
      </div>
      <div class="clear"></div>
    </div>
    <!-- Navigation / End -->
  </div>
  <!-- 960 Container / End -->
  <!-- Content
================================================== -->
  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>Page Status</h2>
        <div id="bolded-line"></div><h1 style="text-align:center;">{$print[ $appSettings['appLanguage'] ]['pageStatus']}</h1>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">
    <!-- 1/2 Columns -->

    <!-- 1/2 Columns End -->
  </div>
  <!-- 960 Container End -->
  <!-- 960 Container -->
  <!-- 960 Container End -->
</div>
<!-- Wrapper / End -->


<!-- Footer
================================================== -->

<!-- Footer Start -->
<div id="footer">
  <!-- 960 Container -->
  <div class="container">
    <!-- About -->
    <div class="four columns">
      <div class="footer-headline">
        <h4>About AllSmiles</h4>
      </div>
      <p>Consectetur adipiscing elit aeneane lorem lipsum, condimentum ultrices consequat eu, vehicula mauris lipsum adipiscing lipsum aenean orci lorem.</p>
    </div>
    <!-- Useful Links -->
    <div class="four columns">
      <div class="footer-headline">
        <h4>Our Services</h4>
      </div>
      <ul class="links-list">
        <li><a href="#">Service One</a></li>
        <li><a href="#">Service Two</a></li>
        <li><a href="#">Service Three</a></li>
        <li><a href="#">Service Four</a></li>
      </ul>
    </div>
    <!-- Photo Stream -->
    <div class="four columns">
      <div class="footer-headline">
        <h4>Our Policy</h4>
      </div>
      <p>Consectetur adipiscing elit aeneane lorem lipsum, condimentum ultrices consequat eu, vehicula mauris lipsum adipiscing lipsum aenean orci lorem.</p>
    </div>
    <!-- Latest Tweets -->
    <div class="four columns">
      <div class="footer-headline">
        <h4>Latest Tweets</h4>
      </div>
      <ul id="twitter">
      </ul>
      <script type="text/javascript">			jQuery(document).ready(function($){			$.getJSON('http://api.twitter.com/1/statuses/user_timeline/cnn.json?count=2&amp;callback=?', function(tweets){			$("#twitter").html(tz_format_twitter(tweets));			}); });		</script>
      <div class="clear"></div>
    </div>
    <!-- Footer / Bottom -->
    <div class="sixteen columns">
      <div id="footer-bottom"> &copy; Copyright 2013 AllSmiles Dental Clinic. All rights reserved.
        <div id="scroll-top-top"><a href="#"></a></div>
      </div>
    </div>
  </div>
  <!-- 960 Container / End -->
</div>
<!-- Footer / End -->
	
BODYCONTENT;
	

?>
<?php
	
	require_once dirname(__FILE__).'/__menuTop.php';

	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
	<script src="assets/build/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
	<script type="text/javascript">$(function(){	$('#loginForm').easyValidate();});
	document.getElementById('sumbitbutton').disabled = false;
	function requestPost(){	return true;}function statusPost( response ){	var container = xId("statusContainer");	response = strip_tags(response);	response = trim( response );		switch( response.toString() ) 	{		case 'success':		case '["success"]':			container.innerHTML = '<div class="notification success closeable">Successfully Logged In.. Redirecting to Profile</div>';			location.href = "index.php?seek=profile";							break;		default:		case 'failure':		case '["failure"]':			container.innerHTML = '<div class="notification error closeable"><p>Account Logon	 Failed</p><a class="close" href="#"></a></div>';		break;					}
	scrollTo(0,0);	}
	</script>	
FOOTJS;

	$select['en'] = '';
	$select['fr'] = '';
	switch( $appSettings['appLanguage'] )
	{
		case 'en':
			$select['en'] = 'selected="selected"';
		break;
		case 'fr':
			$select['fr'] = 'selected="selected"';
		break;
	}


	$pageProperties['bodyContent'] = <<<BODYCONTENT

	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appAccessHeader']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">
    <!-- 1/2 Columns -->
    <div class="eight columns">
	  
      <div class="headline no-margin">
        <h3>{$print[ $appSettings['appLanguage'] ]['loginHeader']}</h3>
      </div>
      <div class="large-notice"><div id="statusContainer"></div>
        <form method="post" autocomplete="off" id="loginForm" onsubmit="return UPLOADER.submit(this, {'onStart' : function(){return requestPost()}, 'onComplete' : statusPost})" action="form.php?url=signin">
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['loginUsername']}:</label>
            <input type="text" name="username" class="text required" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['loginPassword']}:</label>
            <input type="password" name="password" class="text required" />
          </div>
          <div class="field">
            <label>{$print[ $appSettings['appLanguage'] ]['loginLang']}:</label>	<select name="lang" {$select['en']}>	<option value="en">English</option>	<option value="fr" {$select['fr']}>French</option>	</select>
          </div>		  
          <div class="field">
            <input type="submit" id="sumbitbutton" value="{$print[ $appSettings['appLanguage'] ]['loginButton']}"/>
            <div class="loading"></div>
          </div>
      </form>
        <p>&nbsp;</p></div>
      <p>&nbsp;</p>
    </div>
    <div class="eight columns">
      <div class="headline no-margin">
        <h3>{$print[ $appSettings['appLanguage'] ]['signupHeader']}</h3>
      </div>
      <div class="large-notice">
        <h2>{$print[ $appSettings['appLanguage'] ]['signupPhrase']}</h2>
        <p>{$print[ $appSettings['appLanguage'] ]['signupPhraseInfo']}</p>
        <a href="index.php?seek=signup" class="button medium color">{$print[ $appSettings['appLanguage'] ]['signupNewButton']}</a></div>
    </div>
    <!-- 1/2 Columns End -->
  </div>

BODYCONTENT;
	

?>
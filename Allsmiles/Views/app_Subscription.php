<?php
	
	require_once dirname(__FILE__).'/__menuTop.php';

	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/build/css/boxed.css" id="layout">
<link rel="stylesheet" type="text/css" href="assets/build/css/colors/green.css" id="colors">
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script src="Behaviours/formController.js"></script>
		<script src="assets/build/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
		<script type="text/javascript">
		document.getElementById('sumbitbutton').disabled = false;
		
		function requestPost()
		{
			return true;
		}
		
		function statusPost( response )
		{
			
			var container = xId("statusContainer");
			response = strip_tags(response);
			response = trim( response );
			
			switch( response.toString() ) 
			{
				case 'success':
				case '["success"]':
					container.innerHTML = '<div class="notification success closeable">You Have Booked for a subscription<p>You are now required to log on to your appropriate payment provider to pay up your billing</p></div>';				
				break;
				default:
				case 'failure':
				case '["failure"]':
					container.innerHTML = '<div class="notification error closeable"><p>Booking Failed</p><a class="close" href="#"></a></div>';
				break;				
			}

			scrollTo(0,0);
			
		}
		
		
		function selectPrice()
		{
			
		}
		</script>	
FOOTJS;
	
	$subscriptionSelect = '';
	$packageTypes= array();
	$result =& dbQuery("SELECT  `sp_id`,  `sp_title`,  `sp_type`,  `sp_duration`, `sp_amount` FROM `subscription_package`");
	if( dbNumRows($result) )
	{
		while( ($row =& dbFetchAssoc($result)) )
		{
			$packageTypes[ $row['sp_id'] ] = array('title'=>$row['sp_title'], 'type'=>$row['sp_type'], 'duration'=>$row['sp_duration'], 'amount'=>$row['sp_amount']);
		}
		dbFreeResult($result);
	}
	
	foreach( $packageTypes as $k=>$v )
	{
		$subscriptionSelect .= '<option value="'.$k.'">'.$v['title'].'</option>';
	}
	
	
	$existingHTML = '
		<table class="standard-table">
			<tr>
				<th width="2%">#</th>
				<th width="10%">Package Title</th>
				<th width="68%">Package Type</th>
				<th width="10%">Date Activated</th>
				<th width="10%">Date Expires</th>	
				<th width="10%">Status</th>	
			</tr>	
	';
	$resultB =& dbQuery("SELECT  `sl_id`,  `subscription_id`,  `record_id`,  `activated`,  `expires`, `status` FROM `subscription_list`");
	if( dbNumRows($resultB) )
	{
		$y = 0;
		while( ($row =& dbFetchAssoc($resultB)) )
		{
			$y++;
			$existingHTML .= '
				<tr>
					<td width="2%">'.$y.'</td>
					<td width="10%">'.$packageTypes[$row['subscription_id']]['title'].'</td>
					<td width="68%">'.$packageTypes[$row['subscription_id']]['type'].'</td>
					<td width="10%">'.$row['activated'].'</td>
					<td width="10%">'.$row['expires'].'</td>
					<td width="10%">'.$row['status'].'</td>
				</tr>				
			';
		}
		dbFreeResult($resultB);
		
		$existingHTML = '</table>';
	
	} else
	{
		$existingHTML = '<div class="notification notice closeable" style="display: block;">
		<p>There is no subscription Active for you at this moment</p>
		<a href="#" class="close"></a>
		</div>';
	}
	

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appSubscriptionTitle']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">
    <!-- 1/2 Columns -->
    <div class="twelve columns">

	<div class="headline no-margin">
		<h3>{$print[ $appSettings['appLanguage'] ]['appSubscriptionExistingTitle']}</h3>
	</div>	
		
	<div style="display:block; overflow:hidden;">
		{$existingHTML}
	</div>
	
	
		
		
	<div class="headline no-margin">
	<h3>{$print[ $appSettings['appLanguage'] ]['appSubscriptionNewTitle']}</h3>
	</div>
	
	
      <div class="large-notice">
		<div id="statusContainer">	
		</div>
        <form method="post" autocomplete="off" onsubmit="return UPLOADER.submit(this, {'onStart' : function(){return requestPost()}, 'onComplete' : statusPost})" action="form.php?url=subscription">

          <div class="field">
			<input type="hidden" name="uid" value="{$_SESSION['activated']}" />
            <label>{$print[ $appSettings['appLanguage'] ]['appSubscriptionFormStype']}:</label>
			<select name="subscription" onchange="selectPrice();">{$subscriptionSelect}</select>
			<input type="hidden" name="duration" value="31" />
          </div>

    	  <div class="field">
			<label>{$print[ $appSettings['appLanguage'] ]['appSubscriptionFormSprice']}:</label>
			<input type="text" readonly=readonly name="price" id="price" value="5000" />
		  </div>
		  
			<div class="field">
				<label>{$print[ $appSettings['appLanguage'] ]['appSubscriptionFormScurrency']}</label>
				<select>
					<option>Dollars ($)</option>
					<option>Pound (£)</option>										
				</select>
			</div>		  

			<div class="field">
				<label>{$print[ $appSettings['appLanguage'] ]['appSubscriptionFormSpaymentprovider']}</label>
				<select name="pay_provider">
					<option>Branch Collect</option>
					<option>Google e-Wallet</option>	
					<option>Visa</option>
					<option>Master Card</option>
					<option>Pay Pal</option>
					<option>Pro Pay</option>
				</select>					
			</div>		  

		<div class="notification notice" style="display: block;">
			{$print[ $appSettings['appLanguage'] ]['appSubscriptionNote']}
			<a class="close" href="#"></a>
		</div>
			
          <div class="field">
            <input type="submit" id="sumbitbutton" value="{$print[ $appSettings['appLanguage'] ]['appSubscriptionButton']}"/>
            <div class="loading"></div>
          </div>
      </form>
        <p>&nbsp;</p></div>
      <p>&nbsp;</p>
    </div>
    <div class="four columns">

		<div class="headline no-margin">
		<h3>{$print[ $appSettings['appLanguage'] ]['appSubscriptionTip']}</h3>
		</div>

		<!-- Tip -->
		<div class="testimonials-carousel" data-autorotate="3000">
			<ul class="carousel">

				<li class="testimonial">
				<div class="testimonials">Our subscription package allows you to visit the clinic as often as you like, you are required to fill in the type of subscription you prefer for yourself</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>
				</li>

				<li class="testimonial">
				<div class="testimonials">Our subscription package allows you to visit the clinic as often as you like, you are required to fill in the type of subscription you prefer for yourself</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>
				</li>

			</ul>
		</div>		
	
	
    </div>
    <!-- 1/2 Columns End -->
  </div>

BODYCONTENT;
	

?>
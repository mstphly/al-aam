<?php
	
	require_once dirname(__FILE__).'/__menuTop.php';

	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
		<script type="text/javascript">
		document.getElementById('sumbitbutton').disabled = false;
		
		function requestPost()
		{
			return true;
		}
		
		function statusPost( response )
		{
			
			var container = xId("statusContainer");
			response = strip_tags(response);
			response = trim( response );
			
			switch( response.toString() ) 
			{
				case 'success':
				case '["success"]':
					container.innerHTML = '<div class="notification success closeable">You Have Successfully Updated The Subscription Packages</div>';				
				break;
				default:
				case 'failure':
				case '["failure"]':
					container.innerHTML = '<div class="notification error closeable"><p>Update Failed.. Please Try Again</p><a class="close" href="#"></a></div>';
				break;				
			}

			scrollTo(0,0);
			
		}
		
		
		function selectPrice()
		{
			
		}
		</script>	
FOOTJS;
	
	$subscriptionSelect = '';
	$packageTypes= array();
	$result =& dbQuery("SELECT  `sp_id`,  `sp_title`,  `sp_type`,  `sp_duration`, `sp_amount` FROM `subscription_package`");
	if( dbNumRows($result) )
	{
		while( ($row =& dbFetchAssoc($result)) )
		{
			$packageTypes[ $row['sp_id'] ] = array('title'=>$row['sp_title'], 'type'=>$row['sp_type'], 'duration'=>$row['sp_duration'], 'amount'=>$row['sp_amount']);
		}
		dbFreeResult($result);
	}
	
	foreach( $packageTypes as $k=>$v )
	{
		$subscriptionSelect .= '<option value="'.$k.'">'.$v['title'].'</option>';
	}
	
	
	$existingHTML = '
		<table class="standard-table">
			<tr>
				<th width="2%">#</th>
				<th width="10%">Package Title</th>
				<th width="68%">Package Type</th>
				<th width="10%">Package Duration</th>
				<th width="10%">Package Amount</th>	
			</tr>	
	';
    
    $resultB =& dbQuery("SELECT * FROM `subscription_package`");
    if( dbNumRows($resultB) )
    {
    
		$y = 0;
		while( ($row =& dbFetchAssoc($resultB)) )
		{

			$y++;
			$existingHTML .= '
				<tr>
					<td>'.$y.'</td>
					<td>'.$row['sp_title'].'</td>
					<td>'.$row['sp_type'].'</td>
					<td><input type="text" name="duration['.$row['sp_id'].']" value="'.$row['sp_duration'].'" /></td>
					<td><input type="text" name="amount['.$row['sp_id'].']" value="'.$row['sp_amount'].'" /></td>
				</tr>				
			';
		}
		dbFreeResult($resultB);
		$existingHTML .= '</table>';
            	
    }
    	

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appSubscriptionTitle']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">
    <!-- 1/2 Columns -->
    <div class="twelve columns">

		
		
	<div class="headline no-margin">
	<h3>{$print[ $appSettings['appLanguage'] ]['appSubscriptionEditPackage']}</h3>
	</div>
	
	
      <div class="large-notice">
		<div id="statusContainer">	
		</div>
        <form method="post" autocomplete="off" onsubmit="return UPLOADER.submit(this, {'onStart' : function(){return requestPost()}, 'onComplete' : statusPost})" action="form.php?url=subscription">
		
            <div style="display:block;overflow:hidden">
            	<input type="hidden" name="update" value="update" />
                {$existingHTML}
    
            <div class="field" style="margin-top:20px">
                <input type="submit" id="sumbitbutton" value="{$print[ $appSettings['appLanguage'] ]['appUpdateButton']}"/>
            </div>            
		
        </div>
        
      </form>
       
       </div>
    </div>
    <div class="four columns">

	
	
    </div>
    <!-- 1/2 Columns End -->
  </div>

BODYCONTENT;
	

?>
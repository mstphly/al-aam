<?php
	
	$headMenu = '';
	$footerMenu = '';
	if( isset( $_SESSION['activated'] ) )
	{
	
		$profilePhoto = is_file( 'Profile/'.$_SESSION['info']['profileImg'] ) ? 
		'SLIR/w95-h95/allsmiles/Profile/'.$_SESSION['info']['profileImg'] :
		'Profile/Default-95.jpg';
		
		$portalMenu = '';
		# Role Link Seperate
		switch( $_SESSION['info']['roleType'] )
		{
			case 1:
				$portalMenu .=				
					'<ul id="nav">
						  <li><a href="#">Dashboard</a></li>
						  <li><a href="index.php?seek=viewusers">All Users</a><ul><li><a href="index.php?seek=viewusers">Users</a></li><li><a href="index.php?seek=viewpatients">Patient</a></li>
						  <li><a href="index.php?seek=addaccount&acctT=2">Add Doctor</a>
						  <li><a href="index.php?seek=addaccount&acctT=5">Add Receptionist</a>
						  <li><a href="index.php?seek=addaccount&acctT=3">Add Patient</a>						  
						  </li>
						  </ul></li>
						  <li><a href="index.php?seek=inbox">Messaging</a><ul><li><a href="index.php?seek=inbox">Inbox</a></li><li><a href="index.php?seek=newmsg">Create New</a></li></ul></li>
						  <li><a href="#">BackUp Data</a></li>
					</ul>';				
			break;
			case 2:
				// Doctors
				$portalMenu .=
					'<ul id="nav">
						  <li><a href="index.php?seek=inbox">Messaging</a><ul><li><a href="index.php?seek=inbox">Inbox</a></li><li><a href="index.php?seek=newmsg">Create New</a></li></ul></li>
						  <li><a href="#">Booking</a><ul><li><a href="index.php?seek=booking">Make New Booking</a></li><li><a href="index.php?seek=booking&type=patient">View Booking Schedule</a></li></ul></li>
						  <li><a href="index.php?seek=subscription">Subscription</a></li>
					</ul>';				
			break;
			case 3:
			case 4:
				// Patients or Users
				$portalMenu .=
					'<ul id="nav">
						  <li><a href="index.php?seek=inbox" class="loadinto-ajaxbodyContainer">Messaging</a><ul><li><a href="index.php?seek=inbox">Inbox</a></li><li><a href="index.php?seek=newmsg">Create New</a></li></ul></li>
						  <li><a href="index.php?seek=booking">Booking</a></li>
						  <li><a href="index.php?seek=subscription">Subscription</a></li>
					</ul>';
			break;
			case 5:
				// Receptionist
				$portalMenu .=
					'<ul id="nav">
						  <li><a href="index.php?seek=inbox">Messaging</a><ul><li><a href="index.php?seek=inbox">Inbox</a></li><li><a href="index.php?seek=newmsg">Create New</a></li></ul></li>
						  <li><a href="#">Booking</a><ul><li><a href="index.php?seek=booking">Make New Booking</a></li><li><a href="index.php?seek=bookinglist">Patient Scheduling</a></li></ul></li>
						  <li><a href="#">Report (Export)</a>
							  <ul>
								  <li><a href="index.php?seek=reportExcel&rt=2">All Female (excel)</a></li>
								  <li><a href="index.php?seek=reportExcel&rt=1">All Male (excel)</a></li>
								  <li><a href="#">All Teens (excel)</a></li>
								  <li><a href="#">All Adults (excel)</a></li>
								  <li><a href="#">View Subscriber List (excel)</a></li>
							  </ul>
						  </li>						  
						  <li><a href="#">Subscription</a>
							  <ul>
								<li><a href="index.php?seek=subscriptioneditpackage">Edit Subscription Package</a></li>							
							  </ul>						  
						  </li>
					</ul>';				
			break;
		}
		
		
		$headMenu = <<<MENU
	<div id="wrapper">	
	<div class="container ie-dropdown-fix">
    <!-- Header -->
    <div id="header">
      <!-- Logo -->
      <div class="eight columns">
        <div id="logo"> <a href="#"><img src="images/logo.png" alt="logo" /></a>
          <div class="clear"></div>
        </div>
      </div>
      <!-- Social / Contact -->
      <div class="eight columns">
<div style="float:right">
      <div class="userinfo">
                <span>{$_SESSION['info']['fullname']}</span>
            </div><!--userinfo-->
            
            <div class="userinfodrop">
            	<div class="avatar">
                	<a href="#"><img src="{$profilePhoto}" alt="" /></a>
                </div><!--avatar-->
                <div class="userdata">
                	<h4>Maryam Gumel Musa</h4>
                    <ul>
                    	<li><a href="index.php?seek=profile">View Profile</a></li>
						<li><a href="index.php?seek=editprofile">Edit Profile</a></li>
                        <li><a href="index.php?seek=signout">SignOut</a></li>
                    </ul>
                </div><!--userdata-->
            </div><!--userinfodrop-->
</div>		
					  <div style="float:right">
						<dl class="dropdown" id="sample">
								<dt><a href="#"><span>Language</span></a></dt>
								<dd>
									<ul style="display: none;">
										<li><a href="default.php?lang=en">English<span class="value">EN</span></a></li>
										<li><a href="default.php?lang=fr">French<span class="value">FR</span></a></li>
									</ul>
								</dd>
						</dl>
					  </div>	


        <div class="clear"></div>
        <!-- Contact Details -->
        <div id="contact-details">
          <ul>
            <li><i class="mini-ico-envelope"></i><a href="#">support@allsmilesdental.com</a></li>
            <li><i class="mini-ico-user"></i>4123 456
          </li></ul>
        </div>
      </div>
    </div>
    <!-- Header / End -->
	
    <!-- Navigation -->
    <div class="sixteen columns">
      <div id="navigation">
		{$portalMenu}
        <!-- Search Form -->
        <div class="search-form">
          <form method="get" action="#">
            <input type="text" class="search-text-box" />
          </form>
        </div>
      </div>
      <div class="clear"></div>
    </div>
    <!-- Navigation / End -->
	</div>
MENU;

	//$footerMenu = '</div>';

	} else
	{
			
			$headMenu .= '
			<div id="wrapper">	
				<div class="container ie-dropdown-fix">
				 <!-- Header -->
				  <div id="header">
					<!-- Logo -->
					<div class="eight columns">
					  <div id="logo"> <a href="#"><img src="images/logo.png" alt="logo" /></a>
						<div class="clear"></div>
					  </div>
					</div>
					<!-- Social / Contact -->
					<div class="eight columns">
					
					  <!-- Social Icons -->
					  
					  <div class="clear"></div>
					  <!-- Contact Details -->
					  <div id="contact-details">
						<ul>
						  <li><i class="mini-ico-envelope"></i><a href="#">info@al-aam.com</a></li>
						  <li><i class="mini-ico-user"></i>08033144116 </li>
						</ul>
					  </div>
					</div>
				  </div>
				  <!-- Header / End -->		
					<!-- Navigation -->
					<div class="sixteen columns">
					';
			
			
			$links = getDisplayLinks(1); // 1 Unique Id for Menu Pages

			$Hactive = isset( $_GET['pg'] ) ? '' : 'class="active"';
			
			$headMenu .= '<div id="navigation">
							<ul id="nav">';			
			$headMenu .= basename($_SERVER['SCRIPT_FILENAME']) == 'index.php' ? '<li id="on"><a href="default.php">Home</a></li>' : '<li><a href="default.php">Home</a></li>';
			$headMenu .= basename($_SERVER['SCRIPT_FILENAME']) == 'index.php' ? '<li id="on"><a href="aboutus.php">About Us</a></li>' : '<li><a href="aboutus.php">About Us</a></li>';			
			if( !empty($links) )
			{
				
				$c = 0;
				foreach( $links as $v )
				{
					$c++;
					$onMenu = '';
					$on = '';
					if( isset( $_GET['pg'] ) && $v['id'] == $_GET['pg'] )
					{
						$on = ' id="on"';
						$onMenu = '<span class="onMenu">&nbsp;</span>';
					}
					
					$headMenu .= '<li '.$on.'>';
							
							
							if( !empty($v['subLinks']) )
							{
								$headMenu .= '<a href="default.php?pg='.$v['id'].'">'.ucwords(strtolower($v['title'])).'</a>';

								$headMenu .= '<ul>';
									foreach( $v['subLinks'] as $sub )
									{
										if( !empty($sub['extraLinks']) )
										{
											$headMenu .= '<li>';
											$headMenu .= '<a href="default.php?pg='.$v['id'].'&spg='.$sub['id'].'" class="rarr">'.ucwords(strtolower($sub['title'])).'</a>';
											$headMenu .= '<ul>';
												foreach( $sub['extraLinks'] as $extras )
												{
													$headMenu .= '<li><a href="default.php?pg='.$extras['id'].'&ex=1">'.ucwords(strtolower($extras['title'])).'</a></li>';
												}
											$headMenu .= '</ul>';
											$headMenu .= '</li>';
										} else
										{
											$headMenu .= '<li><a href="default.php?pg='.$v['id'].'&spg='.$sub['id'].'">'.ucwords(strtolower($sub['title'])).'</a></li>';
										}
									}
								$headMenu .= '</ul>';

							} else
								$headMenu .= '<a href="default.php?pg='.$v['id'].'">'.$v['title'].'</a>';
							
					$headMenu .= '</li>';
				}
				
			}

			$headMenu .= '</ul>
						   <!-- Search Form -->
							<div class="search-form">
							  <form method="get" action="#">
								<input type="text" class="search-text-box" />
							  </form>
							</div>
						  </div>
						  <div class="clear"></div>
						</div>
						</div>';
						

	}
	
	
	$footerMenu .= <<<FOOTMENU
		<!-- Footer
		================================================== -->
		</div>
		<!-- Footer Start -->
		<div id="footer">
		  <!-- 960 Container -->
		  <div class="container">
			<!-- About -->
			<div class="four columns">
			  <div class="footer-headline">
				<h4>About Al-aam</h4>
			  </div>
			  <p>AL-AAM ENGINEERING SERVICES LIMITED is a Building and Civil Engineering Consulting and Consortium of professional Architects, Engineers, Quantity Surveyors, and Project Managers who are highly trained to provide excellent services in the construction industry.</p>
			</div>
			<!-- Useful Links -->
			<div class="four columns">
			  <div class="footer-headline">
				<h4>Our Services</h4>
			  </div>
			  <ul class="links-list">
				<li><a href="#">Computer Services</a></li>
				<li><a href="#">Project Management</a></li>
				<li><a href="#">Construction</a></li>
				<li><a href="#">Media and Printing</a></li>
			  </ul>
			</div>
			<!-- Photo Stream -->
			<div class="four columns">
			  <div class="footer-headline">
				<h4>Our Policy</h4>
			  </div>
			  <p>We are a multi disciplinary organization with highly qualified and experienced professionals, all specialists in their field and committed to achieving clients requirements.</p>
			</div>
			<!-- Latest Tweets -->
			<div class="four columns">
			  <div class="footer-headline">
			  </div>
			  <ul id="twitter">
			  </ul>
			  <script type="text/javascript">
							jQuery(document).ready(function($){
							$.getJSON('http://api.twitter.com/1/statuses/user_timeline/cnn.json?count=2&amp;callback=?', function(tweets){
							$("#twitter").html(tz_format_twitter(tweets));
							}); });
						</script>
			  <div class="clear"></div>
			</div>
			<!-- Footer / Bottom -->
			<div class="sixteen columns">
			  <div id="footer-bottom"> &copy; Copyright 2016 Al-aam Engineering Services LTD. All rights reserved. | <a href="sitemap.php">Site Map</a>
				<div id="scroll-top-top"><a href="#"></a></div>
			  </div>
			</div>
		  </div>
		  <!-- 960 Container / End -->
		</div>
		<!-- Footer / End -->		
FOOTMENU;

$pageProperties['headContent'] = $headMenu;
$pageProperties['footContent'] = $footerMenu;

?>
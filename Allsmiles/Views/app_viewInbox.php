<?php
	
	checkSessionStatus();
	require_once dirname(__FILE__).'/__menuTop.php';
	
	
	
	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/build/css/boxed.css" id="layout">
<link rel="stylesheet" type="text/css" href="assets/build/css/colors/green.css" id="colors">
<link rel="shortcut icon" href="favicon.ico" />	
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
FOOTJS;
		
	
	$listInTable = $print[ $appSettings['appLanguage'] ]['appMessageTableHead'];

	$result =& dbQuery('SELECT `msg_id`, `record_id`, `msg_title`, `msg_datetime`, `msg_status`, `lname`, `fname`, NOW() as `timenow` FROM `messaging` LEFT JOIN `record` USING (record_id) WHERE `recipient` = "'.$_SESSION['info']['email'].'" ORDER BY msg_datetime DESC');
	if( dbNumRows($result)>0 )
	{
		$no = 0;
		while( ($x =& dbFetchAssoc($result)) )
		{
			$bgStyle='';
			$style='';
			if( $x['msg_status'] == 'Unread' )
			{
				$bgStyle = ' style="background:#EEEEEE"';
				$style = ' style="font-weight:700!important"';
			} else
			{
				$bgStyle = ' style="background:#FFFFFF"';
			}
			
			$no++;
			$listInTable .= 
			'
				<tr '.$bgStyle.'>
					<td>'.$no.'</td>
					<td>'.ucwords($x['lname'].' '.$x['lname']).'</td>
					<td><a '.$style.' href="index.php?seek=viewInbox&msg='.$x['msg_id'].'">'.$x['msg_title'].'</a></td>
					<td>'.ago($x['timenow'], $x['msg_datetime']).'</td>
					<td>
						<a href="#" title="trash message"><i class="mini-ico-minus-sign"></i>Delete</a></td>
				</tr>
			';
		}
		dbFreeResult($result);
		$listInTable .= '</table>';
		
	} else
	{
		$listInTable = '<div class="notification notice closeable" style="display: block;">
							<p><span>Owwwkkayy</span> Empty Inbox (Thats teeth Clean)</p>
							<a href="#" class="close"></a>
						</div>';
	}
	

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appMessageTitle']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">

  
    <!-- 12 Columns -->
    <div class="twelve columns">

      	
			{$listInTable}
	  
    </div>
    <div class="four columns">

		<div class="headline no-margin">
			<h3>Tips On Messaging</h3>
		</div>
		<!-- Tip -->
		<div class="testimonials-carousel" data-autorotate="3000">
			<ul class="carousel">

				<li class="testimonial">
				<div class="testimonials">The Easiest way to communicate with Allsmiles Clinic is here. </div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>
				</li>

				<li class="testimonial">
				<div class="testimonials">The Easiest way to communicate with Allsmiles Clinic is here.</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>				
				</li>

			</ul>
		</div>	     

    </div>  
  
  
  </div>
  <!-- 960 Container End -->
  <!-- 960 Container -->
  <!-- 960 Container End -->

BODYCONTENT;
	

?>
<?php
	
	checkSessionStatus();
	require_once dirname(__FILE__).'/__menuTop.php';
	
	
	
	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/build/css/boxed.css" id="layout">
<link rel="stylesheet" type="text/css" href="assets/build/css/colors/green.css" id="colors">
<link rel="shortcut icon" href="favicon.ico" />
	<style type="text/css">
		.editorDetail,.editor, .editorDetail span, .editorDetail p{ display:block; overflow:hidden;}
		.editorDetail { border-bottom:1px solid #CCCCCC; margin-bottom:10px;}
		.editorDetail .spanA{ float:left; width: 100px;}
		.editorDetail .spanB{ float:left; }
		.editor{ padding:20px 10px 10px; color:#222;  padding:2px 0px;}
	</style>
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
HEADJS;

	$pageProperties['jsFoot'] = <<<FOOTJS
FOOTJS;
		
	$listInTable = '
		<table class="standard-table">
			<tr>
				<th width="2%">#</th>
				<th width="10%">Sender</th>
				<th width="68%">Subject</th>
				<th width="10%">Date</th>
				<th width="10%">Options</th>
			</tr>
	';
	
	
	$displayMessage = '';
	$msgId = $_GET['msg'];
	$result =& dbQuery('SELECT `msg_id`, `record_id`, `msg_title`, `msg_content`, `msg_datetime`, `msg_status`, `title`, `lname`, `fname` FROM `messaging` LEFT JOIN `record` USING (record_id) WHERE `msg_id` = '.$msgId);
	
	
	if( dbNumRows($result)>0 )
	{
		$data =& dbFetchAssoc($result);
		dbFreeResult($result);
		$displayMessage = '<div class="large-notice">
			<div class="editorDetail">
				<p>
					<span class="spanA">'.$print[ $appSettings['appLanguage'] ]['appMessageMFrom'].':</span><span class="spanB">'.ucwords($data['title'].'. '.$data['lname'].' '.$data['fname']).'</span>
				</p>
				<p>
					<span class="spanA">'.$print[ $appSettings['appLanguage'] ]['appMessageMdate'].':</span><span class="spanB">'.$data['msg_datetime'].'</span>
				</p>
			</div>
			<div class="editor">
				'.html_entity_decode(stripslashes($data['msg_content']), ENT_QUOTES).'
			</div>

		</div>';
		
		dbQuery('UPDATE `messaging` SET `msg_status` = "read" WHERE `msg_id` = '.$msgId);
	}
	

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appMessageTitle']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">

  
    <!-- 12 Columns -->
    <div class="twelve columns">

      	
			{$displayMessage}
	  
    </div>
    <div class="four columns">
     
		<div class="headline no-margin">
			<h3>Tips On Messaging</h3>
		</div>
		<!-- Tip -->
		<div class="testimonials-carousel" data-autorotate="3000">
			<ul class="carousel">

				<li class="testimonial">
				<div class="testimonials">The Easiest way to communicate with Allsmiles Clinic is here. </div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>
				</li>

				<li class="testimonial">
				<div class="testimonials">The Easiest way to communicate with Allsmiles Clinic is here.</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>				
				</li>

			</ul>
		</div>	
	  
	  
    </div>  
  
  
  </div>
  <!-- 960 Container End -->
  <!-- 960 Container -->
  <!-- 960 Container End -->

BODYCONTENT;
	

?>
<?php
	
	checkSessionStatus();
	require_once dirname(__FILE__).'/__menuTop.php';
	
	
	
	// LOGIN
	$pageProperties['bodyClass'] = 'login';
	$pageProperties['css'] = '
<link rel="stylesheet" type="text/css" href="assets/build/css/general.css" >
<link rel="stylesheet" type="text/css" href="assets/tinyeditor/tinyeditor.css" >
<link rel="stylesheet" type="text/css" href="Fashion/token-input.css">
<link rel="shortcut icon" href="favicon.ico" />
	<style type="text/css">
		.editorDetail,.editor,.editorDetail .block{ display:block; overflow:hidden;}
		.editorDetail .block{ margin-bottom:5px;}
		.editorDetail { }
		.editorDetail .spanA{ float:left; width: 100px; padding-top:15px;}
		.editorDetail .spanB{ float:left; }
		#to{border-color:#ffffff!important;}
		.editor{ margin-top:20px; padding:20px 10px 10px; line-height:25px; letter-spacing:.5px; word-spacing:1px; color:#222;  padding:2px 0px; font-size:14px; font-family:"Segoe UI", "Myriad Pro", Tahoma;}
	</style>
	';
	
	$pageProperties['jsHead'] = <<<HEADJS
		<script src="assets/build/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/build/js/combined.js"></script>
		<script src="assets/tinyeditor/tiny.editor.packed.js"></script>
		<script type="text/javascript" src="Behaviours/jquery.tokeninput.js"></script>		
HEADJS;

		
	

	$pageProperties['jsFoot'] = <<<FOOTJS
				
				<script type="text/javascript">
                
                	function delFromArr(arr, value)
                    {
                        for(i=0;i<arr.length;i++)
                        {
                            if(arr[i].id == value)
                            {
                                arr.splice(i,1);
                                break;
                            }
                        }                        
                    }
                	var toIds = [];
                    
                    $(function(){
                        $("#to").tokenInput("ajax.php?url=autocomplete",
                        {
                            onAdd: function (item) {
                                toIds.push(item.id);
                            },
                            onDelete: function (item)
                            {
                                delFromArr(toIds, item.id);
                            }, 
                            preventDuplicates: true
                        });
                    });
					document.getElementById('sumbitbutton').disabled = false;
					
					function requestPost()
					{	
                    	document.getElementById("msgTo").value=toIds;
						editor.post();                        
						return true;
					}

					function statusPost( response )
					{

						var container = xId("statusContainer");
						response = strip_tags(response);
						response = trim( response );

						switch( response.toString() ) 
						{
							case 'success':
							case '["success"]':
							container.innerHTML = '<div class="notification success closeable">Message Has being successfully sent</div>';				
							break;
							default:
							case 'failure':
							case '["failure"]':
							container.innerHTML = '<div class="notification error closeable"><p>Sending Message Failed</p><a class="close" href="#"></a></div>';
							break;				
						}

						scrollTo(0,0);

					}
				</script>		
FOOTJS;
		
	$listInTable = '
		<table class="standard-table">
			<tr>
				<th width="2%">#</th>
				<th width="10%">Sender</th>
				<th width="68%">Subject</th>
				<th width="10%">Date</th>
				<th width="10%">Options</th>
			</tr>
	';
	
	
	$msgToDisp = '';
    if( $_SESSION['info']['roleType'] != 3 )
    {
    


    }
	
	switch( $_SESSION['info']['roleType'] )
	{
		case 5: // receptionist
			$msgToDisp =<<<MSGTO

				<div class="block">
					<div class="spanA">{$print[ $appSettings['appLanguage'] ]['appMessageFormMtype']}:
					</div>
					<div class="spanB">
						<select name="msgType">
							<option value="1">System Inbox and Email</option>
							<option value="2">NewsLetter</option>
							<option value="3">Text Message (SMS)</option>
						</select>
					</div>					
				</div>
				
				<div class="block">
					<div class="spanA">{$print[ $appSettings['appLanguage'] ]['appMessageFormMto']}:</div>
					<div class="spanB"><input type="text" name="to" id="to" /></div>
				</div>
MSGTO;
		break;
		case 3:
		break;
		default:
			$msgToDisp =<<<MSGTO
			<div class="block">
				<div class="spanA">{$print[ $appSettings['appLanguage'] ]['appMessageFormMto']}:</div>
				<div class="spanB"><input type="text" name="to" id="to" /></div>
			</div>
MSGTO;
		break;
	}
	
    
	$newMessage = <<<NEW
		<div class="large-notice">
		
			<div id="statusContainer"></div>
			<form method="post" autocomplete="off" onsubmit="return UPLOADER.submit(this, {'onStart' : function(){return requestPost()}, 'onComplete' : statusPost})" action="form.php?url=newmsg">
			
			<div class="editorDetail">
				<input type="hidden" name="uid" value="{$_SESSION['activated']}" />
				{$msgToDisp}
				<div class="block">
				<div class="spanA">{$print[ $appSettings['appLanguage'] ]['appMessageFormMsubject']}:</div><div class="spanB"><input type="text" name="subject" style="width: 510px" /></div>
				</div>
                <input type="hidden" name="msgTo" id="msgTo" value="" />				
			</div>
			
			<div class="editor">
				<textarea id="tinyeditor" name="content" style="width: 700px; height: 200px"></textarea>
				<script type="text/javascript">
				var editor = new TINY.editor.edit('editor', {
				id: 'tinyeditor',
				width: 625,
				height: 175,
				cssclass: 'tinyeditor',
				controlclass: 'tinyeditor-control',
				rowclass: 'tinyeditor-header',
				dividerclass: 'tinyeditor-divider',
				controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
				'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
				'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
				'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
				footer: true,
				fonts: ['Verdana','Arial','Georgia','Trebuchet MS'],
				xhtml: true,
				cssfile: '',
				bodyid: 'editor',
				footerclass: 'tinyeditor-footer',
				toggle: {text: 'source', activetext: 'wysiwyg', cssclass: 'toggle'},
				resize: {cssclass: 'resize'}
				});
				</script>
				
				<div class="field" style="margin-top:20px">
				<input type="submit" id="sumbitbutton" value="{$print[ $appSettings['appLanguage'] ]['appMessageSendTitle']}"/>
				<div class="loading"></div>
				</div>				
			</div>
			
			</form>
		
		</div>
NEW;
	

	$pageProperties['bodyContent'] = <<<BODYCONTENT
	

  <!-- 960 Container -->
  <div class="container">
    <div class="sixteen columns">
      <!-- Page Title -->
      <div id="page-title">
        <h2>{$print[ $appSettings['appLanguage'] ]['appMessageTitle']}</h2>
        <div id="bolded-line"></div>
      </div>
      <!-- Page Title / End -->
    </div>
  </div>
  <!-- 960 Container / End -->
  <!-- 960 Container --><!-- 960 Container End -->
  <!-- 960 Container -->
  <div class="container">

  
    <!-- 12 Columns -->
    <div class="twelve columns">

      	
			{$newMessage}
	  
    </div>

	
<div class="four columns">


		<div class="headline no-margin">
			<h3>Tips On Messaging</h3>
		</div>
		<!-- Tip -->
		<div class="testimonials-carousel" data-autorotate="3000">
			<ul class="carousel">

				<li class="testimonial">
				<div class="testimonials">The Easiest way to communicate with Allsmiles Clinic is here. </div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>
				</li>

				<li class="testimonial">
				<div class="testimonials">The Easiest way to communicate with Allsmiles Clinic is here.</div>
				<div class="testimonials-bg"></div>
				<div class="testimonials-author">Eagles Technologies, <span>Support Team</span></div>				
				</li>

			</ul>
		</div>	

</div>

  
  
	
    </div>
    <!-- 1/2 Columns End -->
  </div>
BODYCONTENT;
	

?>
<?php

	/*

	Copyright (c) 2009 Anant Garg (anantgarg.com | inscripts.com)

	This script may be used for non-commercial purposes only. For any
	commercial purposes, please contact the author at 
	anant.garg@inscripts.com

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.

	*/

	
	activeUsersFilters();
	if( isset($_GET['action']) )
	{
		switch( $_GET['action'] )
		{
			case 'chatheartbeat':
				chatHeartbeat();
			break;
			case 'sendchat':
				sendChat();
			break;
			case 'closechat':
				closeChat();
			break;
			case 'startchatsession':
				startChatSession();
			break;			
		}
	}


	if( !isset($_SESSION['chatHistory']) ) 
	{
		$_SESSION['chatHistory'] = array();
	}

	if( !isset($_SESSION['openChatBoxes']) )
	{
		$_SESSION['openChatBoxes'] = array();
	}
	

	function activeUsersFilters()
	{
		
		$result =& dbQuery('REPLACE INTO `record_status` (`record_id`, `record_status`, `record_seen_date`) VALUES ('.$_SESSION['activated'].', "Active", NOW()) ');
	
		if( isset($_SESSION['LastcheckTime']) && ( strtotime($_SESSION['LastcheckTime']) - $_SERVER['REQUEST_TIME'] ) > 280 )
		{
			
			$list =& dbQuery('DELETE FROM `record_status` WHERE MINUTE( TIMEDIFF(NOW(), `record_seen_date`) ) > 3');
			$_SESSION['LastCheckTime'] = $_SERVER['REQUEST_TIME'];
		
		}
		
	}
	
	
	function chatHeartbeat()
	{
		
			// Load Online List Here
			$tuData = '';
			$data = '';
			switch( $_SESSION['info']['roleType'] )
			{
				case '3':
				case '4':
				
					// They can see online the receptionist or doctors that they are currently active with
					$result =& dbQuery('SELECT `username`, `mname`, `lname`, `fname` FROM `record` WHERE `record_id` IN ( SELECT `record_id` FROM `record_status` WHERE `record_id` != '.$_SESSION['activated'].' ) && `role_type` IN (5)');

					if( dbNumRows($result)>0 )
					{
						while( ($rows =& dbFetchAssoc($result)) )
						{
							$tuData .= ';'.$rows['username'].'|'.$rows['lname'].' '.$rows['fname'];
						}
						dbFreeResult($result);
					}					
					
				break;
				default:
					
					// Every Body Here can see everybody except patience
					$result =& dbQuery('SELECT `username`, `mname`, `lname`, `fname` FROM `record` WHERE `record_id` IN ( SELECT `record_id` FROM `record_status` WHERE `record_id` != '.$_SESSION['activated'].' ) && `role_type` NOT IN (3,4)');

					if( dbNumRows($result)>0 )
					{
						while( ($rows =& dbFetchAssoc($result)) )
						{
							$tuData .= ';'.$rows['username'].'|'.$rows['lname'].' '.$rows['fname'];
						}
						dbFreeResult($result);
					}					
				break;
			}

			
			if( !empty($tuData) )
			{
			$data = <<<EOD
				   {
						"status" : true,
						"s": "0",
						"f": "Online",
						"m": "{$tuData}"
				   },
EOD;
			}			
		
		
		// Load Online List Here		
		
		$result =& dbQuery('SELECT * FROM `chat` WHERE `to` = "'.$_SESSION['info']['username'].'" AND recd = 0 order by id ASC');
	

		$items = '';	
		$chatBoxes = array();
		
		// Friendship-Connection
		$items .= $data;
		

		if( dbNumRows($result)>0 )
		{
			
			while( ($chat =& dbFetchAssoc($result)) )
			{

				if( !isset($_SESSION['openChatBoxes'][$chat['from']]) && 
					isset($_SESSION['chatHistory'][$chat['from']]) ) 
				{
					$items .= $_SESSION['chatHistory'][$chat['from']];
				}
				
				$chat['message'] = sanitize($chat['message']);				

				$items .= <<<EOD
				   {
						"s": "0",
						"f": "{$chat['from']}",
						"m": "{$chat['message']}"
				   },
EOD;

				if( !isset($_SESSION['chatHistory'][$chat['from']]) )
				{
					$_SESSION['chatHistory'][$chat['from']] = '';
				}
				
				$_SESSION['chatHistory'][$chat['from']] .= <<<EOD
					{
						"s": "0",
						"f": "{$chat['from']}",
						"m": "{$chat['message']}"
					},
EOD;
				unset($_SESSION['tsChatBoxes'][$chat['from']]);
				$_SESSION['openChatBoxes'][$chat['from']] = $chat['sent'];
				
			}
			dbFreeResult( $result );
			
		}
		
		
		if (!empty($_SESSION['openChatBoxes']))
		{
		
			foreach( $_SESSION['openChatBoxes'] as $chatbox => $time )
			{
				if( !isset($_SESSION['tsChatBoxes'][$chatbox]) )
				{
					$now  = $_SERVER['REQUEST_TIME'] - strtotime($time);
					$time = date('g:iA M dS', strtotime($time));
					
					$message = "Sent at $time";
					if($now > 180)
					{
						$items .= <<<EOD
							{
							"s": "2",
							"f": "$chatbox",
							"m": "{$message}"
							},
EOD;
						
						if( !isset($_SESSION['chatHistory'][$chatbox]) )
						{
							$_SESSION['chatHistory'][$chatbox] = '';
						}
						
						$_SESSION['chatHistory'][$chatbox] .= <<<EOD
							{
							"s": "2",
							"f": "$chatbox",
							"m": "{$message}"
							},
EOD;
						$_SESSION['tsChatBoxes'][$chatbox] = 1;
					}
					
					
				}
				
			}
			
		}
		
		
		$result =& dbQuery('UPDATE `chat` SET `recd` = 1 WHERE `to` = "'.$_SESSION['info']['username'].'" && `recd` = 0');
		
		if( $items != '' )
		{
			$items = substr($items, 0, -1);
		}
		header('Content-type: application/json');
		
		echo <<<JSONRETURN
				{
					"items": [{$items}]
				}			
JSONRETURN;
		exit;
		
		

	}
	
	

	function chatBoxSession($chatbox)
	{
		
		$items = '';
		
		if(	isset($_SESSION['chatHistory'][$chatbox]) )
		{
			$items = $_SESSION['chatHistory'][$chatbox];
		}
		
		return $items;
		
	}

	
	
	function startChatSession()
	{
	
		$items = '';
		if( !empty($_SESSION['openChatBoxes']) )
		{
			foreach( $_SESSION['openChatBoxes'] as $chatbox => $void )
			{
				$items .= chatBoxSession($chatbox);
			}
		}
		
		
		if($items != '')
		{
			$items = substr($items, 0, -1);
		}
		
		header('Content-type: application/json');
		echo <<<JSONRETURN
				{
					"username": "{$_SESSION['info']['username']}",
					"items": [{$items}]
				}		
JSONRETURN;
		
		exit(0);
	
	}

	
	
	function sendChat()
	{

		$from    = $_SESSION['info']['username'];
		$to      = $_POST['to'];
		$message = $_POST['message'];
		
		$_SESSION['openChatBoxes'][$_POST['to']] = date('Y-m-d H:i:s', time());
		
		$messagesan = sanitize($message);
		
		if( !isset($_SESSION['chatHistory'][$_POST['to']]) )
		{
			$_SESSION['chatHistory'][$_POST['to']] = '';
		}
		
		$_SESSION['chatHistory'][$_POST['to']] .= <<<EOD
		   {
				"s": "1",
				"f": "{$to}",
				"m": "{$messagesan}"
		   },
EOD;
			
		unset($_SESSION['tsChatBoxes'][$_POST['to']]);
		
		
		dbQuery('INSERT INTO `chat` (`from`,`to`,`message`,`sent`) VALUES (
			"'.$from.'",
			"'.$to.'",
			"'.$message.'",
			NOW()
		)');
		
		echo '1';
		exit;
	
	}
	

	function closeChat()
	{
		
		unset($_SESSION['openChatBoxes'][$_POST['chatbox']]);	
		echo '1';
		exit;
		
	}

	function sanitize($text)
	{
		$text = htmlspecialchars($text, ENT_QUOTES);
		$text = str_replace("\n\r", "\n", $text);
		$text = str_replace("\r\n", "\n", $text);
		$text = str_replace("\n", "<br>", $text);
		return $text;
	}
<?php
	
	/* Hard-Coded */
	require_once dirname(__FILE__).'/config.php';
	require_once 'cms.driver/file.driver.php';
	
	$Conn = DB::connect();
	

	function shortener($v, $to=10){ $h = ''; $t = $to; if( strlen($v) > $t ){ $h = substr($v,0,$t).'...'; return $h; } else { return $v; } }		
	function getLinks( $type=false )
	{
		
		$return = array();
		
		$where = ( $type != false ) ? array('pg_type'=>$type, 'pg_publish'=>"YES") : array('pg_publish'=>"YES");	
		$result = DB::load( $GLOBALS['Conn'], TABLE, '*', $where );
				
		if( !empty($result) )
		{
			foreach( $result as $v )
			{
				$return[] = array(
					'id' => $v['pg_id'],
					'title' => $v['pg_title'],
					'linkname' => $v['pg_title'],
					'publish' => $v['pg_publish'],
					'url' => $v['pg_url']
				);
			}
		}
		return $return;
		
	}
	
	
	function setLang( $lang='' )
	{
		$_SESSION['__lang'] = $lang;
	}
	
	
	function getSubLinks()
	{

		$return = array();
		$result = DB::load( $GLOBALS['Conn'], TABLE2, '*', array('spg_publish'=>"YES") );
		if( !empty($result) )
		{
			foreach( $result as $v )
			{
				$return[] = array(
					'id' => $v['spg_id'],
					'pid' => $v['pg_id'],
					'title' => $v['spg_title'],
					'linkname' => $v['spg_title'],
					'publish' => $v['spg_publish'],
					'url' => $v['spg_url']
				);
			}
		}
		return $return;
		
	}
	
	
	
	
	function getExtraLinks()
	{

		$return = array();
		$result = DB::load( $GLOBALS['Conn'], TABLE3, '*', array('mp_publish'=>"YES") );
		if( !empty($result) )
		{
			foreach( $result as $v )
			{
				$return[] = array(
					'id' => $v['mp_id'],
					'pid' => $v['p_id'],
					'title' => $v['mp_title'],
					'linkname' => $v['mp_title'],
					'publish' => $v['mp_publish'],
					'url' => $v['mp_url']
				);
			}
		}
		return $return;
		
	}	
	
	
	
	
	function getDisplayLinks( $type=false )
	{

		$links = getLinks($type);
		$subLinks = getSubLinks();
		$extraLinks = getExtraLinks();
		
		$subReturn = array();
		$extraReturn = array();
		$return = array();
		$availableKeys = array();		
		$availableKeys2 = array();
		
		
		if( !empty($extraLinks) )
		{
			foreach( $extraLinks as $subItem )
			{
				$availableKeys2[ $subItem['pid'] ] = $subItem['pid'];
				$extraReturn[ $subItem['pid'] ][] = array(
														'id' => $subItem['id'],
														'pid' => $subItem['pid'],
														'title' => $subItem['title'],
														'linkname' => $subItem['linkname'],
														'publish' => $subItem['publish'],
														'url' => $subItem['url']
														);
			}
		}
		
		

		if( !empty($subLinks) )
		{
			foreach( $subLinks as $subItem )
			{
				
				$extraList = array();
				$stat = array_search($subItem['id'], $availableKeys2);

				if( false !== $stat )
				{
					$extraList = $extraReturn[ $stat ];
				}
				
				$availableKeys[ $subItem['pid'] ] = $subItem['pid'];
				$subReturn[ $subItem['pid'] ][] = array(
														'id' => $subItem['id'],
														'pid' => $subItem['pid'],
														'title' => $subItem['title'],
														'linkname' => $subItem['linkname'],
														'publish' => $subItem['publish'],
														'url' => $subItem['url'],
														'extraLinks' => $extraList
														);
			}
		}
		
		
		
		if( !empty($links) )
		{

			foreach( $links as $item )
			{
				$subList = array();
				$stat = array_search($item['id'], $availableKeys);

				if( false !== $stat )
				{
					$subList = $subReturn[ $stat ];
				}
				$return[] = array(
								'id' => $item['id'],
								'title' => $item['title'],
								'linkname' => $item['title'],
								'publish' => $item['publish'],
								'url' => $item['url'],
								'subLinks' => $subList
								);
			}
		}
		unset($availableKeys, $availableKeys2, $extraLinks, $subLinks, $links);
		return $return;
		
	}
	
	
	
	
	function getDisplayContent( $pid, $spid = false, $sect=false, $extras=false )
	{
	
		$table = '';
		if( false !== $extras )
		{
			$where = array( 'pg_id'=>$pid, 'pg_type'=>'extra', 's_publish'=>'YES' );
		} else
		{
			$where = false !== $spid ? array( 'pg_id'=>$spid, 'pg_type'=>'sub', 's_publish'=>'YES' ) : array( 'pg_id'=>$pid, 's_publish'=>'YES', 'pg_type'=>'' );
		}
		
		
		if( false !== $sect )
			$where['cid'] = $sect;
		
		$result = DB::load( $GLOBALS['Conn'], STORE, '*', $where );
		
		if( !empty($result) )
		{
			$return = array();
			foreach( $result as $v )
			{	
				$return[] = array(
					'id' => $v['cid'],
					'title' => $v['s_title'],
					'content' => $v['s_content'],
					'media' => $v['media']
				);
			}
			return $return;
		}
		else
			return array();
	
	}
	
	
	

	function displaySlideShow( $showNumber=true )
	{
		
		$movement = '';
		$result = DB::load( $GLOBALS['Conn'], 'slideshow', '*', array('is_publish'=>'Yes') );
		if( !empty($result) )
		{
			$movement = $result[0]['movement'];
			$c = 0;
			$List[0] = '<ul>';
			$List[1] = '<ul id="pagination" class="pagination">';
			foreach( $result as $v )
			{
				$c++;
				$txt = true === $showNumber ? $c : '&nbsp;';
				$List[0] .= '<li><img src="CMS/media/slideshow/'.$v['img_path'].'" /></li>';
				$List[1] .= '<li onclick="slideshow.pos('.$c.')">'.$txt.'</li>'; 
			}
			$List[0] .= '</ul>';
			$List[1] .= '</ul>';
			echo '<div id="wrapper">
					<div>
						<div id="slider">
							'.$List[0].'
						</div>
					</div>
					'.$List[1].'
				</div>';
		}
		$movement = $movement == 'Side By Side' ? 'false' : 'true';
		return "var slideshow=new TINY.slider.slide('slideshow',{
				id:'slider',
				auto:5,
				resume:true,
				vertical:{$movement},
				navid:'pagination',
				activeclass:'current',
				position:1
			});";
		
	}	
	

?>
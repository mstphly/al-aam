<?php
	require_once dirname(__FILE__).'/config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
</head>
<body>
<?php
	require_once("cms_basic/head.php");

	
	switch( DBTYPE ) {
		case 'MYSQL':
			//require_once 'cms.driver/mysql.driver.php';
		break;
		case 'SQLITE':
			//require_once 'cms.driver/sqlite.driver.php';
		break;
		case 'FILE':
			require_once 'cms.driver/file.driver.php';
		break;
	}
	
	$run = DB::connect();
	$result = DB::load( $run, TABLE2, array('spg_id', 'spg_name') );
	
?>
<div class="block center content" >
<p class="contentheader">Add New Extra or General Page to Sub-Page</p>

<form method="post" action="cms_function/new_mispage.php">
<div class="block cnt">

<ul class="block opadd" style="width:450px; margin:20px auto 10px;">
<li><label for="un">Existing Sub-Page</label><select name="sname" class="iselect rw B">
<?php
	foreach( $result as $v )
	{
		if( isset($_GET['go']) && $_GET['go'] == $v['spg_id'] )
		{
			echo '<option value="',$v['spg_id'],'" selected="selected">',$v['spg_name'],'</option>';
		}else
		{
			echo '<option value="',$v['spg_id'],'">',$v['spg_name'],'</option>';
		}
	}
?>
</select></li>
<li><label for="un">Page Name</label><input class="itext rw B" type="text" name="name" /></li>
<li><label for="pwd">Page Title</label><input class="itext rw" type="text" name="title"  /></li>
<li><label for="pwd">Page Url</label><input class="itext rw" type="text" name="url"  /></li>
<li><label for="pwd">Publish Page</label><p style="padding-top:7px; font-size:11px;">Yes <input type="radio" name="publish" value="YES" /> No <input type="radio" name="publish" value="NO" /></p></li>
</ul>




</div>

<div class="block bottomplace">
<input type="submit" class="right ibutton" value="Create" />
<input type="reset" class="right ibutton" value="Cancel" />
</div>
</form>
</div>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
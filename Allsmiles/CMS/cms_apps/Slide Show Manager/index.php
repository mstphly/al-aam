<?php
	require_once '../../config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Slide-Show Manager | CMS Manager</title>
<link type="text/css" rel="stylesheet" href="../../cms_css/style.css" />
<style type="text/css">
.inputs INPUT { width: 300px; height: 25px; background: url("../../cms_img/HSV2.png") no-repeat scroll 16px center rgb(233, 242, 241); border: 1px solid rgb(216, 234, 239); border-radius: 5px 5px 5px 5px; float: left; line-height: 25px; font-size: 13px; padding: 0pt 10px 0pt 40px; }
INPUT[type="file"] { background: url("../../cms_img/HSV2.png") no-repeat scroll left center rgb(233, 242, 241); height: 71px; }
DIV.fileinputs { position: relative; width:350px;}
INPUT.file { width: 350px; height: 25px; background: url("../../cms_img/HSV2.png") no-repeat scroll 16px center rgb(233, 242, 241); border: 1px solid rgb(216, 234, 239); border-radius: 5px 5px 5px 5px; float: left; line-height: 25px; font-size: 13px; padding: 0pt 10px 0pt 40px; }
DIV.fakefile { margin-top: -1px; position: absolute;  z-index: 0;}
INPUT.file { position: relative; text-align: right; opacity: 0; z-index: 200; }

table{ margin:5px auto;table-layout:fixed; font-size:12px; border:1px solid #eee; padding:0;}
table thead{
background: #eee;
background: -moz-linear-gradient(top, #FCFCFC 0%, #F2F2F2 32%, #E8E8E8 60%, #E3E8ED 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#FCFCFC), color-stop(32%,#F2F2F2), color-stop(60%,#E8E8E8), color-stop(100%,#E3E8ED));
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FCFCFC', endColorstr='#E3E8ED',GradientType=0 )
 }
table thead tr{  border:1px solid red; }
table thead tr th{padding:5px 0;}
table thead tr th span{ border-left:1px solid #ccc; padding:0 10px; float:left;}
table tbody td{  border-top:1px solid #f6f6f6; margin:0; padding:5px 10px 4px;}
.cm{ vertical-align:middle; background:#f9f9f9}
.cm{  text-align:center; padding-left:8px;}
table tbody tr:hover{ background:#F4F7FA}
table tbody tr.selected { background:#E8EDF4;}
</style>
</head>
<body>
<?php

	require_once("../../cms_basic/head.php");
	require_once("../../cms.driver/file.driver.php");

	$run = DB::connect();
	
	
	if( isset($_GET['sid'][0]) )
	{
		DB::delete( $run, 'slideshow', array('sid'=>$_GET['sid']), 1);
	}
	
	if( isset($_POST['submit'], $_FILES['media']) )
	{
		$imgPath = '../../media/slideshow/';
		if( !is_dir($imgPath) )
			mkdir($imgPath, 0777);

		if( move_uploaded_file( $_FILES['media']['tmp_name'], $imgPath.'/'.$_FILES['media']['name'] ) )
		{
			$checked = isset( $_POST['publish'] ) ? 'Yes' : 'No'; 
			$affectRows = DB::insert( $run, 'slideshow', array('img_path', 'movement', 'is_publish'), array( $_FILES['media']['name'], $_POST['movement'],  $checked) );
		}

	}
	
	$result = DB::load( $run, 'slideshow', '*', array('is_publish'=>'Yes') );

?>

<div class="block center content" >
<p class="contentheader">SlideShow Manager</p>

<?php

	echo '<form method="post" action="" enctype="multipart/form-data">';
	
	if( !empty($result) )
	{

		echo '<div class="block cnt">';
		
		echo '<table width="98%" cellspacing="0" cellpadding="0" id="striped2" class="pickme"><thead><tr>',
				'<th width="3%">&nbsp;</th>',
				'<th width="47%"><span>Image Name</span></th>',
				'<th width="10%"><span>Movement</span></th>',
				'<th width="10%"><span>Published</span></th>',
				'<th width="30%"><span>Options</span></th>',
			 '</tr></thead><tbody>';
		foreach( $result as $v ) {
			echo '<tr>',
				 	'<td class="cm"><input type="checkbox" name="seek[]" value="',$v['sid'],'"  /></td>',
					'<td>',$v['img_path'],'</td>',
					'<td>',$v['movement'],'</td>',
					'<td>',$v['is_publish'],'</td>',
					'<td><a href="./?sid=',$v['sid'],'">Delete Image</a></td>',
				 '</tr>';
		}
		
		echo '</tbody></table>';
        echo '</div>';
		
	
	}
	
	echo <<<A
<div class="block cnt">
<ul class="block opadd" style="width:850px; margin:20px auto 0;">
<li><label for="pwd">Add Media:</label>
<div class="left block">
<div class="fileinputs">
    <div class="inputs">
        <input class="file" name="media" id="file" onchange="file2.value = this.value" type="file" />
    <div class="fakefile">
        <input id="file2" value="Click to Browse ..." type="text" />
    </div>
    </div>
</div>
</div>

</li>
<li>
	<label for="pwd">Movement:</label><select name="movement" class="iselect rw B"><option value="">Slide Movement</option><option>Side By Side</option><option>Up and Down</option></select>
</li>
<li>
	<label for="pwd">Publish Slide:</label><p style="padding:10px 0"><input type="checkbox" name="publish" /></p>
	
</li>
</ul>

A;
	
?>

</div>


<div class="block bottomplace">
<input type="submit" name="submit" class="right ibutton" value="Add To Slide Show" />
<input type="button" class="right ibutton" value="Delete"  onclick="window.location.href='add.category_poll.php'"  />
</div>
</form>
</div>

<?php require_once '../../cms_basic/foot.php'; ?>
<script type="text/javascript">
function $I(id) {
	return document.getElementById(id);
}

function tnxG(){
	var state = $I('spoon');
	var toggle = state.style.display;
	state.style.display = toggle == 'block' ? 'none' : 'block';
}
</script>
</body>
</html>
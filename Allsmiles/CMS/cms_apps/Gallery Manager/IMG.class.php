<?php

#Resize image class
class Resize
{
	
	private $file_source, 
			$width_resize,
			$height_resize,
			$proportional,
			$upload_path,
			$img_type,
			$n_img_type = false;

	
	public function __construct($file_source, $width_resize, $height_resize, $proportional=false, $upload_path='', $img_name='')
	{
		$this->file_source = $file_source;
		$this->width_resize = $width_resize;
		$this->height_resize = $height_resize;			
		$this->proportional = $proportional;
		$this->upload_path = $upload_path.'/';
		$this->image_name = $img_name;
	}
	
	public function setUploadpath( $upload_path ){
		$this->upload_path = $path;
	}
	
	public function setProportional($proportional)
	{
		$this->proportional = $proportional;
	}
	
	public function setFileSource($file_source)
	{
		$this->file_source = $file_source;
	}
	
	public function setHeightResize($height_resize)
	{
		$this->height_resize = $height_resize;
	}
	
	public function setWidthResize($width_resize)
	{
		$this->width_resize = $width_resize;
	}
	
	public function setNewImageType($type){
		$list = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
		if( in_array($type, $list) ) {
			$this->n_img_type = $type;
		}
	}
	
	public function img_type(){
		return $this->img_type;
	}
	
	private function MemoryUsage()
	{
		$imageInfo = getimagesize($this->file_source);
		$imageInfo['channels'] = !isset($imageInfo['channels']) ? 4 : $imageInfo['channels'];
		$memoryNeeded = round(($imageInfo[0] * $imageInfo[1] * $imageInfo['bits'] * $imageInfo['channels'] / 8 + Pow(2, 16)) * 1.65);
		  
		$memoryLimit = (int) ini_get('memory_limit')*1048576;
		if ((memory_get_usage() + $memoryNeeded) > $memoryLimit)
		ini_set('memory_limit', ceil((memory_get_usage() + $memoryNeeded + $memoryLimit)/1048576).'M');
	}
				
	public function ImageResize()
	{
		
		$this->MemoryUsage();
		if ( $this->height_resize <= 0 && $this->width_resize <= 0 ) return false;
   
		$info = getimagesize($this->file_source);
		$image = '';
		  
		$final_width = 0;
		$final_height = 0;
		list($width_old, $height_old) = $info;

		  
	  	$this->n_img_type = !$this->n_img_type ? $info[2] : $this->n_img_type;
		switch ( $this->n_img_type ) 
		{
			case IMAGETYPE_GIF:
				$this->img_type = 'gif';
				$image = imagecreatefromgif($this->file_source);
			break;
			case IMAGETYPE_JPEG:
				$this->img_type = 'jpg';
				$image = imagecreatefromjpeg($this->file_source);
			break;
			case IMAGETYPE_PNG:
				$this->img_type = 'png';
				$image = imagecreatefrompng($this->file_source);
			break;
			default:
				return false;
			break;
		}
		
				  
		if ($this->proportional) 
		{		          

			$d['width'] = imagesx($image);
			$d['height'] = imagesy($image);
			
			
			$xscale = $d['width']/$this->width_resize;
			$yscale = $d['height']/$this->height_resize;
			
			if( $d['width'] <= $this->width_resize && $d['height'] <= $this->height_resize ) {
			
				$final_width = $d['width'];
				$final_height = $d['height'];
				
			}
			else {
				if($yscale > $xscale) {
		
					$final_width = round( $d['width'] * (1/$yscale) );
					$final_height = round( $d['height'] * (1/$yscale) );
		
				}
				else {
		
					$final_width = round( $d['width'] * (1/$xscale) );
					$final_height = round( $d['height'] * (1/$xscale) );
		
				}
			}
			
		}
		else 
		{
			$final_width = ($this->width_resize <= 0) ? $this->width_resize_old : $this->width_resize;
			$final_height = ($this->height_resize <= 0) ? $this->height_resize_old : $this->height_resize;
		}

		
		$image_resized = imagecreatetruecolor( $final_width, $final_height );
		switch( $this->n_img_type ) {
			case IMAGETYPE_JPEG:
			
				imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
				
			break;
			case IMAGETYPE_GIF:		
			case IMAGETYPE_PNG:
			
				imagealphablending($image_resized, false);
				imagesavealpha($image_resized, true);		
				$transparent = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);			  
				imagefilledrectangle($image_resized, 0, 0, $final_width, $final_height, $transparent);
				imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
			break;
		}
				
		$save_path = $this->upload_path.$this->image_name;
		
		switch ( $this->n_img_type ) 
		{
			case IMAGETYPE_GIF:					
				imagegif($image_resized, $save_path.'.gif', 100);
			break;
			case IMAGETYPE_JPEG:
				imagejpeg($image_resized, $save_path.'.jpg', 100);
			break;
			case IMAGETYPE_PNG:
				imagepng($image_resized, $save_path.'.png', 9);
			break;
			default:
				return false;
			break;
		}		      
		return true;	
	}
}

?>
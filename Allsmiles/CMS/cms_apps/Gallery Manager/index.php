<?php
	require_once '../../config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gallery Manager | CMS Manager</title>
<link type="text/css" rel="stylesheet" href="../../cms_css/style.css" />
<style type="text/css">
.inputs INPUT { width: 300px; height: 25px; background: url("../../cms_img/HSV2.png") no-repeat scroll 16px center rgb(233, 242, 241); border: 1px solid rgb(216, 234, 239); border-radius: 5px 5px 5px 5px; float: left; line-height: 25px; font-size: 13px; padding: 0pt 10px 0pt 40px; }
INPUT[type="file"] { background: url("../../cms_img/HSV2.png") no-repeat scroll left center rgb(233, 242, 241); height: 71px; }
DIV.fileinputs { position: relative; width:350px;}
INPUT.file { width: 350px; height: 25px; background: url("../../cms_img/HSV2.png") no-repeat scroll 16px center rgb(233, 242, 241); border: 1px solid rgb(216, 234, 239); border-radius: 5px 5px 5px 5px; float: left; line-height: 25px; font-size: 13px; padding: 0pt 10px 0pt 40px; }
DIV.fakefile { margin-top: -1px; position: absolute;  z-index: 0;}
INPUT.file { position: relative; text-align: right; opacity: 0; z-index: 200; }

table{ margin:5px auto;table-layout:fixed; font-size:12px; border:1px solid #eee; padding:0;}
table thead{
background: #eee;
background: -moz-linear-gradient(top, #FCFCFC 0%, #F2F2F2 32%, #E8E8E8 60%, #E3E8ED 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#FCFCFC), color-stop(32%,#F2F2F2), color-stop(60%,#E8E8E8), color-stop(100%,#E3E8ED));
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FCFCFC', endColorstr='#E3E8ED',GradientType=0 )
 }
table thead tr{  border:1px solid red; }
table thead tr th{padding:5px 0;}
table thead tr th span{ border-left:1px solid #ccc; padding:0 10px; float:left;}
table tbody td{  border-top:1px solid #f6f6f6; margin:0; padding:5px 10px 4px;}
.cm{ vertical-align:middle; background:#f9f9f9}
.cm{  text-align:center; padding-left:8px;}
table tbody tr:hover{ background:#F4F7FA}
table tbody tr.selected { background:#E8EDF4;}
</style>
</head>
<body>
<?php

	require_once("../../cms_basic/head.php");
	require_once("../../cms.driver/mysql.driver.php");
	require_once("../../cms.driver/file.driver.php");
	
	$run = DB::connect();
	$handle = MDB::connect(HOST,DBNAME,USER,PASS);


	/* Page Actions */
	
		if( isset($_GET['gid'][0]) )
		{
	
			$res = $handle->query('SELECT `page_type`, `gallery_imgname` FROM `gallery` WHERE `gallery_id` = '.$_GET['gid'] );
			if( 0!=$res->num_rows )
			{
				
				$data = $res->fetch_assoc();
				$res->free();
				
				//var_dump( '../../media/Gallery/'.$data['page_type'].'/thumbs/'.$data['gallery_imgname'] );
				
				@unlink( "../../media/Gallery/".$data['page_type']."/thumbs/".$data['gallery_imgname'] );
				@unlink( '../../media/Gallery/'.$data['page_type'].'/imgs/'.$data['gallery_imgname'] );
				
				$handle->query('DELETE FROM `gallery` WHERE `gallery_id` = '.$_GET['gid'] );
				
			}
	
		}
		
		
		
		if( isset($_POST['submit'], $_FILES['media'], $_POST['pageType'][0] ) )
		{
			
			$pageType = $_POST['pageType'];
			$imgPath = '../../media/Gallery/'.$pageType;
			
			if( !is_dir($imgPath) )
			{
				mkdir($imgPath, 0777);
				mkdir($imgPath.'/thumbs/', 0777);
				mkdir($imgPath.'/imgs/', 0777);			
			}
			
			$imgName = $imgPath.'/'.$_FILES['media']['name'];
			
			if( move_uploaded_file( $_FILES['media']['tmp_name'], $imgName ) )
			{
				
				require_once 'IMG.class.php';
				
				$name = strtolower( pathinfo($_FILES['media']['name'], PATHINFO_FILENAME) );
				$ext = strtolower( pathinfo($_FILES['media']['name'], PATHINFO_EXTENSION) );
				$fullName = $name.'.'.$ext;
				
				$resizeObj1 = new Resize( $imgName, 150, 120, true, $imgPath.'/thumbs/', $name );
				$resizeObj1->ImageResize();
	
				$resizeObj2 = new Resize( $imgName, 800, 600, true, $imgPath.'/imgs/', $name );
				$resizeObj2->ImageResize();
				
				$checked = isset( $_POST['is_published'] ) ? 'Y' : 'N';
				$rs = $handle->query('INSERT INTO `gallery` ( `gallery_imgname`, `page_type`, `is_published`) VALUES ("'.$fullName.'", "'.$pageType.'", "'.$checked.'")');		
				unlink( $imgPath.'/'.$_FILES['media']['name']);
				
			}
	
		}
	
	/* Page Actions */




	$pageList = array();
	$list = DB::load($run, TABLE2, array('spg_id', 'spg_name'), array('spg_publish'=>'YES') );
	
	foreach( $list as $v )
	{
		$pageList[ $v['spg_id'] ] = $v['spg_name'];
	}
	
		
	$galleryList = array();
	$result = $handle->query('SELECT * FROM `gallery` WHERE `is_published` = "Y"');
	
	if( 0!=$result->num_rows )
	{
		while( ($row = $result->fetch_assoc()) )
		{
			$galleryList[] = $row;
		}
		$result->free();
	}
	
?>

<div class="block center content" >
<p class="contentheader">Gallery Manager</p>

<?php

	echo '<form method="post" action="" enctype="multipart/form-data">';
	
	if( !empty($result) )
	{

		echo '<div class="block cnt">';
		
		echo '<table width="98%" cellspacing="0" cellpadding="0" id="striped2" class="pickme"><thead><tr>',
				'<th width="3%">&nbsp;</th>',
				'<th width="22%"><span>Image Name</span></th>',
				'<th width="20%"><span>Page</span></th>',
				'<th width="20%"><span>Thumbnail</span></th>',
				'<th width="10%"><span>Publish</span></th>',
				'<th width="15%"><span>Options</span></th>',
			 '</tr></thead><tbody>';
		foreach( $galleryList as $v ) {
			$pageName = $v['page_type'] != 'MD' ? $pageList[ $v['page_type'] ] : 'MD';
			$thumbnail = '../../media/Gallery/'.$v['page_type'].'/thumbs/'.$v['gallery_imgname'];
			echo '<tr>',
				 	'<td class="cm"><input type="checkbox" name="seek[]" value="',$v['gallery_id'],'"  /></td>',
					'<td>',$v['gallery_imgname'],'</td>',
					'<td>',$pageName,'</td>',
					'<td><img src="',$thumbnail,'" /></td>',
					'<td>',$v['is_published'],'</td>',
					'<td><a href="./?gid=',$v['gallery_id'],'">Delete Image</a></td>',
				 '</tr>';
		}
		
		echo '</tbody></table>';
        echo '</div>';
		
	
	}
	
	
	$optionList = '';
	foreach( $pageList as $k=>$v )
	{
		$optionList .= '<option value="'.$k.'">'.$v.'</option>';
	}
	
	echo <<<A
<div class="block cnt">
<ul class="block opadd" style="width:850px; margin:20px auto 0;">
<li><label for="pwd">Add Media:</label>
<div class="left block">
<div class="fileinputs">
    <div class="inputs">
        <input class="file" name="media" id="file" onchange="file2.value = this.value" type="file" />
    <div class="fakefile">
        <input id="file2" value="Click to Browse ..." type="text" />
    </div>
    </div>
</div>
</div>

</li>
<li>
	<label>Image Page</label>
	<select name="pageType" class="iselect rw B">
	<option value="">Please Select page</option>
	<option>MD</option>
	{$optionList}
	</select>
</li>
<li>
	<label>Publish Image:</label><p style="padding:10px 0"><input type="checkbox" name="is_published" /></p>
	
</li>
</ul>

A;
	
?>

</div>


<div class="block bottomplace">
<input type="submit" name="submit" class="right ibutton" value="Add To Gallery" />
<input type="button" class="right ibutton" value="Delete"  onclick="window.location.href='add.category_poll.php'"  />
</div>
</form>
</div>

<?php require_once '../../cms_basic/foot.php'; ?>
<script type="text/javascript">
function $I(id) {
	return document.getElementById(id);
}

function tnxG(){
	var state = $I('spoon');
	var toggle = state.style.display;
	state.style.display = toggle == 'block' ? 'none' : 'block';
}
</script>
</body>
</html>
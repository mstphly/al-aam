<?php
	require_once dirname(__FILE__).'/config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
</head>
<body>
<?php require_once("cms_basic/head.php") ?>
<div class="block center content" >
<p class="contentheader">Account Settings</p>

<?php

	$acct = parse_ini_file(LOGON, true);
	
    echo <<<ACCOUNT
    <form method="post" action="cms_function/setting.modify.php">
    <div class="block cnt"><ul class="block opadd" style="width:450px; margin:20px auto 10px;">
    <li><label for="un">Email</label><input value="{$acct['CMS_ACCESS']['email']}" class="itext rw B" type="text" name="ac_email" /></li>
    <li><label for="pwd">Password</label><input value="{$acct['CMS_ACCESS']['pass']}" class="itext rw" type="text" name="ac_password"  /></li>
    <li><label for="pwd">Fullname</label><input value="{$acct['CMS_ACCESS']['fullname']}" class="itext rw" type="text" name="ac_fullname"  /></li>
    </ul>
    </div>
    <div class="block bottomplace">
    <input type="submit" class="right ibutton" value="Save Changes" />
    </div>
    </form>
ACCOUNT;

?>
</div>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
<?php
	require_once dirname(__FILE__).'/config.php';
	if( !isset($_GET['edit']) )
		die;		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
</head>
<body>
<?php require_once("cms_basic/head.php") ?>
<div class="block center content" >
<p class="contentheader">Edit Page</p>

<form method="post" action="cms_function/edit.complete.php">
<div class="block cnt">

<?php

	$parameters = explode(',', $_GET['edit']);

	$link = DB::connect();
	$rok = DB::load( $link, TABLE, '*', array('pg_id'=>$parameters) );
	
	$list = DB::load( $link, PGTYPE, '*' );
	
	if( $rok>0 ) {
		foreach( $rok as $v ) {
			$yes = ''; $no = '';
			if( $v['pg_publish']=='YES')
				$yes = 'selected';
			else
				$no = 'selected';
				
			
			$options = '';
			foreach( $list as $pageTypeItem )
			{
				$chosen = $pageTypeItem['pgtype_id'] == $v['pg_type'] ? 'selected="selected"' : '';
				$options .= '<option value="'.$pageTypeItem['pgtype_id'].'" '.$chosen.'>'.$pageTypeItem['pgtype_name'].'</option>';
			}
		echo <<<EDIT
				<ul class="block opadd" style="width:450px; margin:20px auto 10px;">
				<li><label for="un">Page Type</label><select class="iselect rw B" name="type[{$v['pg_id']}]">{$options}</select></li>
				<li><label for="un">Page Name</label><input class="itext rw B" type="text" name="name[{$v['pg_id']}]" value="{$v['pg_name']}" /></li>
				<li><label for="pwd">Page Title</label><input class="itext rw" type="text" name="title[{$v['pg_id']}]" value="{$v['pg_title']}" /></li>
				<li><label for="pwd">Page Url</label><input class="itext rw" type="text" name="url[{$v['pg_id']}]" value="{$v['pg_url']}" /></li>
				<li><label for="pwd">Publish Page</label><select class="iselect" name="publish[{$v['pg_id']}]"><option {$yes}>YES</option><option {$no}>NO</option></select></li>
				</ul>
EDIT;
		}
	}

?>


</div>

<div class="block bottomplace">
<input type="Submit" class="right ibutton" value="Save Changes" />
<input type="reset" class="right ibutton" value="Cancel" />
</div>
</form>
</div>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
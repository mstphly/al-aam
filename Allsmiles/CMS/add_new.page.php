<?php
	require_once dirname(__FILE__).'/config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
</head>
<body>
<?php

	require_once("cms_basic/head.php");
	
	$run = DB::connect();
	
	$result = DB::load( $run, PGTYPE );

?>
<div class="block center content" >
<p class="contentheader">Add New Page</p>

<form method="post" action="cms_function/new_page.php">
<div class="block cnt">

<ul class="block opadd" style="width:450px; margin:20px auto 10px;">
<li>
<label for="un">Page Name</label><select name="type" class="iselect rw B"><option value="">Please Choose Type</option>
<?php
	foreach( $result as $option )
	{
		echo '<option value="',$option['pgtype_id'],'">',$option['pgtype_name'],'</option>';
	}
?>
</select></li>
<li><label for="un">Page Name</label><input class="itext rw B" type="text" name="name" /></li>
<li><label for="pwd">Page Title</label><input class="itext rw" type="text" name="title"  /></li>
<li><label for="pwd">Page Url</label><input class="itext rw" type="text" name="url"  /></li>
<li><label for="pwd">Publish Page</label><p style="padding-top:7px; font-size:11px;">Yes <input type="radio" name="publish" value="YES" /> No <input type="radio" name="publish" value="NO" /></p></li>
</ul>




</div>

<div class="block bottomplace">
<?php
	$BUTTONstate = 'disabled';
	if( !empty($result) )
	{
		$BUTTONstate = 'enabled';
	}
?>
<input type="submit" <?php echo $BUTTONstate; ?> class="right ibutton" value="Create" />
<input type="reset" class="right ibutton"  <?php echo $BUTTONstate; ?> value="Cancel" />
</div>
</form>
</div>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
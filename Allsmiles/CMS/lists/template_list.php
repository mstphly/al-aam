<?php

	function getAll_dir_file($path){		
		$store=array(); 
		$p_opendir = opendir($path);
		while( $subdir = readdir($p_opendir) )
			$store[] = $subdir;
			
		return $store;
	}
	
	function ext_type($file){
		return pathinfo($file, PATHINFO_EXTENSION);
	}
	
	function extract_name($file) {
		return substr( $file, 0, -4 );
	}
	
	$files = getAll_dir_file('../media/');
	$allowed = array('html');
	
	$return = '';
	foreach( $files as $file ) {
		$ext = ext_type($file);
		if( in_array($ext, $allowed) ) 
			$return .= '["'.extract_name($file).'", "media/'.$file.'"],';
					
	}
	$return = substr( $return, 0 , -1);
	
	echo '
		var tinyMCETemplateList = new Array(
			'.$return.'
		)
	';
	exit;


?>
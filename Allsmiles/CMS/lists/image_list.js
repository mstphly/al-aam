// This list may be created by a server logic page PHP/ASP/ASPX/JSP in some backend system.
// There images will be displayed as a dropdown in all image dialogs if the "external_link_image_url"
// option is defined in TinyMCE init.
function Sack(file) {
    this.xmlhttp = null;
    var encoded,
    regexp,
    varArray,
    urlVars,
    totalurlstring,
    elemNodeName,
    urlstringtemp,
    key = '';
    this.resetData = function() {
        this.method = "POST";
        this.queryStringSeparator = "?";
        this.argumentSeparator = "&";
        this.URLString = "";
        this.encodeURIString = true;
        this.execute = false;
        this.element = null;
        this.elementObj = null;
        this.requestFile = file;
        this.vars = new Object();
        this.responseStatus = new Array(2);
    };
    this.resetFunctions = function() {
        this.onLoading = function() {};
        this.onLoaded = function() {};
        this.onInteractive = function() {};
        this.onCompletion = function() {};
        this.onError = function() {};
        this.onFail = function() {};
    };
    this.reset = function() {
        this.resetFunctions();
        this.resetData();
    };
    this.createAJAX = function() {
        try {
            var XmlHttpVersions = ["MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.5.0", "MSXML2.XMLHTTP.4.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"];
            var hz = XmlHttpVersions.length;
            for (var wp = 0; wp < hz; wp++) {
                this.xmlhttp = new ActiveXObject(XmlHttpVersions[wp]);
            }
        } catch(e1) {
            try {
                this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e2) {
                this.xmlhttp = null;
            }
        }
        if (!this.xmlhttp) {
			(typeof XMLHttpRequest != "undefined") ?this.xmlhttp = new XMLHttpRequest() : this.failed = true;
        }
    };
    this.setVar = function(name, value) {
        this.vars[name] = Array(value, false);
    };
    this.encVar = function(name, value, returnvars) {
		return (true === returnvars) ? Array(encodeURIComponent(name), encodeURIComponent(value)) : this.vars[encodeURIComponent(name)] = Array(encodeURIComponent(value), true);
    };
    this.processURLString = function(string, encode) {
        encoded = encodeURIComponent(this.argumentSeparator);
        regexp = new RegExp(this.argumentSeparator + "|" + encoded);
        varArray = string.split(regexp);
        var jj = varArray.length;
        for (var i = 0; i < jj; i++) {
            urlVars = varArray[i].split("=");
			(true === encode) ? this.encVar(urlVars[0], urlVars[1]) : this.setVar(urlVars[0], urlVars[1]);
        }
    };
    this.createURLString = function(urlstring) {
        if (this.encodeURIString && this.URLString.length) {
            this.processURLString(this.URLString, true);
        }
        if (urlstring) {
			
			(this.URLString.length) ? this.URLString += this.argumentSeparator + urlstring : this.URLString = urlstring;
        }
        this.setVar("recycler", new Date().getTime());
        urlstringtemp = [];
        for (key in this.vars) {
            if (false === this.vars[key][1] && true === this.encodeURIString) {
                encoded = this.encVar(key, this.vars[key][0], true);
                delete this.vars[key];
                this.vars[encoded[0]] = Array(encoded[1], true);
                key = encoded[0];
            }
            urlstringtemp[urlstringtemp.length] = key + "=" + this.vars[key][0];
        }
		this.URLString += (urlstring)?this.argumentSeparator + urlstringtemp.join(this.argumentSeparator) : urlstringtemp.join(this.argumentSeparator);
    };
    this.runResponse = function() {
        jP(this.response);
    };
    this.runAJAX = function(urlstring) {
        if (this.failed) {
            this.onFail();
        } else {
            this.createURLString(urlstring);
            if (this.element) {
                this.elementObj = xId(this.element);
            }
            if (this.xmlhttp) {
                var self = this;
                if (this.method == "GET") {
                    totalurlstring = this.requestFile + this.queryStringSeparator + this.URLString;
                    this.xmlhttp.open(this.method, totalurlstring, true);
                } else {
                    this.xmlhttp.open(this.method, this.requestFile, true);
                    try {
                        this.xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    } catch(e) {}
                }
                this.xmlhttp.onreadystatechange = function() {
                    switch (self.xmlhttp.readyState) {
                    case 1:
                        self.onLoading();
                        break;
                    case 2:
                        self.onLoaded();
                        break;
                    case 3:
                        self.onInteractive();
                        break;
                    case 4:
                        self.response = self.xmlhttp.responseText;
                        self.responseXML = self.xmlhttp.responseXML;
                        self.responseStatus[0] = self.xmlhttp.status;
                        self.responseStatus[1] = self.xmlhttp.statusText;
                        if (self.execute) {
                            self.runResponse();
                        }
                        if (self.elementObj) {
                            elemNodeName = self.elementObj.nodeName;
                            elemNodeName.toLowerCase();
                           (elemNodeName == "input" || elemNodeName == "select" || elemNodeName == "option" || elemNodeName == "textarea")?self.elementObj.value = self.response : self.elementObj.innerHTML = self.response;
                        }
						
						(self.responseStatus[0] == "200") ? self.onCompletion() : self.onError();
                        self.URLString = "";
                        break;
                    }
                };
                this.xmlhttp.send(this.URLString);
            }
        }
    };
    this.reset();
    this.createAJAX();
}


var jP = (function() {
    var number = '(?:-?\\b(?:0|[1-9][0-9]*)(?:\\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\\b)';
    var oneChar = '(?:[^\\0-\\x08\\x0a-\\x1f\"\\\\]' + '|\\\\(?:[\"/\\\\bfnrt]|u[0-9A-Fa-f]{4}))';
    var string = '(?:\"' + oneChar + '*\")';
    var jsonToken = new RegExp('(?:false|true|null|[\\{\\}\\[\\]]' + '|' + number + '|' + string + ')', 'g');
    var escapeSequence = new RegExp('\\\\(?:([^u])|u(.{4}))', 'g');
    var escapes = {
        '"': '"',
        '/': '/',
        '\\': '\\',
        'b': '\b',
        'f': '\f',
        'n': '\n',
        'r': '\r',
        't': '\t'
    };
    function unescapeOne(_, ch, hex) {
        return ch ? escapes[ch] : String.fromCharCode(parseInt(hex, 16));
    }
    var EMPTY_STRING = '';
    var SLASH = '\\';
    var firstTokenCtors = {
        '{': Object,
        '[': Array
    };
    var hop = Object.hasOwnProperty;
    return function(json, opt_reviver) {
        var toks = json.match(jsonToken);
        var result;
        var tok = toks[0];
        if ('{' === tok) {
            result = {};
        } else if ('[' === tok) {
            result = [];
        } else {
            throw new Error(tok);
        }
        var key;
        var stack = [result];
        var n = toks.length;
        for (var i = 1; i < n; ++i) {
            tok = toks[i];
            var cont;
            switch (tok.charCodeAt(0)) {
            default:
                cont = stack[0];
                cont[key || cont.length] = +(tok);
                key = void(0);
                break;
            case 0x22:
                tok = tok.substring(1, tok.length - 1);
                if (tok.indexOf(SLASH) !== -1) {
                    tok = tok.replace(escapeSequence, unescapeOne);
                }
                cont = stack[0];
                if (!key) {
                    if (cont instanceof Array) {
                        key = cont.length;
                    } else {
                        key = tok || EMPTY_STRING;
                        break;
                    }
                }
                cont[key] = tok;
                key = void 0;
                break;
            case 0x5b:
                cont = stack[0];
                stack.unshift(cont[key || cont.length] = []);
                key = void 0;
                break;
            case 0x5d:
                stack.shift();
                break;
            case 0x66:
                cont = stack[0];
                cont[key || cont.length] = false;
                key = void 0;
                break;
            case 0x6e:
                cont = stack[0];
                cont[key || cont.length] = null;
                key = void 0;
                break;
            case 0x74:
                cont = stack[0];
                cont[key || cont.length] = true;
                key = void 0;
                break;
            case 0x7b:
                cont = stack[0];
                stack.unshift(cont[key || cont.length] = {});
                key = void 0;
                break;
            case 0x7d:
                stack.shift();
                break;
            }
        }
        if (stack.length) {
            throw new Error();
        }
        if (opt_reviver) {
            var walk = function(holder, key) {
                var value = holder[key];
                if (value && typeof value === 'object') {
                    var toDelete = null;
                    for (var k in value) {
                        if (hop.call(value, k) && value !== holder) {
                            var v = walk(value, k);
                            if (v !== void 0) {
                                value[k] = v;
                            } else {
                                if (!toDelete) {
                                    toDelete = [];
                                }
                                toDelete.push(k);
                            }
                        }
                    }
                    if (toDelete) {
                        for (var i = toDelete.length; --i >= 0;) {
                            delete value[toDelete[i]];
                        }
                    }
                }
                return opt_reviver.call(holder, key, value);
            };
            result = walk({
                '': result
            },
            '');
        }
        return result;
    };
})();




var finito = '';
var jx = new Sack();
jx.reset();
jx.setVar("type", 'media-img');
jx.requestFile = 'file_lister.php';
jx.onCompletion = function() {
	alert('ok');
	finito = jx.response;
};
jx.runAJAX();

var tinyMCEImageList = finito;

//var tinyMCEImageList = new Array(
//	// Name, URL
//	finito
//);
//	["Logo 1", "media/logo.jpg"],
//	["Logo 2 Over", "media/logo_over.jpg"]

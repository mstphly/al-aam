<?php
	
	function getAll_dir_file($path){		
		$store=array(); 
		$p_opendir = opendir($path);
		while( $subdir = readdir($p_opendir) )
			$store[] = $subdir;
			
		return $store;
	}
	
	function ext_type($file){
		return pathinfo($file, PATHINFO_EXTENSION);
	}
	
	function extract_name($file) {
		return substr( $file, 0, -4 );
	}
	
	$files = getAll_dir_file('../media/');
	$allowed = array('jpg', 'peg', 'png', 'gif');

	switch( $_GET['type'] ) {
		case 'media-img':
			$allowed = array('jpg', 'peg', 'png', 'gif');
			$sync = 'tinyMCEImageList';
		break;
		case 'media-movie':
			$allowed = array('avi', 'flv', 'dcr', 'mov', 'ram', 'rm', 'swf');
			$sync = 'tinyMCEMediaList';
		break;
		case 'file-template':
			$allowed = array('html');
			$sync = 'tinyMCETemplateList';
		break;
	}
	
	$return = '';
	foreach( $files as $file ) {
		$ext = ext_type($file);
		if( in_array($ext, $allowed) ) 
			$return .= '["'.extract_name($file).'", "media/'.$file.'"],';
					
	}
	$return = substr( $return, 0 , -1);
	
	echo '
		var '.sync.' = new Array(
			'.$return.'
		)
	';
	exit;


?>
<?php
	require_once dirname(__FILE__).'/config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
<style type="text/css">

table{ margin:5px auto;table-layout:fixed; font-size:12px; border:1px solid #eee; padding:0;}
table thead{
background: #eee;
background: -moz-linear-gradient(top, #FCFCFC 0%, #F2F2F2 32%, #E8E8E8 60%, #E3E8ED 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#FCFCFC), color-stop(32%,#F2F2F2), color-stop(60%,#E8E8E8), color-stop(100%,#E3E8ED));
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FCFCFC', endColorstr='#E3E8ED',GradientType=0 )
 }
table thead tr{  border:1px solid red; }
table thead tr th{padding:5px 0;}
table thead tr th span{ border-left:1px solid #ccc; padding:0 10px; float:left;}
table tbody td{  border-top:1px solid #f6f6f6; margin:0; padding:5px 10px 4px;}
.cm{ vertical-align:middle; background:#f9f9f9}
.cm{  text-align:center; padding-left:8px;}
table tbody tr:hover{ background:#F4F7FA}
table tbody tr.selected { background:#E8EDF4;}
</style>
</head>
<body>
<?php require_once("cms_basic/head.php") ?>
<div class="block center content" >
<p class="contentheader">View All Sub-Page</p>


<?php
	
	$run = DB::connect();
	$result = DB::load( $run, TABLE2, '*', array('pg_id'=>$_GET['go']) );
	
	
	if( !empty($result) ) {
		
		echo '<form method="post" action="cms_function/action_01.php?go=',$_GET['go'],'" onsubmit="return returnee();">',
			 '<div class="block cnt">';
		
		echo '<table width="98%" cellspacing="0" cellpadding="0" id="striped2" class="pickme"><thead><tr>',
				'<th width="3%">&nbsp;</th>',
				'<th width="5%"><span>ID</span></th>',
				'<th width="10%"><span>Pages</span></th>',
				'<th width="20%"><span>Title</span></th>',
				'<th width="10%"><span>Published</span></th>',
				'<th width="23%"><span>Url</span></th>',
				'<th width="30%"><span>Options</span></th>',
			 '</tr></thead><tbody>';
		foreach( $result as $v ) {
			echo '<tr>',
				 	'<td class="cm"><input type="checkbox" name="seek[]" value="',$v['spg_id'],'"  /></td>',
					'<td>',$v['spg_id'],'</td>',
					'<td>',$v['spg_name'],'</td>',
					'<td>',$v['spg_title'],'</td>',
					'<td>',$v['spg_publish'],'</td>',
					'<td>',$v['spg_url'],'</td>',
					'<td><a href="view.page_content.php?view=',$v['spg_id'],'&type=sub">View Contents</a> | <a href="add_new.content.php?go=',$v['spg_id'],'&type=sub">Add Content</a> | <a href="view.extras.php?go=',$v['spg_id'],'">Extra Pages</a></td>',
				 '</tr>';
		}
		
		echo '</tbody></table>';
        echo '</div>',
             '<div class="block bottomplace">',
                '<input type="submit" class="right ibutton" value="Delete Sub-Page" name="button" />',
                '<input type="submit" class="right ibutton" value="Edit Sub-Page" name="button" />',
				'<input type="button" class="right ibutton" value="Goto Page" name="button" />',
             '</div>',
			 '</form>';
	
	} else { 
		echo '<div class="block cnt">',
				'<div style="padding:30px; margin:10px; border:1px solid #eee; text-align:center; color:grey; font-size:12px;">This Website Page Has No Sub-Page <a href="add_new.subpage.php?go=',$_GET['go'],'">Add New Sub-Page</a></div>',
			 '</div>'; 
	}
	
?>


</div>
<script type="text/javascript" src="cms_js/row_lock.js"></script>
<script type="text/javascript" defer="defer">
function returnee() {
	var chk = document.getElementsByTagName("input");
	for(var i=0,k=chk.length;i<k;i++) {
		if( chk[i].checked )
			return true;
	}
		return false;
}

// Helps reset checkfields on page load
var chk = document.getElementsByTagName("input");
for(var i=0,k=chk.length;i<k;i++) {
	if( chk[i].checked )
		chk[i].checked = false;
}
</script>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
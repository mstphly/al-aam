<?php
	require_once dirname(__FILE__).'/config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
<style type="text/css">

table{ margin:5px auto;table-layout:fixed; font-size:12px; border:1px solid #eee; padding:0;}
table thead{
background: #eee;
background: -moz-linear-gradient(top, #FCFCFC 0%, #F2F2F2 32%, #E8E8E8 60%, #E3E8ED 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#FCFCFC), color-stop(32%,#F2F2F2), color-stop(60%,#E8E8E8), color-stop(100%,#E3E8ED));
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FCFCFC', endColorstr='#E3E8ED',GradientType=0 )
 }
table thead tr{  border:1px solid red; }
table thead tr th{padding:5px 0;}
table thead tr th span{ border-left:1px solid #ccc; padding:0 10px; float:left;}
table tbody td{  border-top:1px solid #f6f6f6; margin:0; padding:5px 10px 4px;}
.cm{ vertical-align:middle; background:#f9f9f9}
.cm{  text-align:center; padding-left:8px;}
table tbody tr:hover{ background:#F4F7FA}
table tbody tr.selected { background:#E8EDF4;}
</style>
</head>
<body>
<?php require_once("cms_basic/head.php") ?>
<div class="block center content" >
<p class="contentheader">View Page Content</p>


<?php
	
	$run = DB::connect();
	
	$where = isset( $_GET['type'] ) ? array('pg_id'=>$_GET['view'], 'pg_type'=>'sub') : array('pg_id'=>$_GET['view'],'pg_type'=>'');
	
	$result = DB::load( $run, STORE, '*', $where );
	
	
	if( !empty($result) ) {
		$formtype = isset( $_GET['type'] ) ? '?type=sub' : '';
		echo '<form method="post" action="cms_function/view.content_form.php',$formtype,'">',
				'<input type="hidden" name="return" value="',$_GET['view'],'" />',
			 '<div class="block cnt">';
		
		echo '<table width="98%" cellspacing="0" cellpadding="0" id="striped2" class="pickme"><thead><tr>',
				'<th width="3%"></th>',
				'<th width="10%"><span>Unique Id</span></th>',
				'<th width="23%"><span>Title</span></th>',
				'<th width="25%"><span>Tip Content</span></th>',
				'<th width="10%"><span>Published</span></th>',
				'<th width="10%"><span>Media</span></th>',
				'<th width="20%"><span>Options</span></th>',
			 '</tr></thead><tbody>';
		foreach( $result as $v ) {
			echo '<tr>',
				 	'<td class="cm"><input type="checkbox" name="seek[]" value="',$v['cid'],'"  /></td>',
					'<td>',$v['cid'],'</td>',
					'<td>',$v['s_title'],'</td>',
					'<td style="color:#aaa">',shorten($v['s_content'], 150),'</td>',
					'<td>',$v['s_publish'],'</td>',
					'<td>',$v['media'],'</td>',
					'<td><a href="delete.content.php?kill=',$v['cid'],'&amp;return=',$_GET['view'],'">Delete Content</a> | <a href="edit.content.php?view=',$v['cid'],'">Edit Content</a></td>',
				 '</tr>';
		}
		
		echo '</tbody></table>';
        echo '</div>',
             '<div class="block bottomplace">',
				'<input type="submit" class="right ibutton" value="Delete Section" name="button" />',
                '<input type="button" onclick="document.location=\'add_new.content.php?go=',$_GET['view'],'\'" class="right ibutton" value="Add New Section" name="button" />',
				'<input type="button" onclick="\'window.history.go(-1)\'" class="right ibutton" value="&larr;Back" />',
             '</div>',
			 '</form>';
	
	} else { 
		$type = isset( $_GET['type'] ) ? '&type='.$_GET['type'] : '';
		echo '<div class="block cnt">',
				'<div style="padding:30px; margin:10px; border:1px solid #eee; text-align:center; color:grey; font-size:12px;">This Page Has No Content <a href="add_new.content.php?go=',$_GET['view'],$type,'">Add New Content</a></div>',
			 '</div>'; 
	}
	
?>

	

</div>
<script type="text/javascript" src="cms_js/row_lock.js"></script>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
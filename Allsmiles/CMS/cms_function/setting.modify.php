<?php
	
/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
	require_once '../config.php';
	if( is_file(LOGON) ) {
		
		$content = parse_ini_file(LOGON,true);
		$rebuild = '[CMS_ACCESS]'."\n";
		$rebuild .= 'email = '.$_POST['ac_email']."\n";
		$rebuild .= 'pass = '.$_POST['ac_password']."\n";
		$rebuild .= 'fullname = '.$_POST['ac_fullname'];
		
		file_put_contents(LOGON, $rebuild);
		
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ../setting.edit.php?info=1');
		exit;
	
	}

?>
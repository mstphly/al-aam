<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */

	if( !isset($_SESSION['_cms']) || $_SESSION['_cms'] != 1 ) {
		if( basename($_SERVER['SCRIPT_FILENAME']) != 'login.php' )
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: login.php?redirect='.basename($_SERVER['PHP_SELF']) );
			exit;
		}
	}
?>
<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */

	require_once '../config.php';
	
	if( isset($_POST['button']) ) {
		if( $_POST['button'] == 'Upload' ) {
			
			if( !empty($_FILES['media']['name']) ) {
				
				if( !is_dir(IMG_PATH) )
					mkdir(IMG_PATH, 0777);

				if( move_uploaded_file( $_FILES['media']['tmp_name'], IMG_PATH.'/'.$_FILES['media']['name'] ) )
					echo 1;
				else
					echo 0;
				
				exit;
			}

		} else {
			
			if( isset($_POST['section']) && !empty($_POST['section']) ) {

				switch( DBTYPE ) {
					case 'MYSQL':
						require_once '../cms.driver/mysql.driver.php';
					break;
					case 'SQLITE':
						require_once '../cms.driver/sqlite.driver.php';
					break;
					case 'FILE':
						require_once '../cms.driver/file.driver.php';
					break;
				}	
				
				$has_media = 'NO';
				$media = array('<img', '<object');
				foreach( $media as $sym ) {
					$a = strstr($_POST['content'],$sym );
					if( !empty( $a ) ) {
						$has_media = 'YES';
						break;
					}
				}
		
				$link = DB::connect( );
				$rok = DB::update( $link, STORE , array('s_title'=>$_POST['name'], 's_content'=>$_POST['content'], 'media'=>$has_media, 's_publish'=>$_POST['publish']), array( 'cid'=>$_POST['section'] ) );
				
				if( $rok>0 ) {
					echo '11';
					exit;
				}
				
			}
				echo 419;
				exit;
		}
	}

?>
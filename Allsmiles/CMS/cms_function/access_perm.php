<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
	session_start();
	require_once '../config.php';

	if( is_file(LOGON) )
	{
	
		$content = parse_ini_file( LOGON, true );
		
		if( $content['CMS_ACCESS']['email'] == $_POST['ac_email'] && $content['CMS_ACCESS']['pass'] == $_POST['ac_password'] ) 
		{
			
			$_SESSION['__lang'] = $_POST['lang'] != 'Default' ? $_POST['lang'] : '';
			$_SESSION['_cms'] = 1;
			$_SESSION['site_manager'] = $content['CMS_ACCESS']['fullname'];
			session_regenerate_id(true);
			session_write_close();	
			
			#sync to index
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ../'.$_POST['redirect']);
			exit;
		
		} else
		{
			
			#back to login
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ../login.php?info=0');
			exit;
		
		}
	}
	
	exit('This Application is Not Fully Installed');
	
?>
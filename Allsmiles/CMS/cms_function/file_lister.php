<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
	if( !isset($_GET['type']) )
		exit;
	
	function getAll_dir_file($path){		
		$store=array(); 
		$p_opendir = opendir($path);
		while( $subdir = readdir($p_opendir) )
			$store[] = $subdir;
			
		return $store;
	}
	
	function ext_type($file){
		return pathinfo($file, PATHINFO_EXTENSION);
	}
	
	function extract_name($file) {
		return substr( $file, 0, -4 );
	}
	
	$files = getAll_dir_file('../media/');

	switch( $_GET['type'] ) {
		case 'media-img':
			$allowed = array('jpg', 'peg', 'png', 'gif');
		break;
		case 'media-movie':
			$allowed = array('avi', 'flv', 'dcr', 'mov', 'ram', 'rm', 'swf');
		break;
		case 'file-template':
			$allowed = array('tml');
		break;
	}
	
	$return =array();
	foreach( $files as $file ) {
		$ext = ext_type($file);
		if( in_array($ext, $allowed) ) 
			$return[] = array( extract_name($file), 'media/'.$file );
					
	}
	
	echo json_encode( $return );
	exit;

?>
<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
	require_once '../config.php';

	
	if ( isset($_POST['button']) && isset($_POST['seek']) && !empty($_POST['seek']) ) {

		switch( DBTYPE ) {
			case 'MYSQL':
				require_once '../cms.driver/mysql.driver.php';
			break;
			case 'SQLITE':
				require_once '../cms.driver/sqlite.driver.php';
			break;
			case 'FILE':
				require_once '../cms.driver/file.driver.php';
			break;
		}	

		$link = DB::connect();

		switch( $_POST['button'] ) {
			case 'Delete Sub-Page':
				// Delete from container then sub-content
				$rok1 = DB::delete( $link, STORE, array('pg_id'=>$_POST['seek'], 'pg_type'=>'sub') );
				$rok2 = DB::delete( $link, TABLE2, array('spg_id'=>$_POST['seek']) );
				
				if( DB::affected_rows() > 0 )
				{
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: ../view.subpages.php?go='.$_GET['go'].'&info=1');					
				} else
				{
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: ../view.subpages.php?go='.$_GET['go'].'&info=0');					
				}
				
			break;
			case 'Delete Page':
			
				$rok1 = DB::delete( $link, STORE, array('pg_id'=>$_POST['seek'], 'pg_type'=>'') );
				$rok2 = DB::delete( $link, TABLE, array('pg_id'=>$_POST['seek']) );
				
				if( DB::affected_rows()>0 ) {
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: ../view.pages.php?info=1');
				} else {
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: ../view.pages.php?info=0');
				}
			
			break;
			case 'Delete':

				$rok2 = DB::delete( $link, TABLE3, array('mp_id'=>$_POST['seek']) );
				
				if( DB::affected_rows() > 0 )
				{
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: ../view.extras.php?go='.$_GET['go'].'&info=1');					
				} else
				{
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: ../view.extras.php?go='.$_GET['go'].'&info=0');					
				}
				
			break;
			case 'Edit Page':
				header('Location: ../edit.page.php?edit='.implode(',', $_POST['seek']) );
			break;
			case 'Edit Sub-Page':
				header('Location: ../edit.subpage.php?go='.$_GET['go'].'&edit='.implode(',', $_POST['seek']) );
			break;
			case 'Edit':
				header('Location: ../edit.extras.php?go='.$_GET['go'].'&edit='.implode(',', $_POST['seek']) );
			break;
			case 'Goto Page':
			break;
		}

	}

?>
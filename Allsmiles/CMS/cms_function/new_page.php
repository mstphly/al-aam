<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
	require_once '../config.php';
	require_once './cms_cleaner.php';
	
	switch( DBTYPE ) {
		case 'MYSQL':
			require_once '../cms.driver/mysql.driver.php';
		break;
		case 'SQLITE':
			require_once '../cms.driver/sqlite.driver.php';
		break;
		case 'FILE':
			require_once '../cms.driver/file.driver.php';
		break;
	}
	
	$link = DB::connect();
	$result = DB::insert( $link, TABLE, array('pg_name','pg_title','pg_url','pg_publish','pg_type'), array( $_POST['name'], $_POST['title'], $_POST['url'], $_POST['publish'], $_POST['type'] ) );
	
	if( $result > 0 ) {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ../view.pages.php?info=1&type=new');
	} else {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location; ../view.pages.php?info=0&type=new');
	}
	exit;
	
?>
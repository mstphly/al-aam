<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */

function shorten($v, $to=10){ $h = ''; $t = $to; if( strlen($v) > $t ){ $h = substr($v,0,$t).'...'; return $h; } else { return $v; } }
function write_ini_file($file, array $options){
    $tmp = '';
    foreach($options as $section => $values){
        $tmp .= "[$section]\n";
        foreach($values as $key => $val){
            if(is_array($val)){
                foreach($val as $k =>$v){
                    $tmp .= "{$key}[$k] = \"$v\"\n";
                }
            }
            else
                $tmp .= "$key = \"$val\"\n";
        }
        $tmp .= "\n";
    }
    file_put_contents($file, $tmp);
    unset($tmp);
}

?>
<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
	require_once '../config.php';
	require_once './cms_cleaner.php';

	switch( DBTYPE ) {
		case 'MYSQL':
			require_once '../cms.driver/mysql.driver.php';
		break;
		case 'SQLITE':
			require_once '../cms.driver/sqlite.driver.php';
		break;
		case 'FILE':
			require_once '../cms.driver/file.driver.php';
		break;
	}	
	$link = DB::connect();
	$ok=0;
	
	
	
	
	
	
	$extraLink = '';
	$plus = '?';
	
	if( isset($_GET['extras']) )
	{
		
		$extraLink = 'extras';
		$plus .= 'go='.$_GET['extras'].'&';
		
		foreach( $_POST['name'] as $k=>$v ) {
			
			$modify = DB::update( $link, TABLE3, array(
														'mp_name'=>$_POST['name'][$k],
														'mp_title'=>$_POST['title'][$k],
														'mp_url'=>$_POST['url'][$k],
														'mp_publish'=>$_POST['publish'][$k]
													 ), array('mp_id'=>$k), 1); 	
			
			if( $modify>0 )
				$ok = 1;
		
		}
		
		
		
		
	}elseif( isset($_GET['sub']) )
	{
		
		$extraLink = 'subpages';
		$plus .= 'go='.$_GET['sub'].'&';
		foreach( $_POST['name'] as $k=>$v ) {
			
			$modify = DB::update( $link, TABLE2, array(
														'spg_name'=>$_POST['name'][$k],
														'spg_title'=>$_POST['title'][$k],
														'spg_url'=>$_POST['url'][$k],
														'spg_publish'=>$_POST['publish'][$k]
													 ), array('spg_id'=>$k), 1); 	
			
			if( $modify>0 )
				$ok = 1;
		
		}
		
	} else
	{
		$extraLink = 'pages';
		foreach( $_POST['name'] as $k=>$v ) {
			
			$modify = DB::update( $link, TABLE, array(
														'pg_name'=>$_POST['name'][$k],
														'pg_title'=>$_POST['title'][$k],
														'pg_url'=>$_POST['url'][$k],
														'pg_publish'=>$_POST['publish'][$k],
														'pg_type'=>$_POST['type'][$k]
													 ), array('pg_id'=>$k), 1); 	
			
			if( $modify>0 )
				$ok = 1;
		
		}
	}
	
	
	
	
	if( $ok==1 ) {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ../view.'.$extraLink.'.php'.$plus.'info=1&type=edit');
		exit;
	} else {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ../view.'.$extraLink.'.php'.$plus.'info=1&type=edit');
		exit;
	}
	


?>
<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
	require_once '../config.php';
	require_once './cms_cleaner.php';
	
	switch( DBTYPE ) {
		case 'MYSQL':
			require_once '../cms.driver/mysql.driver.php';
		break;
		case 'SQLITE':
			require_once '../cms.driver/sqlite.driver.php';
		break;
		case 'FILE':
			require_once '../cms.driver/file.driver.php';
		break;
	}
	
	$link = DB::connect();
	
	$result = 0;
	if( !empty($_POST['sname']) )
	{
		$result = DB::insert( $link, TABLE2, array('pg_id', 'spg_name','spg_title','spg_url','spg_publish'), array( $_POST['sname'], $_POST['name'], $_POST['title'], $_POST['url'], $_POST['publish'] ) );
	}
	
	if( $result > 0 ) {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ../view.subpages.php?go='.$_POST['sname'].'&info=1&type=new');
	} else {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location; ../view.subpages.php?go='.$_POST['sname'].'&info=0&type=new');
	}
	exit;
	
?>
<?php
	require_once dirname(__FILE__).'/config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
</head>
<body>
<?php
	require_once("cms_basic/head.php");
?>
<div class="block center content" >
<p class="contentheader">Add New Page Type</p>

<form method="post" action="cms_function/new_page.php">
<div class="block cnt">

<ul class="block opadd" style="width:450px; margin:20px auto 10px;">
<li><label for="un">Page-Type Name</label><input class="itext rw B" type="text" name="name" /></li>
</ul>




</div>

<div class="block bottomplace">
<input type="submit" class="right ibutton" value="Create" />
<input type="reset" class="right ibutton"  value="Cancel" />
</div>
</form>
</div>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
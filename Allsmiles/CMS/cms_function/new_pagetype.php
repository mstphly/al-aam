<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
	require_once '../config.php';
	require_once './cms_cleaner.php';
	
	switch( DBTYPE ) {
		case 'MYSQL':
			require_once '../cms.driver/mysql.driver.php';
		break;
		case 'SQLITE':
			require_once '../cms.driver/sqlite.driver.php';
		break;
		case 'FILE':
			require_once '../cms.driver/file.driver.php';
		break;
	}
	
	$link = DB::connect();
	if( !empty($_POST['type']) )
	{
		$result = DB::insert( $link, PGTYPE, array('pgtype_name'), array( $_POST['type'] ) );
	}	

	header('HTTP/1.1 301 Moved Permanently');
	header('Location: ../add.pagetype.php');
	exit(0);
		
?>
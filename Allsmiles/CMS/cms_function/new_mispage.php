<?php

	/*
	 *	All Thanks to Almighty God
	 *	For a Peace and Love Handwriting
	 *	Author: Ojutiku Oluwatobi
	 */
 
	require_once '../config.php';
	require_once './cms_cleaner.php';
	
	switch( DBTYPE ) {
		case 'MYSQL':
			require_once '../cms.driver/mysql.driver.php';
		break;
		case 'SQLITE':
			require_once '../cms.driver/sqlite.driver.php';
		break;
		case 'FILE':
			require_once '../cms.driver/file.driver.php';
		break;
	}
	
	$link = DB::connect();
	
	$result = 0;
	if( !empty($_POST['sname']) )
	{
		$result = DB::insert( $link, TABLE3, array('p_id', 'p_type', 'mp_name', 'mp_title', 'mp_url', 'mp_publish'), array( $_POST['sname'], 'SUB', $_POST['name'], $_POST['title'], $_POST['url'], $_POST['publish']) );
	}
	
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: ../view.extras.php?go='.$_POST['sname']);
	exit;
	
?>
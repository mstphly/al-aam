<?php
	require_once dirname(__FILE__).'/config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
<style type="text/css">
.inputs INPUT { width: 300px; height: 25px; background: url("cms_img/HSV2.png") no-repeat scroll 16px center rgb(233, 242, 241); border: 1px solid rgb(216, 234, 239); border-radius: 5px 5px 5px 5px; float: left; line-height: 32px; font-size: 13px; padding: 0pt 10px 0pt 40px; }
INPUT[type="file"] { background: url("cms_img/HSV2.png") no-repeat scroll left center rgb(233, 242, 241); height: 71px; }
DIV.fileinputs { position: relative;}
INPUT.file { width: 350px; height: 25px; background: url("cms_img/HSV2.png") no-repeat scroll 16px center rgb(233, 242, 241); border: 1px solid rgb(216, 234, 239); border-radius: 5px 5px 5px 5px; float: left; line-height: 32px; font-size: 13px; padding: 0pt 10px 0pt 40px; }
DIV.fakefile { margin-top: -1px; position: absolute; left: 100px; z-index: 0;}
INPUT.file { position: relative; text-align: right; opacity: 0; z-index: 200; }
</style>
</head>
<body>
<?php require_once("cms_basic/head.php") ?>
<div class="block center content" >
<p class="contentheader">Add Content</p>

<form method="post" name="addcontentform" action="cms_function/new.content_form.php" enctype="multipart/form-data" onsubmit="return UPLOADER.submit(this, {onStart : run, onComplete : dc})">
<div class="block cnt">
<input type="hidden" name="pg" value="<?php echo $_GET['go']; ?>" />
<input type="hidden" name="pg_type" value="<?php echo isset($_GET['type']) ? $_GET['type'] : ''; ?>" />

<ul class="block opadd" style="width:850px; margin:20px auto 0;">
<li><label for="un">Section Name</label><input class="itext rw B" type="text" name="name" tabindex="1" /></li>
<li><label for="un">Content</label><div class="block"><textarea id="elm1" name="content" rows="15" cols="80" style="width: 80%" tabindex="2"></textarea></div></li>
<li><label for="pwd">Publish</label><p style="padding-top:7px; font-size:11px;">Yes <input type="radio" name="publish" value="YES" tabindex="3" /> No <input type="radio" name="publish" value="NO" tabindex="4" /></p></li>
<li><label for="pwd">Add Media:</label><p style="padding-top:7px; font-size:11px;"><a href="#" onclick="return tnxG()" tabindex="5">Quick Media Upload (Optional, Note: File Overwrite if exists)</a></p>

<div class="clearfix" style="font-size:12px;" id="story"></div>
<div id="spoon" style="display:none">
<div class="fileinputs">
    <div class="inputs">
        <input class="file" name="media" id="file" size="60" onchange="file2.value = this.value" type="file" />
    <div class="fakefile">
        <input size="82" id="file2" value="Click to Browse ..." type="text" />
    </div>
    </div>
</div>
<input type="submit" class="ibutton" style=" position:relative; left:100px; padding:2px 10px;" value="Upload" name="button" />
</div>
</li>
</ul>




</div>

<div class="block bottomplace">

<input type="submit" class="right ibutton" value="Add Content" name="button" tabindex="6" />
<input type="reset" class="right ibutton" value="Cancel" name="button" tabindex="7" />
<div id="status" class="right" style=" margin-top:8px; font-size:11px; color:#030; background:url(cms_img/spinner-0.gif) no-repeat; padding-left:20px; display:none"> request is been processed</div>
</div>
</form>
</div>
<script type="text/javascript" src="cms_js/script_uploader.js" defer="defer"></script>
<script type="text/javascript" src="cms_external_lib/tiny_mce/tiny_mce.js" ></script>
<script type="text/javascript" defer="defer">
tinyMCE.init({
    // General options
    mode: "textareas",
    theme: "advanced",
    plugins: "autolink,lists,pagebreak,style,layer,table,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

    // Theme options
    theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2: "cut,copy,paste,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview,|,forecolor",
    theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl",
    theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,nonbreaking,template",
    theme_advanced_toolbar_location: "top",
    theme_advanced_toolbar_align: "left",
    theme_advanced_statusbar_location: "bottom",
    theme_advanced_resizing: true,

    // Example content CSS (should be your site CSS)
    //content_css: "css/content.css",

    // Drop lists for link/image/media/template dialogs
    template_external_list_url: "lists/template_list.php",
    external_image_list_url: "lists/image_list.php",
    media_external_list_url: "lists/movie_list.php",

    // Style formats
    style_formats: [{
        title: 'Bold text',
        inline: 'b'
    }, {
        title: 'Red text',
        inline: 'span',
        styles: {
            color: '#ff0000'
        }
    }, {
        title: 'Red header',
        block: 'h1',
        styles: {
            color: '#ff0000'
        }
    }, {
        title: 'Example 1',
        inline: 'span',
        classes: 'example1'
    }, {
        title: 'Example 2',
        inline: 'span',
        classes: 'example2'
    }, {
        title: 'Table styles'
    }, {
        title: 'Table row 1',
        selector: 'tr',
        classes: 'tablerow1'
    }],

    // Replace values for the template plugin
    template_replace_values: {
        username: "cms_manager",
        staffid: "111111"
    }
});

function $I(id) {
	return document.getElementById(id);
}

function dc( response ) {
	
	/*hide status*/
	//button
	$I('status').style.display = 'none';
	var disp = $I('story');
	switch( response ) {
		case 1:case 0:case '1':case '0':
			if( response==1 ){disp.style.color='GREEN'; disp.innerHTML = 'Media Successfully Uploaded';}
			else{disp.style.color='RED'; disp.innerHTML = 'Media Upload Failed';}
		break;
		default:
			if( response==11 ){ alert('Section Successfully Added'); document.forms['addcontentform'].reset(); }
			else{ alert( response ); }
		break;
	}
	
}

function run() {
	$I('status').style.display = 'block';
	return true;
}

function tnxG(){
	var state = $I('spoon');
	var toggle = state.style.display;
	state.style.display = toggle == 'block' ? 'none' : 'block';
}
</script>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="../cms_css/style.css" />
<style type="text/css">
#acloginpod {
    background: url("../cms_img/acloginpodbg.gif") repeat-x scroll 0 0 #EBEBEB;
    border: 1px solid #D3D3D3;
    border-radius: 7px 7px 7px 7px;
    width: 320px;
}
.acloginform {
    margin: 22px;
}
fieldset,form {
    margin: 0 !important;
    padding: 0 !important;
}
#acloginpod label {
    color: #444444;
    display: block;
    font-size: 15px;
    margin-bottom: 3px;
}

#acloginpod input.textinput,#acloginpod select.textinput {
    background: url("../cms_img/textinputbg.gif") repeat-x scroll 0 0 #FFFFFF;
    border: 1px solid #D3D3D3;
    color: #000000;
    font-size: 15px;
    margin-bottom: 10px;
    padding: 7px 0;
    text-indent: 7px;
    width: 100%;
}

#acloginpod select.textinput{
padding:5px 12px 5px 0;
}

.fl-left {
    float: left;
}
#acloginpod .acloginbttn {
    margin-top: 10px;
    width: 121px;
}

#acloginpod legend em {
    left: -9999em;
    position: absolute;
}

</style>
</head>

<body>
<div id="header" class="block">
    <div id="branding">
        <p id="name"><a id="home" href="#">Cms Manager Setup</a></p>
    </div>
 </div>

<div id="content">    
	
    <div id="acloginpod" style="margin:50px auto;">
        <div class="acloginform">
          <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" autocomplete="off">
            <input type="hidden" value="<?php echo isset($_GET['redirect']) ? $_GET['redirect'] : 'index.php'; ?>" name="redirect">
            <fieldset>
            <legend><em>Enter your AccountCenter login information below</em></legend>
            <label for="primary_email">DataStore Type:</label>
            <select class="textinput" tabindex="2" name="type">
            <option>SELECT</option>
            <option>MYSQL</option>
            <option>SQLITE</option>
            </select>
            <label for="ac_password">DataStore Name:</label>
            <input type="text" tabindex="3" name="name" class="textinput">
            <p style="border-bottom:1px solid #EBEBEB; margin:2px 0 12px;"></p>

            <input type="text" tabindex="3" name="accessuser" class="textinput" value="Access UserName" style="color:#E9E9E9" onfocus="if(this.value=='Access UserName'){this.value=''}" onblur="if(this.value==''){this.value='Access UserName';}" />
            <input type="text" tabindex="3" name="accesspwd" class="textinput" value="Access PassWord" style="color:#E9E9E9" onfocus="if(this.value=='Access PassWord'){this.value=''}" onblur="if(this.value==''){this.value='Access PassWord';}" />
            
            <div class="aclogin-action" style="text-align:center">
              <input type="submit" tabindex="6" title="Login" name="action" value="Install &raquo;" class="acloginbttn ibutton">
              <div class="clearfix">&nbsp;</div>
            </div>
            </fieldset>
          </form>
        </div>
      </div>

</div>

<?php
	if( isset($_POST['action']) ) {
		//begin setup
		
		$handle = mysqli_init();
		$link = mysqli_real_connect( $handle, "localhost", $_POST['accessuser'], $_POST['accesspwd'], 'information_schema' );
		if( !mysqli_connect_errno() ) {
			//mysqli_query( $handle, 'DROP DATABASE IF EXISTS "'.$_POST['name'].'"');
			mysqli_query( $handle, 'CREATE DATABASE IF NOT EXISTS "'.$_POST['name'].'"');
			mysqli_select_db( $handle, $_POST['name']);
		} else {
			die("Could Not Connect To Mysql Database");
		}
//		$container = '';
//		
//		$mysql_table = "
//						CREATE TABLE `content` (
//						`pg_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
//						`pg_name` CHAR(50) NOT NULL,
//						`pg_title` CHAR(200) NOT NULL,
//						`pg_url` VARCHAR(500) NULL,
//						`pg_content` LONGTEXT NULL,
//						`pg_publish` CHAR(2) NOT NULL,
//						PRIMARY KEY (`pg_id`)
//						)
//						COLLATE='utf8_general_ci'
//						ENGINE=MyISAM;
//					  ";
		
	}
?>

</body>
</html>
<?php
	require_once dirname(__FILE__).'/config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
<style type="text/css">

table{ margin:5px auto;table-layout:fixed; font-size:12px; border:1px solid #eee; padding:0;}
table thead{
background: #eee;
background: -moz-linear-gradient(top, #FCFCFC 0%, #F2F2F2 32%, #E8E8E8 60%, #E3E8ED 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#FCFCFC), color-stop(32%,#F2F2F2), color-stop(60%,#E8E8E8), color-stop(100%,#E3E8ED));
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FCFCFC', endColorstr='#E3E8ED',GradientType=0 )
 }
table thead tr{  border:1px solid red; }
table thead tr th{padding:5px 0;}
table thead tr th span{ border-left:1px solid #ccc; padding:0 10px; float:left;}
table tbody td{  border-top:1px solid #f6f6f6; margin:0; padding:5px 10px 4px;}
.cm{ vertical-align:middle; background:#f9f9f9}
.cm{  text-align:center; padding-left:8px;}
table tbody tr:hover{ background:#F4F7FA}
table tbody tr.selected { background:#E8EDF4;}
</style>

</head>
<body>
<?php
	require_once("cms_basic/head.php");
	
	$run = DB::connect();
	$result = DB::load( $run, LANG );
?>
<div class="block center content" >
<p class="contentheader">Add New Language Support</p>

<?php

	if( !empty($result) )
	{

		echo '<div class="block cnt">';
		echo '<table width="98%" cellspacing="0" cellpadding="0" id="striped2" class="pickme"><thead><tr>',
				'<th width="5%"></th>',
				'<th width="10%"><span>ID</span></th>',
				'<th width="10%"><span>Short Name</span></th>',
				'<th width="55%"><span>Full Name</span></th>',				
				'<th width="20%"><span>Options</span></th>',			
			 '</tr></thead><tbody>';
		foreach( $result as $v ) 
		{
			echo '<tr>',
					'<td class="cm"><input type="checkbox" name="seek[]" disabled  /></td>',
					'<td>',$v['lang_id'],'</td>',
					'<td>',$v['lang_shortname'],'</td>',
					'<td>',$v['lang_fullname'],'</td>',					
					'<td><a href="delete.language.php?kill=',$v['lang_id'],'">Delete Language</a></td>',
				 '</tr>';
		}
		
		echo '</tbody></table></div>';
	
	}


?>

<form method="post" action="cms_function/new_language.php">
<div class="block cnt">

<ul class="block opadd" style="width:450px; margin:20px auto 10px;">
<li><label for="un">Language Short Name</label><input class="itext rw B" type="text" name="short" /></li>
<li><label for="un">Language Full Name</label><input class="itext rw B" type="text" name="full" /></li>
</ul>




</div>

<div class="block bottomplace">
<input type="submit" class="right ibutton" value="Create" />
<input type="reset" class="right ibutton"  value="Cancel" />
</div>
</form>
</div>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
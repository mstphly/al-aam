<?php
	global $CMS_PATH;
?>
<div id="header" class="block">
	<?php
	
	if( isset($_SESSION['_cms']) && !empty($_SESSION['_cms']) )
	{
		$appDir = CMS_PATH.'cms_apps/';

		echo <<<CMSAPPOPEN
		<ul id="cmsmenu" class="menu cms left block">
			<li><a href="#">APPS</a>
				<ul>
CMSAPPOPEN;
					
					if( is_dir($appDir) )
					{
						$dirHandle = opendir($appDir);
						while( ($folderName = readdir($dirHandle)) )
						{
							if( !in_array($folderName, array('.', '..')) )
							{
								echo '<li><a href="',$CMS_PATH,'cms_apps/',$folderName,'/">',$folderName,'</a>';
								if( $folderName == 'Platform Registration' )
								{
									echo '<ul>',
											'<li><a href="#">Academy 1 Business School</a></li>',
											'<li><a href="#">Brand Evolution Academy</a></li>',
											'<li><a href="#">Follow Your Dream Conference</a></li>',
											'<li><a href="#">Student Mentorship Program</a></li>',
											'<li><a href="#">World Class Business and Leadership Conference</a></li>',
											'<li><a href="#">Make Millions as an independent consultant</a></li>',
											'<li><a href="#">career Angels</a></li>',
										 '</ul>';
								}
								echo '</li>';
							}
						}
						closedir($dirHandle);
					}
		echo <<<CMSAPPCLOSE
		</ul>
			</li>
		</ul>
CMSAPPCLOSE;
	
	}
  
    ?>
    
    <div id="branding">
        <p id="name"><a id="home" href="#">Cms Manager</a></p>
    </div>
    <div id="actions" class="block">
        <ul class="action right block">
		
        <?php
		
            if( isset($_SESSION['_cms']) ) {
                echo <<<SIGNOUT
                        <li><a href="{$CMS_PATH}cms_function/auth.signout.php">LogOut</a></li>
SIGNOUT;
            } else {
				echo <<<LOGIN
                        <li><a href="{$CMS_PATH}login.php">Login</a></li>
LOGIN;
			}
        ?>
           
        </ul>
        
		<?php

        if( isset($_SESSION['_cms']) ) {

            echo <<<OPTIONS
            <ul class="action right block">
            <li><a href="{$CMS_PATH}setting.edit.php">Account Settings</a></li>
            </ul>

            <ul class="action right block">
			<li><a href="{$CMS_PATH}add.language.php">Add New Language</a></li>
            <li><a href="{$CMS_PATH}add.pagetype.php">Add Page Type</a></li>
			<li><a href="{$CMS_PATH}view.pages.php">View Pages</a></li>
            <li><a href="{$CMS_PATH}add_new.page.php">New Page</a><a href="{$CMS_PATH}add_new.subpage.php">New Sub-Page</a></li>
            </ul>
OPTIONS;

		}
		?>

    </div>
</div>
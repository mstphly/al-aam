

var UPLOADER = {
	frame : function (c) {
		var n,
		d,
		i;
		n = 'f' + Math.floor(Math.random() * 99999);
		d = document.createElement('DIV');
		d.innerHTML = '<iframe style="display:none" src="about:blank" id="' + n + '" name="' + n + '" onload="UPLOADER.loaded(\'' + n + '\')"></iframe>';
		document.body.appendChild(d);
		i = document.getElementById(n);
		if (c && typeof(c.onComplete) === 'function') {
			i.onComplete = c.onComplete;
		}
		return n;
	},
	form : function (f, name) {
		f.setAttribute('target', name);
	},
	submit : function (f, c) {
		UPLOADER.form(f, UPLOADER.frame(c));
		return (c && typeof(c.onStart) === 'function') ? c.onStart() : true;
	},
	loaded : function (id) {
		var d,
		i = document.getElementById(id);
		if (i.contentDocument) {
			d = i.contentDocument;
		} else if (i.contentWindow) {
			d = i.contentWindow.document;
		} else {
			d = window.frames[id].document;
		}

		if (d.location.href === "about:blank") {
			return;
		}
		if (typeof(i.onComplete) === 'function') {
			i.onComplete(d.body.innerHTML);
		}
	}
};

function Sack(file) {
    this.xmlhttp = null;
    var encoded,
    regexp,
    varArray,
    urlVars,
    totalurlstring,
    elemNodeName,
    urlstringtemp,
    key = '';
    this.resetData = function() {
        this.method = "POST";
        this.queryStringSeparator = "?";
        this.argumentSeparator = "&";
        this.URLString = "";
        this.encodeURIString = true;
        this.execute = false;
        this.element = null;
        this.elementObj = null;
        this.requestFile = file;
        this.vars = new Object();
        this.responseStatus = new Array(2);
    };
    this.resetFunctions = function() {
        this.onLoading = function() {};
        this.onLoaded = function() {};
        this.onInteractive = function() {};
        this.onCompletion = function() {};
        this.onError = function() {};
        this.onFail = function() {};
    };
    this.reset = function() {
        this.resetFunctions();
        this.resetData();
    };
    this.createAJAX = function() {
        try {
            var XmlHttpVersions = ["MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.5.0", "MSXML2.XMLHTTP.4.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"];
            var hz = XmlHttpVersions.length;
            for (var wp = 0; wp < hz; wp++) {
                this.xmlhttp = new ActiveXObject(XmlHttpVersions[wp]);
            }
        } catch(e1) {
            try {
                this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e2) {
                this.xmlhttp = null;
            }
        }
        if (!this.xmlhttp) {
			(typeof XMLHttpRequest != "undefined") ?this.xmlhttp = new XMLHttpRequest() : this.failed = true;
        }
    };
    this.setVar = function(name, value) {
        this.vars[name] = Array(value, false);
    };
    this.encVar = function(name, value, returnvars) {
		return (true === returnvars) ? Array(encodeURIComponent(name), encodeURIComponent(value)) : this.vars[encodeURIComponent(name)] = Array(encodeURIComponent(value), true);
    };
    this.processURLString = function(string, encode) {
        encoded = encodeURIComponent(this.argumentSeparator);
        regexp = new RegExp(this.argumentSeparator + "|" + encoded);
        varArray = string.split(regexp);
        var jj = varArray.length;
        for (var i = 0; i < jj; i++) {
            urlVars = varArray[i].split("=");
			(true === encode) ? this.encVar(urlVars[0], urlVars[1]) : this.setVar(urlVars[0], urlVars[1]);
        }
    };
    this.createURLString = function(urlstring) {
        if (this.encodeURIString && this.URLString.length) {
            this.processURLString(this.URLString, true);
        }
        if (urlstring) {
			
			(this.URLString.length) ? this.URLString += this.argumentSeparator + urlstring : this.URLString = urlstring;
        }
        this.setVar("recycler", new Date().getTime());
        urlstringtemp = [];
        for (key in this.vars) {
            if (false === this.vars[key][1] && true === this.encodeURIString) {
                encoded = this.encVar(key, this.vars[key][0], true);
                delete this.vars[key];
                this.vars[encoded[0]] = Array(encoded[1], true);
                key = encoded[0];
            }
            urlstringtemp[urlstringtemp.length] = key + "=" + this.vars[key][0];
        }
		this.URLString += (urlstring)?this.argumentSeparator + urlstringtemp.join(this.argumentSeparator) : urlstringtemp.join(this.argumentSeparator);
    };
    this.runResponse = function() {
        jP(this.response);
    };
    this.runAJAX = function(urlstring) {
        if (this.failed) {
            this.onFail();
        } else {
            this.createURLString(urlstring);
            if (this.element) {
                this.elementObj = xId(this.element);
            }
            if (this.xmlhttp) {
                var self = this;
                if (this.method == "GET") {
                    totalurlstring = this.requestFile + this.queryStringSeparator + this.URLString;
                    this.xmlhttp.open(this.method, totalurlstring, true);
                } else {
                    this.xmlhttp.open(this.method, this.requestFile, true);
                    try {
                        this.xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    } catch(e) {}
                }
                this.xmlhttp.onreadystatechange = function() {
                    switch (self.xmlhttp.readyState) {
                    case 1:
                        self.onLoading();
                        break;
                    case 2:
                        self.onLoaded();
                        break;
                    case 3:
                        self.onInteractive();
                        break;
                    case 4:
                        self.response = self.xmlhttp.responseText;
                        self.responseXML = self.xmlhttp.responseXML;
                        self.responseStatus[0] = self.xmlhttp.status;
                        self.responseStatus[1] = self.xmlhttp.statusText;
                        if (self.execute) {
                            self.runResponse();
                        }
                        if (self.elementObj) {
                            elemNodeName = self.elementObj.nodeName;
                            elemNodeName.toLowerCase();
                           (elemNodeName == "input" || elemNodeName == "select" || elemNodeName == "option" || elemNodeName == "textarea")?self.elementObj.value = self.response : self.elementObj.innerHTML = self.response;
                        }
						
						(self.responseStatus[0] == "200") ? self.onCompletion() : self.onError();
                        self.URLString = "";
                        break;
                    }
                };
                this.xmlhttp.send(this.URLString);
            }
        }
    };
    this.reset();
    this.createAJAX();
}

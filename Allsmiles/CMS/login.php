<?php
	require_once dirname(__FILE__).'/config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
<style type="text/css">
#acloginpod{background:url(cms_img/acloginpodbg.gif) repeat-x scroll 0 0 #EBEBEB;border:1px solid #D3D3D3;border-radius:7px 7px 7px 7px;width:320px;}
.acloginform{margin:22px;}
fieldset,form{margin:0!important;padding:0!important;}
#acloginpod label{color:#444;display:block;font-size:15px;margin-bottom:3px;}
#acloginpod input.textinput, #acloginpod select.textinput{background:url(cms_img/textinputbg.gif) repeat-x scroll 0 0 #FFF;border:1px solid #D3D3D3;color:#000;font-size:15px;margin-bottom:10px;text-indent:7px;width:100%;font-family:arial;padding:7px 0;}
.fl-left{float:left;}
#acloginpod .acloginbttn{float:right;margin-top:10px;width:121px;}
.aclogin-action label{border-bottom:1px solid #D3D3D3;color:#777;font-size:12px;margin:0 0 7px;padding:0 0 5px;}
#acloginpod a.forgotpass{color:#AAA;display:block;font-size:12px;}
#acloginpod legend em{left:-9999em;position:absolute;}
a{color:#005E8F;text-decoration:none;}
</style>
</head>

<body>
<?php
	
	require_once("cms_basic/head.php");
	
	$langSupport = '';
	$run = DB::connect();
	$result = DB::load( $run, LANG );
	
	if( !empty($result) )
	{
		foreach( $result as $v )
		{
			$langSupport .= '<option value="'.$v['lang_shortname'].'">'.$v['lang_fullname'].'</option>';
		}
	}
?>

<div id="content">    
	
    <div id="acloginpod" style="margin:50px auto;">
        <div class="acloginform">
          <form method="post" action="cms_function/access_perm.php">
            <input type="hidden" value="<?php echo isset($_GET['redirect']) ? $_GET['redirect'] : 'index.php'; ?>" name="redirect" />
            <fieldset>
            <legend><em>Enter your Account Center login information below</em></legend>
            <label for="primary_email">Email:</label>
            <input type="text" tabindex="2" name="ac_email" class="textinput" />
            <label for="ac_password">Password:</label>
            <input type="password" tabindex="3" name="ac_password" class="textinput" />
			<label for="ac_password">Language Support:</label>
			<select name="lang" class="textinput">
				<option>Default</option>
				<?php echo $langSupport; ?>
			</select>
            <div class="aclogin-action">
              <div class="fl-left">
                <label for="remember_me">
                <input type="checkbox" tabindex="4" name="rememberme" id="remember_me" />
                Remember me</label>
                <a tabindex="5" href="#" class="forgotpass">Forgot
                your password?</a> </div>
              <input type="submit" tabindex="6" title="Login" value="Access >>" class="acloginbttn ibutton" />
              <div class="clearfix">&nbsp;</div>
            </div>
            </fieldset>
          </form>
        </div>
      </div>

</div>
<?php
	require_once 'cms_basic/foot.php';
?>
</body>
</html>
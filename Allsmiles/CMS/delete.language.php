<?php

	if( isset($_GET['kill']) && !empty($_GET['kill']) )
	{
		
		require_once './config.php';
				
		$link = DB::connect();
		$delete = DB::delete($link, LANG, array('lang_id'=>$_GET['kill']), 1);
		
		if( $delete>0 ) 
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: add.language.php?info=1');
			exit;
		} else
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location:add.language.php?info=0');
			exit;
		}
	
	}

?>
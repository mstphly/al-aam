<?php
	require_once dirname(__FILE__).'/config.php';
	if( !isset($_GET['edit']) )
		die;		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Manager</title>
<link type="text/css" rel="stylesheet" href="cms_css/style.css" />
</head>
<body>
<?php require_once("cms_basic/head.php") ?>
<div class="block center content" >
<p class="contentheader">Edit Sub-Page</p>

<form method="post" action="cms_function/edit.complete.php?sub=<?php echo $_GET['go'] ?>">
<div class="block cnt">

<?php

	$parameters = explode(',', $_GET['edit']);

	$link = DB::connect();
	$rok = DB::load( $link, TABLE2, '*', array('spg_id'=>$parameters) );
	
	if( $rok>0 ) {
		foreach( $rok as $v ) {
			
			$yes = ''; $no = '';
			if( $v['spg_publish']=='YES')
				$yes = 'selected';
			else
				$no = 'selected';
				
			
		echo <<<EDITSUBPAGE
				<ul class="block opadd" style="width:450px; margin:20px auto 10px;">
				<li><label for="un">Page Name</label><input class="itext rw B" type="text" name="name[{$v['spg_id']}]" value="{$v['spg_name']}" /></li>
				<li><label for="pwd">Page Title</label><input class="itext rw" type="text" name="title[{$v['spg_id']}]" value="{$v['spg_title']}" /></li>
				<li><label for="pwd">Page Url</label><input class="itext rw" type="text" name="url[{$v['spg_id']}]" value="{$v['spg_url']}" /></li>
				<li><label for="pwd">Publish Page</label><select class="iselect" name="publish[{$v['spg_id']}]"><option {$yes}>YES</option><option {$no}>NO</option></select></li>
				</ul>
EDITSUBPAGE;
		}
	}

?>


</div>

<div class="block bottomplace">
<input type="Submit" class="right ibutton" value="Save Changes" />
<input type="reset" class="right ibutton" value="Cancel" />
</div>
</form>
</div>
<?php require_once 'cms_basic/foot.php'; ?>
</body>
</html>
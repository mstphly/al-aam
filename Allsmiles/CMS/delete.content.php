<?php

	if( isset($_GET['kill']) && !empty($_GET['kill']) ) {
		
		require_once './config.php';
				
		$link = DB::connect();
		$delete = DB::delete($link, STORE, array('cid'=>$_GET['kill'], 'pg_id'=>$_GET['return']), 1);
		
		if( $delete>0 ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: view.page_content.php?info=1&view='.$_GET['return'].'');
			exit;
		}
		
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: view.page_content.php?info=0&view='.$_GET['return'].'');
	
	}

?>
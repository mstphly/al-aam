<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
abstract class MDB {

	private function __construct(){}
	private function __clone(){}
	
	public static function connect( $host = HOST , $db = DBNAME, $user = USER, $pass = PASS ) {
		
		$handle = mysqli_init();
		$link = mysqli_real_connect( $handle, $host, $user, $pass, $db );
		if( mysqli_connect_errno() )
			return self::log_error( mysqli_connect_error().' with error info: '.mysqli_error() );

		return $handle;
		
	}
	
	/*
	 #  col = represent the columns
	 #	value = represent value could perform multiple insert by using multiple array
	 #	link = database handle
	 #	table = table name
	 */
	public static function insert( $link, $table, $cols, $value ) {
		
		$block = ' (';
		if( is_array($cols) ) {
			foreach( $cols as $k=>$v ) {
				$block .= $v.',';
			}
			$block = substr($block, 0, -1);
		}
		$block .= ') VALUES ';
		
		#trick to checking for multi-array
		if( is_array($value[0]) ) {
			
			foreach( $value as $v ) {
				
				$block .= ' (';
				foreach( $v as $b ) {
					#clean user data here
					$block .= '"'.$b.'",';
				}
				$block = substr($block,0,-1);
				$block .= ' ),';
			
			}
			$block = substr($block, 0, -1);
			
		} else {
			
			$block .= ' (';
			foreach( $value as $b ) {
				#clean user data here
				$block .= '"'.$b.'",';
			}
			$block = substr($block,0,-1);
			$block .= ' )';
		
		}
		
		$link->query( 'INSERT INTO '.$table.$block );
		return $link->affected_rows;
		
	}

	/*
	 #  limit = default:false -> unlimited
	 #	condition = an assoc array of condition to check for
	 #	link = database handle
	 #	table = table name
	 */	
	public static function delete( $link, $table, $condition=false, $limit=false ) {
		
		$block = '';
		if( $condition !== false ) {
			$block .= ' WHERE';
			foreach( $condition as $k=>$v ) {
			
				if( is_array($v) )
					$block .= ' '.$k.' IN (\''.implode("','", $v).'\') &&';
				else
					$block .= ' '.$k.'="'.$v.'" &&';
			
			}
			$block = substr($block,0,-2);
		}
		
		if( $limit !== false )
			$block .= ' LIMIT '.(int)$limit;
		
		$link->query( 'DELETE FROM '.$table.$block );
		return $link->affected_rows;
		
	}
	
	/*
	 #  limit = default:false -> unlimited
	 #	retreive = columns to retreive
	 #	condition = an assoc array of condition to check for
	 #	link = database handle
	 #	table = table name
	 */	
	public static function load( $link, $table, $retreive=false, $condition=false, $limit=false ) {
		
		$block = '';
		if( $retreive !== false ) {
			foreach( $retreive as $column ) {
				$block .= $column.',';
			}
			$block = substr($block,0,-1);
		} else
			$block .= ' *';
		
		$block .= ' FROM '.$table.' ';
			
		if( $condition !== false ) {
			$block .= ' WHERE';
			foreach( $condition as $k=>$v ) {
				
				if( is_array($v) )
					$block .= ' '.$k.' IN (\''.implode("','", $v).'\') &&';
				else
					$block .= ' '.$k.'="'.$v.'" &&';
			}
			$block = substr($block,0,-2);
		}
		
		if( $limit !== false )
			$block .= ' LIMIT '.(int)$limit;
		
		$run = $link->query( 'SELECT'.$block ) or die('ERROR');
		return self::loadAll($run);
		
	}

	/*
	 #  limit = default:false -> unlimited
	 #	updatelist = columns to retreive
	 #	condition = an assoc array of condition to check for
	 #	link = database handle
	 #	table = table name
	 */		
	public static function update( $link, $table, $updatelist=array(), $condition=false, $limit=false ) {
		
		$block = '';
		foreach( $updatelist as $k=>$v ) {
			$block .= $k.'="'.$v.'",';
		}
		$block = substr($block,0,-1);
		
		if( $condition !== false ) {
			$block .= ' WHERE';
			foreach( $condition as $k=>$v ) {
				
				if( is_array($v) )
					$block .= ' '.$k.' IN (\''.implode("','", $v).'\') &&';
				else
					$block .= ' '.$k.'="'.$v.'" &&';
			}
			$block = substr($block,0,-2);			
		}
		
		if( $limit !== false )
			$block .= ' LIMIT '.(int)$limit;
			
		$run = $link->query( 'UPDATE '.$table.' SET '.$block );
		return $link->affected_rows;		
		
	}
	
	/*
	 #	link = This is the connection resource used for the pre-action that caused error
	 */		
	public static function error( $link ) {
		return mysqli_error( $link );
	}
	
	
	private static function loadAll( $result ) {
		
		$return = array();
		if( $result ) {
			while( $f = mysqli_fetch_array($result) ) {
				$return[] = $f;
			}
		}
		return $return;
	
	}

	
}

?>
<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 *  	Version 1.3
 */

// for this project only

class DB
{
	
	const TABLE_EXT = '.ACE';
	
	const RELEASE_MODE = 10;
	const DEBUG_MODE = 5;
	
	protected static $_mode = self::RELEASE_MODE;
	
	private static $lastinsert_id = false;
	private static $found_rows = false;
	private static $affected_rows = false;
	
	
    private function __construct(){}
    private function __clone(){}

	
	/*
	 *	Variable $Link
	 *	This is The Folder Path of Where Your FDB is located In
	 *	Return: Is also the specified path
	 */
    public static function connect( $link = FDB, $mode = self::DEBUG_MODE ) {
		
		if( self::DEBUG_MODE === $mode )
			error_reporting(-1);
		else {
			error_reporting(0);
			self::$_mode = self::RELEASE_MODE;
		}
		return is_dir($link) ? $link : dirname($link);
		
    }
	
	
	
	/*
	 *	This Function Creates a new table structure with the file
	 *
	 *
	 */
	public static function create( $link, $table_name, $cols ) {
		
		if( is_array($cols) ) {
			$build_table = '[architect]';
			foreach( $cols as $k=>$v ) {
				$build_table.= "\n".'cols[] = \''.$v.'\'';
			}
			file_put_contents( $link.'/'.$table_name, '' );
			return file_put_contents( $link.'/'.$table_name.self::TABLE_EXT, $build_table );
		}
			trigger_error('3rd parameter must be in array format', E_USER_ERROR);
		
	}	
	
	
    /*\
     #  col = represent the columns
     #	value = represent value could perform multiple insert by using multiple array
     #	link = database handle
     #	table = table name
    \*/
    public static function insert( $link, $table_name, $cols, $data ) {
        
		$affected_rows = 0;
				
        if( is_file($link.'/'.$table_name) )	{
            $content = unserialize( file_get_contents($link.'/'.$table_name) );
			$content = empty($content) ? array('increment'=>0) : $content;
		} else
            $content = array('increment'=>0);
		
		$table_structure = parse_ini_file( $link.'/'.$table_name.self::TABLE_EXT );
		
		/* Check Against Debug Mode */
		if( self::DEBUG_MODE === self::$_mode )
			self::val_col($table_structure, $cols);
		
		$table_structure = self::table_struct( $table_structure );
		

        if( is_array($data[0]) ) {
			// multi-insert
			
			for( $i=0, $size=count($data); $i<$size; $i++ ) {
				
				$affected_rows++;
				$content['increment']++;
				
				/*
				 * newrow = columns
				 */
				$new_row = $table_structure;
				if( $new_row['auto_increment'] )
					$new_row[ $new_row['auto_increment'] ] = $content['increment'];
				
				foreach( $cols as $k=>$v )
					$new_row[$v] = $data[$i][$k];
				
				//unset evry present table property
				unset( $new_row['auto_increment'] );
				
				$content[] = $new_row;
				self::$lastinsert_id = $content['increment'];
			
			}
			
        } else {

            $affected_rows++;
			$content['increment']++;
            $new_row = $table_structure;

			if( isset($new_row['auto_increment']) )
				$new_row[ $new_row['auto_increment'] ] = $content['increment'];			
			
			foreach( $cols as $k=>$v )
                $new_row[$v] = $data[$k];
			
			//unset evry present table property
			unset( $new_row['auto_increment'] );
			
			$content[] = $new_row;
			self::$lastinsert_id = $content['increment'];
        }

        file_put_contents( $link.'/'.$table_name, serialize($content) );
        return self::$affected_rows = $affected_rows;

    }

    /*
     #  limit = default:false -> unlimited
     #	condition = an assoc array of condition to check for
     #	link = database handle
     #	table = table name
     */
    public static function delete( $link, $table_name, $condition='*', $limit=false ) {
		
		if( self::DEBUG_MODE === self::$_mode ) {
			
			$table_structure = parse_ini_file( $link.'/'.$table_name.self::TABLE_EXT );			
			if( $condition != '*' ) {
				$cond_cols = array_keys($condition);
				self::val_col($table_structure, $cond_cols);
			}
			
			if( $limit && !is_int($limit) ) {
				trigger_error('4th parameter expects Integer, String given ', E_USER_ERROR);
			}
		
		}
		
		
        $affected_rows = 0;
        if( is_file($link.'/'.$table_name) ) {
            $content = unserialize( file_get_contents($link.'/'.$table_name) );
            $prop = array_shift($content);
			
            if( $condition !== '*' ) {
				
				
				// to reduce no of call to lcase method
				$tempContent = self::lcase($content);
				$tempCondition = self::lcase($condition);				

				foreach( $content as $ck=>$vv ) {
					
					/* NEW UPDATE */
						$whereReturn = true;
						foreach( /*$condition*/ $tempCondition as $whereKey=>$whereValue )
						{
							
							if( false === $whereReturn )
								break;
							
							if( is_array($whereValue) ) // like IN search in mysql
							{
								
								//$whereReturn = in_array( self::lcase( $vv[$whereKey] ), self::lcase($whereValue) ) ? true : false;
								$whereReturn = in_array( $tempContent[$ck][$whereKey], $whereValue ) ? true : false;
								
							} else
							{
								
								//$whereReturn = self::lcase( $vv[$whereKey] ) == self::lcase( $whereValue ) ? true : false;
								$whereReturn = $tempContent[$ck][$whereKey] == $whereValue ? true : false;
									
							}
							
						}
						
						// IF WHERE RETURNS TRUE
						if( true === $whereReturn )
						{
							$affected_rows++;
							unset( $content[$ck] );														
						}
						
					/* NEW UPDATE */
									
					if( $limit && $affected_rows==$limit )
						break;					
				
				}

				$content = array_reverse($content);
				$content['increment'] = $prop;
				$content = array_reverse($content);
				file_put_contents( $link.'/'.$table_name, serialize($content) );

            } else {
				
				$affected_rows = count( $content );
				file_put_contents( $link.'/'.$table_name, '' );
			
			}

            return self::$affected_rows = $affected_rows;

        }
        return self::$affected_rows = 0;

    }
	
	

    /*
     #  limit = default:false -> unlimited
     #	retreive = columns to retreive
     #	condition = an assoc array of condition to check for
     #	link = database handle
     #	table = table name
     */
    public static function load( $link, $table_name, $retreive_cols='*', $condition=false, $limit=false, $aggregateFunc=false ) {

		if( self::DEBUG_MODE === self::$_mode ) {
			
			$table_structure = parse_ini_file( $link.'/'.$table_name.self::TABLE_EXT );
			if( $retreive_cols !== '*' ) {
				self::val_col($table_structure, $retreive_cols);
			}
			
			if( $condition ) {
				$cond_cols = array_keys($condition);
				self::val_col($table_structure, $cond_cols);
			}
			
			if( $limit && ( !is_array($limit) || !is_nan($limit) ) )
			{
				trigger_error('5th parameter expects Integer or Array, String given ', E_USER_ERROR);
			}elseif( is_array($limit) && count($limit) != 2  )
			{
				trigger_error('5th parameter expects array size to be 2');
			}
		
		}
		

		/* NEW LIMIT IMPLEMENTATION */
			$limit_start = 0;
			$limit_end = false;
			if( is_array($limit) )
			{
				$limit_start = $limit[0];
				$limit_end = $limit[1];
			} else
			{
				$limit_end = $limit ? $limit : false;
			}
		/* NEW LIMIT IMPLEMENTATION */
			

        if( is_file($link.'/'.$table_name) ) {
            $content = unserialize( file_get_contents($link.'/'.$table_name) );

            if( empty($content) )
                return 0;

            $found = array();
            $prop = array_shift($content);
			
			$aggregateActive = false;
			$aggregateCalc = false;
			
			if( $aggregateFunc )
			{
				
				switch( $aggregateFunc )
				{
					case 'count':
						$aggregateActive = 'count';
						if( false === $condition )
						{
							if( false === $limit_end )
							{
								return count($content);
							} else
							{
								$slice = array_slice( $content, $limit_start, $limit_end );
								return count( $slice );
							}
						}
					break;
				}
				
			}
	
			
			$limit_chk = 0;
			$limit_startchk = 0;
			
			// to reduce no of call to lcase method
			$tempContent = self::lcase($content);
			$tempCondition = self::lcase($condition);
			
            foreach( $content as $kk=>$vv ) {
				
                if( false === $condition ) {
					
					$limit_startchk++;
					if( $limit_startchk <= $limit_start )
					{
						continue;
					}
					
					if( $retreive_cols !== '*' ) {
						
						foreach( $vv as $lkk=>$lvv ) {
							if( !in_array($lkk, $retreive_cols) )
								unset( $vv[$lkk] );
						}
						
					}
					
					if( $aggregateFunc )
					{
						switch( $aggregateFunc )
						{
							case 'count':
								$aggregateCalc += 1;
								$found = $aggregateCalc;
							break;
						}
					} else
					{
						$found[] = $vv;
					}
					
					// Tick Limit
					$limit_chk++;
					
                } else {
					
					/* NEW UPDATE */
						$whereReturn = true;
						
						foreach( /*$condition*/$tempCondition as $whereKey=>$whereValue )
						{
							
							if( false === $whereReturn )
								break;
							
							if( is_array($whereValue) ) // like IN search in mysql
							{
								
								//$whereReturn = in_array( self::lcase( $vv[$whereKey] ), self::lcase($whereValue) ) ? true : false;
								$whereReturn = in_array( $tempContent[$kk][$whereKey], $whereValue ) ? true : false;
								
							} else
							{
								
								//$whereReturn = self::lcase( $vv[$whereKey] ) == self::lcase( $whereValue ) ? true : false;
								$whereReturn = $tempContent[$kk][$whereKey] == $whereValue ? true : false;
									
							}
							
						}
						
						// IF WHERE RETURNS TRUE
						if( true === $whereReturn )
						{
							
							$limit_startchk++;
							if( $limit_startchk <= $limit_start )
							{
								continue;
							}							
							
	
							if( $aggregateFunc )
							{
								switch( $aggregateFunc )
								{
									case 'count':
										$aggregateCalc += 1;
										$found = $aggregateCalc;
									break;
								}								
							} else
							{
								if( $retreive_cols !== '*' ) {
									
									foreach( $vv as $lkk=>$lvv ) {
										if( !in_array($lkk, $retreive_cols) )
											unset( $vv[$lkk] );
									}
																
								}
								$found[] = $vv;
							}
							
							// Tick limit
							$limit_chk++;
							
						}
					/* NEW UPDATE */					
					
                }
				
				if( $limit_end && $limit_chk == $limit_end )
					break;
				
            }
			self::$found_rows = count( $found );
			return $found ? $found : 0;
        }

    }

    /*
     #  limit = default:false -> unlimited
     #	updatelist = columns to retreive
     #	condition = an assoc array of condition to check for
     #	link = database handle
     #	table = table name
     */
    public static function update( $link, $table_name, $update_list=array(), $condition=false, $limit=false ) {
		
		if( self::DEBUG_MODE === self::$_mode ) {
			
			$table_structure = parse_ini_file( $link.'/'.$table_name.self::TABLE_EXT );
			
			if( !empty($update_list) ) {
				$upd_cols = array_keys($update_list);
				self::val_col($table_structure, $upd_cols);
			}
			
			if( $condition ) {
				$cond_cols = array_keys($condition);
				self::val_col($table_structure, $cond_cols);
			}
			
			if( $limit && !is_int($limit) ) {
				trigger_error('5th parameter expects Integer, String given ', E_USER_ERROR);
			}
						
		}
		
		
        $affected_rows = 0;
        if( is_file($link.'/'.$table_name) ) {
            $content = unserialize( file_get_contents($link.'/'.$table_name) );
            $prop = array_shift($content);

            if( false===$condition ) {
				
				foreach( $content as $ck=>$cv ) {
					
					$affected_rows++;
					foreach( $update_list as $uk=>$uv )
						$content[$ck][$uk] = $uv;

					if( $limit && $affected_rows==$limit )
						break;						

				}

            } else {
				
				// To reduce the no of call to lcase method
				$tempContent = self::lcase($content);
				$tempCondition = self::lcase($condition);
				
				foreach( $content as $ck=>$vv ) {
	
					
					/* NEW UPDATE */
						$whereReturn = true;
						foreach( /*$condition*/$tempCondition as $whereKey=>$whereValue )
						{
							
							if( false === $whereReturn )
								break;
							
							if( is_array($whereValue) ) // like IN search in mysql
							{
								
								//$whereReturn = in_array( self::lcase( $vv[$whereKey] ), self::lcase($whereValue) ) ? true : false;
								$whereReturn = in_array( $tempContent[$ck][$whereKey], $whereValue ) ? true : false;
								
							} else
							{
								
								//$whereReturn = self::lcase( $vv[$whereKey] ) == self::lcase( $whereValue ) ? true : false;
								$whereReturn = $tempContent[$ck][$whereKey] == $whereValue ? true : false;	
							
							}
							
						}
						
						// IF WHERE RETURNS TRUE
						if( true === $whereReturn )
						{

							$affected_rows++;
							foreach( $update_list as $uk=>$uv )
								$content[$ck][$uk] = $uv;
							
						}
						
					/* NEW UPDATE */
					
					if( $limit && $affected_rows==$limit )
						break;						
				
				}

            }

            $content = array_reverse($content);
            $content['increment'] = $prop;
            $content = array_reverse($content);
            file_put_contents( $link.'/'.$table_name, serialize($content) );

            return self::$affected_rows = $affected_rows;

        }
        return self::$affected_rows = 0;

    }
	
	
	
	public static function lastinsert_id() {
		return self::$lastinsert_id;
	}
	
	public static function affected_rows() {
		return self::$affected_rows;
	}
	
	public static function num_rows() {
		return self::$found_rows;
	}
	
	
	
	
	private static function lcase( $str ) {
		if( is_array($str) )
			return function_exists('mb_strtolower') ? unserialize(mb_strtolower(serialize($str))) : unserialize(strtolower(serialize($str)));
		
		return function_exists('mb_strtolower') ? mb_strtolower($str) : strtolower($str); 

/*
		if( is_array($str) )
			return function_exists('mb_strtolower') ? array_map('mb_strtolower', $str) : array_map('strtolower', $str);
			
		return function_exists('mb_strtolower') ? mb_strtolower($str) : strtolower($str);
*/
	}
	
	private static function table_struct( $table_struct ) {
		$r = '';
		foreach( $table_struct['cols'] as $k=>$v ) {
			if( false !== strpos($v, ':') ) {
				
				$vs = explode(':', $v);
				$r[ $vs[0] ] = '';
				$r[ $vs[1] ] = $vs[0];
			} else
				$r[$v] = '';
		}
		
		return $r;
	}
	
	
	private static function val_col( $table_struct, $cols ) {
	
		$ls = array();
		foreach( $table_struct['cols'] as $k=>$v ) {
			if( false !== strpos($v, ':') ) {
				list($o1) = explode(':', $v);
				$ls[] = $o1;
				continue;
			}
				$ls[] = $v;
		}
		
		foreach( $cols as $k=>$v ) {
			if( !in_array($v, $ls) )
				trigger_error('Column {'.$v.'} does not exist in table', E_USER_ERROR);
		}
			return;
		
	}


}

//		table structure
//		array(
//		'increment'=>0,
//		 array(
//					'pg_id'=>'',
//					'pg_name'=>'',
//					'pg_title'=>'',
//					'pg_url'=>'',
//					'pg_publish'=>'')
//		);

?>

<?php

/*
 *	All Thanks to Almighty God
 *	For a Peace and Love Handwriting
 *	Author: Ojutiku Oluwatobi
 */
 
abstract class DB {

	private function __construct(){}
	private function __clone(){}
	
	public static function connect( $host = HOST, $db = DBNAME, $user = USER, $pass = PASS ) {
		
		$handle = new PDO('sqlite:'.$db, $user, $pass, array(PDO::ATTR_PERSISTENT => true));
		$handle->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);
		$handle->exec('PRAGMA synchronous=OFF;PRAGMA count_changes=OFF');

		return $handle;
		
	}
	
	/*
	 #  col = represent the columns
	 #	value = represent value could perform multiple insert by using multiple array
	 #	link = database handle
	 #	table = table name
	 */
	public static function insert( $link, $table, $cols, $value ) {
		
		$block = ' (';
		if( is_array($cols) ) {
			foreach( $cols as $k=>$v ) {
				$block .= $v.',';
			}
			$block = substr($block, 0, -1);
		}
		$block .= ') VALUES ';
		
		#trick to checking for multi-array
		if( is_array($value[0]) ) {
			
			foreach( $value as $v ) {
				
				$block .= ' (';
				foreach( $v as $b ) {
					#clean user data here
					$block .= '"'.$b.'",';
				}
				$block = substr($block,0,-1);
				$block .= ' ),';
			
			}
			$block = substr($block, 0, -1);
			
		} else {
			
			$block .= ' (';
			foreach( $value as $b ) {
				#clean user data here
				$block .= '"'.$b.'",';
			}
			$block = substr($block,0,-1);
			$block .= ' )';
		
		}
		
		$link->query( 'INSERT INTO '.$table.$block );
		return $link->rowCount();
		
	}

	/*
	 #  limit = default:false -> unlimited
	 #	condition = an assoc array of condition to check for
	 #	link = database handle
	 #	table = table name
	 */	
	public static function delete( $link, $table, $condition=false, $limit=false ) {
		
		$block = '';
		if( $condition !== false ) {
			$block .= ' WHERE';
			foreach( $condition as $k=>$v ) {
			
				if( is_array($v) )
					$block .= ' '.$k.' IN (\''.implode("','", $v).'\') &&';
				else
					$block .= ' '.$k.'="'.$v.'" &&';
			
			}
			$block = substr($block,0,-2);
		}
		
		if( $limit !== false )
			$block .= ' LIMIT '.(int)$limit;
		
		$link->query( 'DELETE FROM '.$table.$block );
		return $link->rowCount();
		
	}

	/*
	 #  limit = default:false -> unlimited
	 #	retreive = columns to retreive
	 #	condition = an assoc array of condition to check for
	 #	link = database handle
	 #	table = table name
	 */	
	public static function load( $link, $table, $retreive=false, $condition=false, $limit=false ) {
		
		$block = '';
		if( $retreive !== false ) {
			foreach( $retreive as $column ) {
				$block .= $column.',';
			}
			$block = substr($block,0,-1);
		} else
			$block .= ' *';
		
		$block .= ' FROM '.$table.' ';
			
		if( $condition !== false ) {
			$block .= ' WHERE';
			foreach( $condition as $k=>$v ) {
				
				if( is_array($v) )
					$block .= ' '.$k.' IN (\''.implode("','", $v).'\') &&';
				else
					$block .= ' '.$k.'="'.$v.'" &&';
			}
			$block = substr($block,0,-2);
		}
		
		if( $limit !== false )
			$block .= ' LIMIT '.(int)$limit;
		
		$run = $link->query( 'SELECT'.$block ) or die('ERROR');
		return $run->fetchAll();
		
	}

	/*
	 #  limit = default:false -> unlimited
	 #	updatelist = columns to retreive
	 #	condition = an assoc array of condition to check for
	 #	link = database handle
	 #	table = table name
	 */		
	public static function update( $link, $table, $updatelist=array(), $condition=false, $limit=false ) {
		
		$block = '';
		foreach( $updatelist as $k=>$v ) {
			$block .= $k.'="'.$v.'",';
		}
		$block = substr($block,0,-1);
		
		if( $condition !== false ) {
			$block .= ' WHERE';
			foreach( $condition as $k=>$v ) {
				
				if( is_array($v) )
					$block .= ' '.$k.' IN (\''.implode("','", $v).'\') &&';
				else
					$block .= ' '.$k.'="'.$v.'" &&';
			}
			$block = substr($block,0,-2);			
		}
		
		if( $limit !== false )
			$block .= ' LIMIT '.(int)$limit;
			
		$run = $link->query( 'UPDATE '.$table.' SET '.$block );
		return $link->rowCount();		
		
	}
	
	/*
	 #	link = This is the connection resource used for the pre-action that caused error
	 */		
	public static function error( $link ) {
		return $link->errorInfo();
	}
	
	
}

?>
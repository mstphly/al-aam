<?php

	require_once('Components/class.siteGuard.php');
	
	
	$bckp_folder = $_SERVER['DOCUMENT_ROOT'].'project.portal/REST/';//'E:/myphpbackup/';                # where to save gzipped files copy
	$root_folder = array($_SERVER['DOCUMENT_ROOT'].'project.portal/');//array('E:/myphp1/','E:/myphp2/'); # folders to be monitored
	
	$guard = new CSitePagesGuard($root_folder,
	  array('backupfolder'=>$bckp_folder)
	);
	$guard->AddFileExtension('php'); # I'm using special file extension for my scripts
	
	$result = $guard->RegisterAllFiles(); # initially register all files and make backup copy
	echo $result;

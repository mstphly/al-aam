<?php

	$_POST = array_map('trim', $_POST);	

	$is_valid = VALIDATION::m_valid( 
	
		array(
			array( 'v'=>$_POST['username'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['initials'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['firstname'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['lastname'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['birthday'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['telephoneno'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['address1'], 'typ'=>'emp' ),			
			array( 'v'=>$_POST['city'], 'typ'=>'emp' ),												
		) 
	
	);
	

	
	
	if( false === $is_valid )
	{
		echo( json_encode(array("validation-error")) );
		exit();
	}
	
	

	// Fresh Registration
	$imgName = '';
	$profileQuery = '';
	if( $_FILES['image']['name'] )
	{

		$uploadPath = 'Profile/';
		$types_array = array("image/pjpeg", "image/jpg", "image/jpeg", "image/x-png", "image/png");
		if (!in_array($_FILES['image']['type'], $types_array)) {} 
		else
		{

			$imgName = $_SERVER['REQUEST_TIME'];
			$ext = '';
			switch( $_FILES['image']['type'] )
			{
					case 'image/pjpeg':
					case 'image/jpg':
					case 'image/jpeg':
						$ext = '.jpg';
					break;
					case 'image/x-png':
					case 'image/png':
						$ext = '.png';
					break;
					default:
						$ext = '.jpg';
					break;
			}


			$imgName = $_SERVER['REQUEST_TIME'].$ext;
			move_uploaded_file($_FILES['image']['tmp_name'], $uploadPath.$imgName);
			
			$profileQuery = ',`profileimage` = "'.$imgName.'"';
			
		}
		
	}
	
	
	$insert =& dbQuery('UPDATE `record` SET 
											`role_type`="'.$_POST['role'].'",
											`title`="'.$_POST['title'].'",  
											`fname`="'.$_POST['firstname'].'",  
											`mname`="'.$_POST['initials'].'",  
											`lname`="'.$_POST['lastname'].'",  
											`dob`="'.$_POST['birthday'].'", 
											`sex`="'.$_POST['sex'].'",
											`federaltaxid`="'.$_POST['tax'].'", 
											`phone`="'.$_POST['telephoneno'].'",  
											`fax`="'.$_POST['fax'].'",  
											`email`="'.$_POST['email'].'",  
											`occupation`="'.$_POST['occupation'].'",  
											`organisation`="'.$_POST['organisation'].'",  
											`street`="'.$_POST['address1'].'",  
											`streetb`="'.$_POST['address2'].'",  
											`city`="'.$_POST['city'].'", 
											`nextofkinfullname`="'.$_POST['nokFullname'].'",  
											`nextofkinstreet2`="'.$_POST['nokAddress1'].'",  
											`nextofkinstreet2b`="'.$_POST['nokAddress2'].'",  
											`nextofkincity2`="'.$_POST['nokCity'].'",  
											`nextofkinphone`="'.$_POST['nokPhone'].'",  
											`country`="'.$_POST['country'].'"
											'.$profileQuery.'
						WHERE `record_id` = '.$_POST['record']);
	
	
	if( dbAffectedRows($insert) > 0 )
	{		
		
		echo(json_encode(array("success")));
		exit();
		
	} else
	{
		echo(json_encode(array("failure")));
		exit();
	}
	
	
?>
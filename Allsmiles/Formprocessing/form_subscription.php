<?php

	if( !empty($_POST['uid']) )
	{
		
		$result =& dbQuery('INSERT INTO `subscription_list` (`subscription_id`,  `record_id`,  `activated`,  `expires`) VALUES 
			"'.$_POST['subscription'].'",
			"'.$_POST['uid'].'",
			"NOW()",
			"DATE_ADD( NOW(), INTERVAL '.$_POST['duration'].' DAY)"
		');

		if( dbAffectedRows($result)>0 )
		{
			
			$transaction_name = "subscription";
			$trans_no = $_SERVER['REQUEST_TIME'];
			$resultB =& dbQuery('INSERT INTO `transaction` (`record_id`,  `trans_name`,  `trans_no`,  `trans_amount`,  `trans_date`,  `generated_date`,  `trans_year`,  `trans_status`) VALUES (
				'.$_POST['uid'].',
				"'.$transaction_name.'",
				"'.$trans_no.'",
				"'.$_POST['price'].'",
				"NOW()",
				"NOW()",
				"YEAR( NOW() )"
			)');
			
			echo( json_encode(array("success")) );			
		
		} else
		{
			
			echo( json_encode(array("failure")) );		
		}
		exit;
	}
	
	
	if( !empty($_POST['update']) )
	{
		$affectedRows = 0;
		foreach( $_POST['amount'] as $k=>$v )
		{
			$updateQuery = 'UPDATE `subscription_package` SET `sp_duration` = '.$_POST['duration'][$k].', `sp_amount` = '.$_POST['amount'][$k].' WHERE `sp_id` = '.$k;
			$result =& dbQuery( $updateQuery );
			
			if( dbAffectedRows($result)>0 )
			{
				$affectedRows++;
			}

		}
		
		if( $affectedRows > 0 )
		{
			echo( json_encode(array("success")) );
		} else
		{
			echo( json_encode(array("failure")) );
		}
		exit;
	}
	
?>
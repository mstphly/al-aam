<?php

	$_POST = array_map('trim', $_POST);	

	$is_valid = VALIDATION::m_valid( 
	
		array(
			array( 'v'=>$_POST['username'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['password'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['confirmpassword'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['initials'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['firstname'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['lastname'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['birthday'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['telephoneno'], 'typ'=>'emp' ),
			array( 'v'=>$_POST['address1'], 'typ'=>'emp' ),			
			array( 'v'=>$_POST['city'], 'typ'=>'emp' ),												
		) 
	
	);
	

	
	
	if( false === $is_valid )
	{

		// $returnCode = 0;
		// switch( VALIDATION::errkey() )
		// {
			// case 12: // Email
				// $returnCode = 12;
			// break;
			// default:
				// $returnCode = 0;
			// break;
		// }
				
		
		// header('HTTP/1.1 301 Moved Permanently');
		// header('Location: ../'.$_POST['querystring'].'&errCode='.$returnCode);
		// exit;
		echo( json_encode(array("validation-error")) );
		exit();

	}
	
	
	$programmeType = 1; // Value is from DB
	
	$existingStatus =& dbQuery('SELECT 1 FROM `record` WHERE `username` = "'.$_POST['username'].'" LIMIT 1');
	
	if( 1==dbNumRows($existingStatus) )
	{
	
		echo( json_encode(array("account-exists")) );
		exit(0);
	
	} else
	{
	
		// Fresh Registration
		$imgName = '';
		if( $_FILES['image']['name'] )
		{

			$uploadPath = 'Profile/';
			$types_array = array("image/pjpeg", "image/jpg", "image/jpeg", "image/x-png", "image/png");
			if (!in_array($_FILES['image']['type'], $types_array)) {} 
			else
			{

				$imgName = $_SERVER['REQUEST_TIME'];
				$ext = '';
				switch( $_FILES['image']['type'] )
				{
						case 'image/pjpeg':
						case 'image/jpg':
						case 'image/jpeg':
							$ext = '.jpg';
						break;
						case 'image/x-png':
						case 'image/png':
							$ext = '.png';
						break;
						default:
							$ext = '.jpg';
						break;
				}


				$imgName = $_SERVER['REQUEST_TIME'].$ext;
				move_uploaded_file($_FILES['image']['tmp_name'], $uploadPath.$imgName);
				
			}
			
		}
		
		
		$insert =& dbQuery('INSERT INTO `record` (`role_type`, `username`, `password`, `authorized`, `fname`, `mname`, `lname`, `dob`, `title`, `sex`, `street`,`streetb`, `city`, `phone`, `country`, `profileimage`) VALUES ("'.$_POST['role'].'", "'.$_POST['username'].'", "'.$_POST['password'].'", 1, "'.$_POST['firstname'].'", "'.$_POST['initials'].'", "'.$_POST['lastname'].'", "'.$_POST['birthday'].'", "'.$_POST['title'].'", "'.$_POST['sex'].'", "'.$_POST['address1'].'", "'.$_POST['address2'].'", "'.$_POST['city'].'", "'.$_POST['telephoneno'].'", "'.$_POST['country'].'", "'.$imgName.'")');
		
		if( dbAffectedRows($insert) > 0 )
		{
					
			//Add doctor to specialist type here -> doctorType
			if( isset($_POST['doctorType']) )
			{
			
				$doctorId = dbInsertId();
				$insert =& dbQuery('INSERT INTO `doctor_specialist_map` (`record_id`,  `specialist_id`) VALUES ('.$doctorId.', '.$_POST['doctorType'].')');
				
			}
			
			echo(json_encode(array("success")));
			exit();
			
		} else
		{
			echo(json_encode(array("failure")));
			exit();
		}
		
	}
	
	
?>
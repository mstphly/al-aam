<?php

	require_once '../Components/config.php';

	
	$res =& dbQuery("select * from login where termsofservice = '$_POST[hidden_std_id]'");
	
	if( 0!=dbNumRows($sql) )
	{
		
		$row =& dbFetchObject($res);
		
		$errCode = 0;
		$old_password = $_POST['old_password'];
		if( $old_password != $row->password )
		{
			$errCode = 1;
		}elseif( $new_password != $new_password2 )
		{
			$errCode = 2;
		}elseif( strlen($new_password) < 6 )
		{
			$errCode = 3;
		}
		
		if( !empty($errCode) )
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ../index.php?mod=profile&sk=editPassword&err='.$errCode);
			exit;
		}
		
		$password = $new_password;
		$new_password = $new_password;
		$query = "UPDATE login SET log_password = '$new_password', password='$password' WHERE termsofservice = '$_POST[hidden_std_id]'";
		if( dbQuery($query) )
		{
			
			$to = $row->log_email;
			$today = date("j/m/Y, H:m");
			
			$body = "<html><body style=\"font-size:12px; font-family:tahoma;\"><div>Your Password Have been Successfully Updated. Mail sent on $today by $from\n\nYour New Account Info for $website_name registration is as follows:\n\nUsername: $mymatric_no\nPassword: $new_password\n\nThanks.\n\nAdministrator,\nFor NUC Database Portal, $website_name</div></body></html>";
			
			$sendMailStatus = MAIL::send_mail(array(
				'm_to' => $to,
				'm_from' => array( portalMail, portalName ),
				'm_bc' => '',
				'm_cc' => '',
				'm_reply' => '',
				'm_subject' => "Your Updated Account Logon Info From {portalName} Portal",
				'm_body' => $body,
				'm_type' => 'html',
				'm_priority' => '',
				'mcharset' => '',
				'mencoding' => ''
				)
			);

			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ../index.php?mod=profile&sk=editPassword&info=1');
			exit;
			
		
		}
	
	}
	
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: ../index.php?mod=profile&sk=editPassword&err=4');
	exit;
	
?>
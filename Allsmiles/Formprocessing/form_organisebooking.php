<?php
	
	
	if( isset($_POST['confirmButton']) )
	{	
		
		$update =& dbQuery('UPDATE `bookings` SET `doctor_id` = '.$_POST['d_id'].', `book_status` = "BOOKED", `appointment_status` = "PENDING" WHERE `book_id` = '.$_POST['book_id'] );
		
		if( dbAffectedRows()>0 )
		{
			
			echo( json_encode(array("success")) );
				$messageBody = '
					<html>
						<body>
							<div class="display:block; overflow:hidden; ">
								<div style="background:#333333; padding:5px 15px; font-size:14px; color:#FFFFFF">
									Booking Appointment COnfirm Details
								</div>
								<div style="padding:5px 10px; font-size:13px;border:1px solid #666666; line-height:22px;">
									Dear '.$_POST['bookfullname'].',
									
									Your booking for the date '.date("l, F j, Y, g:i A", strtotime($_POST['bookdatetime'])).' is confirmed, you are expected to visit to the clinic
									
									<div style="margin-top:30px; border-top:1px solid #CCCCCC; background:#FEFEFE; padding:5px; 10px">
										This email is an automated mail from Allsmiles dental clinic, please do not reply to it
									</div>
								</div>
								
							</div>
						</body>
					</html>
				';
				
				$msg = '
				<div class="display:block; overflow:hidden; ">
								<div style="background:#333333; padding:5px 15px; font-size:14px; color:#FFFFFF">
									Booking Appointment COnfirm Details
								</div>
								<div style="padding:5px 10px; font-size:13px;border:1px solid #666666; line-height:22px;">
									Dear '.$_POST['bookfullname'].',
									
									Your booking for the date '.date("l, F j, Y, g:i A", strtotime($_POST['bookdatetime'])).' is confirmed, you are expected to visit to the clinic
									
									<div style="margin-top:30px; border-top:1px solid #CCCCCC; background:#FEFEFE; padding:5px; 10px">
										This email is an automated mail from Allsmiles dental clinic, please do not reply to it
									</div>
								</div>
								
							</div>
				';
				
				$adminEmail = $GLOBALS['appSettings']['portalMail'];
				$insert =& dbQuery('INSERT INTO `messaging` (`record_id`, `msg_title`, `msg_content`, `recipient`) VALUES ("'.$_POST['uid'].'", "Booking Appointment Confirmation", "'.htmlentities($msg, ENT_QUOTES).'", "'.$adminEmail.'")');
				
				$sendMailStatus = MAIL::send_mail(array(
									'm_to' => $_POST['email'],
									'm_from' => array( $GLOBALS['appSettings']['portalMail'], $GLOBALS['appSettings']['portalName'] ),
									'm_bc' => '',
									'm_cc' => '',
									'm_reply' => '',
									'm_subject' => 'Booking Appointment Confirmation',
									'm_body' => $messageBody,
									'm_type' => 'html',
									'm_priority' => '',
									'mcharset' => '',
									'mencoding' => ''
									)
								);
			
			
			
			exit;								
								
		}
			
	} else if( isset($_POST['cancelButton']) )
	{
	
		dbQuery('UPDATE `bookings` SET `book_status` = "CANCELLED", `appointment_status` = "CANCELLED BY US" WHERE `book_id` = '.$_POST['book_id'] );
		
		if( dbAffectedRows()>0 )
		{
			
			echo( json_encode(array("success2")) );
				$messageBody = '
					<html>
						<body>
							<div class="display:block; overflow:hidden; ">
								<div style="background:#333333; padding:5px 15px; font-size:14px; color:#FFFFFF">
									Booking Appointment Cancellation
								</div>
								<div style="padding:5px 10px; font-size:13px;border:1px solid #666666; line-height:22px;">
									Dear '.$_POST['bookfullname'].',
									
									Your booking for the date '.date("l, F j, Y, g:i A", strtotime($_POST['bookdatetime'])).' has being cancelled, you are expected to contact the clinic for more details
									
									<div style="margin-top:30px; border-top:1px solid #CCCCCC; background:#FEFEFE; padding:5px; 10px">
										This email is an automated mail from Allsmiles dental clinic, please do not reply to it
									</div>
								</div>
								
							</div>
						</body>
					</html>
				';
				
				$msg = '
				<div class="display:block; overflow:hidden; ">
								<div style="background:#333333; padding:5px 15px; font-size:14px; color:#FFFFFF">
									Booking Appointment Cancellation
								</div>
								<div style="padding:5px 10px; font-size:13px;border:1px solid #666666; line-height:22px;">
									Dear '.$_POST['bookfullname'].',
									
									Your booking for the date '.date("l, F j, Y, g:i A", strtotime($_POST['bookdatetime'])).' has being cancelled, you are expected to contact the clinic for more details
									
									<div style="margin-top:30px; border-top:1px solid #CCCCCC; background:#FEFEFE; padding:5px; 10px">
										This email is an automated mail from Allsmiles dental clinic, please do not reply to it
									</div>
								</div>
								
							</div>
				';
				
				$adminEmail = $GLOBALS['appSettings']['portalMail'];
				$insert =& dbQuery('INSERT INTO `messaging` (`record_id`, `msg_title`, `msg_content`, `recipient`) VALUES ("'.$_POST['uid'].'", "Booking Appointment Confirmation", "'.htmlentities($msg, ENT_QUOTES).'", "'.$adminEmail.'")');
				
				$sendMailStatus = MAIL::send_mail(array(
									'm_to' => $_POST['email'],
									'm_from' => array( $GLOBALS['appSettings']['portalMail'], $GLOBALS['appSettings']['portalName'] ),
									'm_bc' => '',
									'm_cc' => '',
									'm_reply' => '',
									'm_subject' => 'Booking Appointment Cancellation',
									'm_body' => $messageBody,
									'm_type' => 'html',
									'm_priority' => '',
									'mcharset' => '',
									'mencoding' => ''
									)
								);
			
			exit;								
								
		}		
	
	}
	

?>
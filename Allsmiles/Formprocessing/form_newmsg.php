<?php
	
	if( !empty($_POST['uid']) )
	{
		
		$query = 'INSERT INTO `messaging` (`record_id`, `msg_title`, `msg_content`, `recipient`) VALUES';
		if( $_SESSION['info']['roleType'] == 3 || $_SESSION['info']['roleType'] == 4 )
		{ // This means its a patient
			$adminEmail = $GLOBALS['appSettings']['portalMail'];
			$query .= '("'.$_POST['uid'].'", "'.$_POST['subject'].'", "'.$_POST['content'].'", "'.$adminEmail.'")';				
		} else
		{
		
			$to = array();
			$msgT = explode(',', $_POST['msgTo']);
			foreach( $msgT as $k )
			{
				$to[] = $k;
				$query .= '("'.$_POST['uid'].'", "'.$_POST['subject'].'", "'.htmlentities($_POST['content'], ENT_QUOTES).'", "'.$k.'")';
			}
			$result =& dbQuery($query);
			
			$messageBody = '
				<html>
					<body>
						<div class="display:block; overflow:hidden; ">
							<div style="background:#333333; padding:5px 15px; font-size:14px; color:#FFFFFF">
								'.$_POST['subject'].'
							</div>
							<div style="padding:5px 10px; font-size:13px;border:1px solid #666666; line-height:22px;">
								'.$_POST['content'].'
								
								<div style="margin-top:30px; border-top:1px solid #CCCCCC; background:#FEFEFE; padding:5px; 10px">
									This email is an automated mail from Allsmiles dental clinic, please do not reply to it
								</div>
							</div>
							
						</div>
					</body>
				</html>
			';
			
			$sendMailStatus = MAIL::send_mail(array(
								'm_to' => $to,
								'm_from' => array( $GLOBALS['appSettings']['portalMail'], $GLOBALS['appSettings']['portalName'] ),
								'm_bc' => '',
								'm_cc' => '',
								'm_reply' => '',
								'm_subject' => $_POST['subject'],
								'm_body' => $messageBody,
								'm_type' => 'html',
								'm_priority' => '',
								'mcharset' => '',
								'mencoding' => ''
								)
							);			
		}


		if( dbAffectedRows($result)>0 )
		{
			
			echo( json_encode(array("success")) );
			exit;		
			
		} else
		{
			echo( json_encode(array("failure")) );
			exit;		
		}
	
	}	
?>
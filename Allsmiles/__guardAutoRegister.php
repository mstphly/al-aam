<?php

	require_once('Components/class.siteGuard.php');
	
	
	$bckp_folder = $_SERVER['DOCUMENT_ROOT'].'project.portal/REST/';
	$root_folder = array($_SERVER['DOCUMENT_ROOT'].'project.portal/');
	
	$adminemail = 'administrator@mysite.net';        # checking report will be sent to this email
	$viruses = array(
		  'Madguy' => '<script src="http://www.malwareholder.bad/madguy.js">',
		  'Destroyer'  => 'iframe src="a href="http://stupid-destroyer.bad/virus-script.htm"'
		  );
	
	$guard = new CSitePagesGuard($root_folder,
	  array('backupfolder'=>$bckp_folder,
	  'email' => $adminemail
	  )
	);
	
	$guard->SetMalwareSignatures('./viruses.def');   # Use a file as virus signatures storage
	
	$guard->UpdateFilesInfo(true); # Updating file data, with reporting results

<?php


	/* Components Setup */
		 require_once 'Components/func.loader.php';
		 require_once 'Components/func.timezone.php';
		 require_once 'Components/config.php';
		 require_once 'CMS/cms_api.php';
	/* Components Setup */	

ob_start('app_guard');
	

	
	// Real Processing Is Performed Here || NORMAL PAGE LOAD PROCESSING
	if( isset($_GET['seek'][0]) )
	{
		
		// BASIC CMS WEBSITE CONTROL	
		switch( $_GET['seek'] )
		{
			default:
				require_once 'Views/index.php';
			break;
			case 'notfound':
				require_once 'Views/notfound.php';
			break;
			case 'signup':
				require_once 'Views/register.php';
			break;
			case 'signin':
				require_once 'Views/login.php';
			break;
			case 'addaccount':
				require_once 'Views/app_addAccount.php';
			break;
			
			case 'profile':
				require_once 'Views/app_Profile.php';
			break;
			case 'editprofile':
				require_once 'Views/app_ProfileEdit.php';
			break;
			case 'deleteprofile':
				require_once 'Views/app_ProfileEdit.php';
			break;			
			
			case 'checkup':
				require_once 'Views/mod_Checkup.php';
			break;
			case 'viewusers':
				require_once 'Views/app_viewUsers.php';
			break;
			case 'viewpatients':
				require_once 'Views/app_viewPatients.php';
			break;
			
			case 'booking':
				require_once 'Views/app_Bookings.php';
			break;
			case 'bookingnew':
				require_once 'Views/app_BookingsNew.php';
			break;
			case 'bookinglist':
				require_once 'Views/app_ViewBookings.php';
			break;
			case 'bookingorganise':
				require_once 'Views/app_BookingsOrganise.php';
			break;
			
			case 'inbox':
				require_once 'Views/app_viewInbox.php';
			break;
			case 'viewInbox':
				require_once 'Views/app_viewMessage.php';
			break;
			case 'newmsg':
				require_once 'Views/app_newMessage.php';
			break;
			
			case 'subscription':
				require_once 'Views/app_Subscription.php';
			break;
			case 'subscriptioneditpackage':
				require_once 'Views/app_SubscriptionEditPackage.php';
			break;
			
			case 'reportExcel':
				require_once 'Classes/PHPExcel.php';
				require_once 'Views/reportExcel.php';
			break;
			
			case 'signout':
				
				session_start();
				$_SESSION = array();
				if( isset($_COOKIE[session_name()]) ) {
				   setcookie(session_name(), '', $_SERVER['REQUEST_TIME']-60*60*24*100, '/');
				}
				session_unset();
				session_destroy();
				
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: default.php');
				exit;
	
			break;
		}
		
	} else
	{
	
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: default.php');
		exit;
		
	}

	// CHECK FOR INTERCEPTION HERE
	if( isset($_GET['ajaxPageRequest']) )
	{
		// That means user intended ajax type of request
		echo $pageProperties['bodyContent'];
		exit;
	}
	
	$chatModule = '';
	if( isset($_SESSION['activated']) )
	{
		$_SESSION['username'] = $_SESSION['info']['username'];
		$chatModule = <<<CHATPLUGS
			<link type="text/css" rel="stylesheet" media="all" href="assets/jquerychat/css/chat.css" />
			<link type="text/css" rel="stylesheet" media="all" href="assets/jquerychat/css/screen.css" />		

			<!--[if lte IE 7]>
			<link type="text/css" rel="stylesheet" media="all" href="assets/jquerychat/css/screen_ie.css" />
			<![endif]-->
			
			<script type="text/javascript" src="assets/jquerychat/js/chat.js"></script>
			<script type="text/javascript">
				$(document).ready(function() {
					onlineList('Online');
				});
			</script>
CHATPLUGS;
		
	}


echo <<<DOCTYPE
	<!DOCTYPE html>
	<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
	<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
	<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
DOCTYPE;

echo <<<HEAD
	<head>
		<meta charset="utf-8" />
		<title>{$pageProperties['pageTitle']}</title>
		{$pageProperties['metadata']}
		<link href="Fashion/makeup.css" type="text/css" rel="stylesheet" />
		{$pageProperties['css']}
		{$pageProperties['jsHead']}
		
		<script type="text/javascript" src="assets/htmlhttprequest/htmlhttprequest_commented.js"></script>
		<script type = "text/javascript"> 
		var docClickLoader = new RemoteFileLoader('docClickLoader');
		function loadInto(src, destId, evt) {
			var ok = docClickLoader.loadInto(src.href || src.getAttribute('href'), destId);
			if (ok)
				cancelEvent(evt);
		};
		function toggleInto(src, destId, evt) {
			var dest = document.getElementById(destId);
			if (!dest.contentLoaded) {
				var ok = docClickLoader.loadInto(src.href || src.getAttribute('href'), destId);
				if (ok)
					dest.contentLoaded = true;
			}
			cancelEvent(evt);
			if (!dest.toggleState) {
				src.innerHTML = 'Close: ' + src.innerHTML;
				dest.style.display = 'block';
				dest.toggleState = 1;
			} else {
				src.innerHTML = src.innerHTML.replace(/^Close: /, '');
				dest.style.display = 'none';
				dest.toggleState = 0;
			}
		};
		addEvent(document, 'click', function (evt) {
			evt = evt || window.event;
			if (evt.which > 1 || evt.button > 1)
				return;
			var src = evt.target || evt.srcElement;
			if (src.nodeType && src.nodeType != 1)
				src = src.parentNode;
			while (src) {
				var srcName = (src.nodeName || src.tagName || '').toLowerCase();
				if (srcName == 'a' && src.className && src.className.match(/^(load|toggle)into-(.+)$/)) {
					if (RegExp.$1 == 'load')
						return loadInto(src, RegExp.$2, evt);
					if (RegExp.$1 == 'toggle')
						return toggleInto(src, RegExp.$2, evt);
				}
				src = src.parentNode;
			}
		}, 1);
		</script>
		{$chatModule}
	</head>
	
HEAD;


	{
		
		if( !isset($pageProperties['bodyClass']) )
			$pageProperties['bodyClass'] = '';

		if( !isset($pageProperties['bodyContent']) )
			$pageProperties['bodyContent'] = '';
			
		if( !isset($pageProperties['jsFoot']) )
			$pageProperties['jsFoot'] = '';
	
	}
	
echo <<<BODYSTART
	<body class="{$pageProperties['bodyClass']}">
BODYSTART;
	$GLOBALS['appSettings']['cache'] = false;	
	
	echo <<<ENDBODYHTML
			{$pageProperties['headContent']}
			<div id="ajaxbodyContainer">
			{$pageProperties['bodyContent']}
			</div>
			{$pageProperties['footContent']}
			{$pageProperties['jsFoot']}
	
	</body></html>
ENDBODYHTML;


ob_end_flush();

?>
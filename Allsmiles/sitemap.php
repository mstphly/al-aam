<?php


	/* Components Setup */
		 require_once 'Components/func.loader.php';
		 require_once 'Components/func.timezone.php';
		 require_once 'Components/config.php'; 
		 require_once 'CMS/cms_api.php';
		 require_once 'Views/__menuTop.php';
	/* Components Setup */

	switch( $appSettings['appLanguage'] )
	{
		case 'fr':
		setLang( 'fr' );
		break;
		case 'en':
		setLang();
		break;		
	}


echo <<<DOCTYPE
	<!DOCTYPE html>
	<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
	<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
	<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
DOCTYPE;

echo <<<HEAD
	<head>
		<meta charset="utf-8" />		
		<title>AllSmiles Dental Clinic | Site Map</title>
		<link rel="stylesheet" type="text/css" href="assets/build/css/style.css">
		<link rel="stylesheet" type="text/css" href="assets/build/css/boxed.css" id="layout">
		<link rel="stylesheet" type="text/css" href="assets/build/css/colors/green.css" id="colors">
		<link rel="shortcut icon" href="favicon.ico" />
		<script src="assets/build/js/jquery.min.js"></script>
		<script src="assets/build/js/custom.js"></script>
		<script src="assets/build/js/selectnav.js"></script>
		<script src="assets/build/js/flexslider.js"></script>
		<script src="assets/build/js/twitter.js"></script>
		<script src="assets/build/js/tooltip.js"></script>
		<script src="assets/build/js/effects.js"></script>
		<script src="assets/build/js/fancybox.js"></script>
		<script src="assets/build/js/carousel.js"></script>
		<script src="assets/build/js/isotope.js"></script>		
	</head>
	
HEAD;

$data = '<ol>
			<li><h4>Home</h4><span><a href="default.php">http:localhost:8080/allsmiles/default.php</a></span></li>
		';
$siteMapData = getDisplayLinks(1);
foreach( $siteMapData as $v )
{
	$data .= '<li><h4>'.$v['title'].'</h4>
		<span><a href="default.php?pg='.$v['id'].'">http:localhost:8080/allsmiles/default.php?pg='.$v['id'].'</a></span></li>';
}
$data .= '</ol>';
	
$displayContent = <<<HOMEPAGE
							<div class="container">

								
							</div>
		
		
		
			<div class="container">

				<div class="sixteen columns">
					<!-- Headline -->
					<div class="headline no-margin">
				  <h3>Site Map</h3></div>
				  <div class="editor">
				  {$data}
				  </div>
				  <img src="Fashion/Images/SITEMAP.png" alt="Site Map Image" />
				</div>
				
				
			</div>			
HOMEPAGE;

echo <<<BODYSTART
	<body>
BODYSTART;
	$GLOBALS['appSettings']['cache'] = false;
	
	echo <<<ENDBODYHTML

	<!-- Wrapper Start -->
<div id="wrapper">
  <!-- Header
================================================== -->
  <!-- 960 Container -->
  <div class="container ie-dropdown-fix">
	{$GLOBALS['headMenu']}
  </div>
  <!-- 960 Container / End -->
  <!-- Content
================================================== -->
  <!-- 960 Container -->
	{$displayContent}
</div>
<!-- Wrapper / End -->

{$GLOBALS['footerMenu']}
	</body></html>
ENDBODYHTML;

?>
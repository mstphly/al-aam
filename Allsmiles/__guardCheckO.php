<?php

	require_once('Components/class.siteGuard.php');
	
	
	$bckp_folder = $_SERVER['DOCUMENT_ROOT'].'project.portal/REST/';//'E:/myphpbackup/';                # where to save gzipped files copy
	$root_folder = array($_SERVER['DOCUMENT_ROOT'].'project.portal/');//array('E:/myphp1/','E:/myphp2/'); # folders to be monitored
	$adminemail = 'administrator@mysite.net';        # checking report will be sent to this email
	
	$guard = new CSitePagesGuard($root_folder,
	  array('backupfolder'=>$bckp_folder,
	  'email' => $adminemail
	  )
	);
	
	$guard->CheckFiles(CSitePagesGuard::RESTORE_NONE); # just check, no restore will be done
